#include "sis_forms.h"
#include "sis_util.h"
#include <stdlib.h> 
#include <string.h>
#include <math.h>
#include <hdf5.h>
#include <pthread.h>
#include <unistd.h>
#include "primes.h"

/*
Get 3D cube coordinate in 1D array
@x, x coordinate
@y, y coordinate
@z, z coordinate
*/
static size_t p3d(size_t x,size_t y, size_t z, size_t size)
{
    return (x+y*size+(z*size*size));// ((x*d1)+y*d2)+z;
}

static size_t dp3d(size_t x,size_t y, size_t z, size_t width,size_t height)
{
    return (x+y*width+(z*width*height));// ((x*d1)+y*d2)+z;
}





int uniformRand(int rangeLow, int rangeHigh,struct drand48_data *buffer) 
{
    long int randVal;
    lrand48_r(buffer,&randVal);
    double myRand = randVal/(1.0 + RAND_MAX); 
    int range = rangeHigh - rangeLow + 1;
    int myRand_scaled = (myRand * range) + rangeLow;
    return myRand_scaled;
}

sis_Detector *createDetector(size_t widtharg,size_t heightarg, size_t deptharg,float xzangle,float yzangle,float xyangle,int rollMode)
{
    sis_Detector* ret = (sis_Detector *)malloc(sizeof(sis_Detector));
    size_t elements;
    elements = widtharg*heightarg*deptharg;
    //printf("w %lu h %lu d %lu\n",widtharg,heightarg,deptharg);
    ret->data = (float*)calloc(elements,sizeof(float));
    if (ret->data==NULL){printf("Detector Malloc Failed to Allocate Memory \n");exit(EXIT_FAILURE);}
    ret->dataImaginary = NULL;

    ret->width = widtharg;
    ret->height = heightarg;
    ret->depth = deptharg;
    ret->middleW = round(widtharg/2.0);
    ret->middleH = round(heightarg/2.0);
    ret->yaw = xzangle;
    ret->pitch = yzangle;
    ret->roll = xyangle;
    ret->rollMode = rollMode;
    ret->pos[0] = 0;
    ret->pos[1] = 0;
    ret->pos[2] = 0;
    return ret;  
}

sis_Detector *createDetectorComplex(size_t widtharg,size_t heightarg, size_t deptharg,float xzangle,float yzangle,float xyangle,int rollMode)
{
    sis_Detector* ret = (sis_Detector *)malloc(sizeof(sis_Detector));
    size_t elements;
    elements = widtharg*heightarg*deptharg;
    //printf("w %lu h %lu d %lu\n",widtharg,heightarg,deptharg);
    ret->data = (float*)calloc(elements,sizeof(float));
    if (ret->data==NULL){printf("Detector Malloc Failed to Allocate Memory \n");exit(EXIT_FAILURE);}
    ret->dataImaginary = (float*)calloc(elements,sizeof(float));
    if (ret->dataImaginary==NULL){printf("Detector Malloc Failed to Allocate Memory \n");exit(EXIT_FAILURE);}

    ret->width = widtharg;
    ret->height = heightarg;
    ret->depth = deptharg;
    ret->middleW = round(widtharg/2.0);
    ret->middleH = round(heightarg/2.0);
    ret->yaw = xzangle;
    ret->pitch = yzangle;
    ret->roll = xyangle;
    ret->rollMode = rollMode;
    ret->pos[0] = 0;
    ret->pos[1] = 0;
    ret->pos[2] = 0;
    return ret;  
}

void deleteDetector(sis_Detector* toDelete)
{
    free(toDelete->data);
    if (toDelete->dataImaginary!=NULL)
        free(toDelete->dataImaginary);
    free(toDelete);
}

sis_Volume *createVolume(size_t sizearg,int x,int y, int z)
{
    sis_Volume* ret = (sis_Volume *)malloc(sizeof(sis_Volume));
    ret->data = (float*)calloc(sizearg*sizearg*sizearg,sizeof(float));
    ret->dataImaginary = NULL;
    if (ret->data==NULL){printf("Volume Malloc Failed to Allocate Memory \n");exit(EXIT_FAILURE);}
    ret->size = sizearg;
    ret->middle = round(sizearg/2.0);
    ret->pos[0] = x;
    ret->pos[1] = y;
    ret->pos[2] = z;
    return ret;  
}

sis_Volume *createVolumeComplex(size_t sizearg,int x,int y, int z)
{
    sis_Volume* ret = (sis_Volume *)malloc(sizeof(sis_Volume));
    ret->data = (float*)calloc(sizearg*sizearg*sizearg,sizeof(float));
    if (ret->data==NULL){printf("Volume Malloc Failed to Allocate Memory \n");exit(EXIT_FAILURE);}

    ret->dataImaginary = (float*)calloc(sizearg*sizearg*sizearg,sizeof(float));
    if (ret->dataImaginary==NULL){printf("Volume Malloc Failed to Allocate Memory \n");exit(EXIT_FAILURE);}
    ret->size = sizearg;
    ret->middle = round(sizearg/2.0);
    ret->pos[0] = x;
    ret->pos[1] = y;
    ret->pos[2] = z;
    return ret;  
}

void deleteVolume(sis_Volume* toDelete)
{
    free(toDelete->data);
    if (toDelete->dataImaginary!=NULL)
        free(toDelete->dataImaginary);
    free(toDelete);
}


 sis_Volume * loadHDF5floatsVolume(char * fileName, int complex)
 {
    hid_t       file, dataset;         /* handles */
    hid_t       datatype, dataspace;
    herr_t      status;
    hsize_t     dims_out[3];
    int rank,status_n;
    sis_Volume * volume;
    H5T_class_t t_class;                 /* data type class */
    H5T_order_t order;                 /* data order */
    size_t      size;
    
    int err=0;
    
    file = H5Fopen(fileName, H5F_ACC_RDONLY, H5P_DEFAULT);
    if (file<0)
    {
        printf("HDF5 Error: Unable to Open File\n");
        H5Fclose(file);
        exit(1);
    }
    dataset = H5Dopen2(file, "real", H5P_DEFAULT);
    datatype  = H5Dget_type(dataset);     /* datatype handle */
    t_class     = H5Tget_class(datatype);
    //if (t_class == H5T_FLOAT) printf("Data set has FLOAT type \n");
    order     = H5Tget_order(datatype);
    //if (order == H5T_ORDER_LE) printf("Little endian order \n");

    size  = H5Tget_size(datatype);
    //printf(" Data size is %d \n", (int)size);

    dataspace = H5Dget_space(dataset);    /* dataspace handle */
    rank      = H5Sget_simple_extent_ndims(dataspace);
    status_n  = H5Sget_simple_extent_dims(dataspace, dims_out, NULL);
    //printf("rank %d, dimensions %lu x %lu x %lu \n", rank,
	//   (unsigned long)(dims_out[0]), (unsigned long)(dims_out[1]), (unsigned long)(dims_out[2]));
    
    if (complex==1)
       volume = createVolumeComplex(dims_out[0],0,0,0); 
    else
        volume = createVolume(dims_out[0],0,0,0);
    status = H5Dread(dataset, H5T_NATIVE_FLOAT, H5S_ALL, H5S_ALL, H5P_DEFAULT, 
                    volume->data);
                    
	if (status<0)
    {
        printf("HDF5 Error: %d\n",(int)status);
        H5Dclose(dataset);
        H5Fclose(file);
        exit(1);
    }   
    H5Dclose(dataset);
    if (complex==1)
    {
        //printf("Reading Complex\n");
        dataset = H5Dopen2(file, "complex", H5P_DEFAULT);

        status = H5Dread(dataset, H5T_NATIVE_FLOAT, H5S_ALL, H5S_ALL, H5P_DEFAULT, 
                    volume->dataImaginary);    
        if (status<0)
        {
            printf("HDF5 Error: %d\n",(int)status);
            H5Fclose(file);
            exit(1);
        }  

        H5Dclose(dataset);
    }
	
    
    H5Sclose(dataspace);
    H5Fclose(file);
    return volume;
 }
 
void saveHDF5PlaneProjection(sis_Detector* detector, int planeIndex, char* filename)
{
    hid_t       file, space, dset;
    herr_t      status;
    hsize_t     dims[2];
    dims[0] = detector->height;
    dims[1] = detector->width; 
    //printf("dW %d dH %d \n",detector->width,detector->height);
    file = H5Fcreate (filename, H5F_ACC_TRUNC, H5P_DEFAULT, H5P_DEFAULT);
    space = H5Screate_simple (2, dims, NULL);
    dset = H5Dcreate (file, "images", H5T_NATIVE_FLOAT, space, H5P_DEFAULT,
                H5P_DEFAULT, H5P_DEFAULT);
    status = H5Dwrite (dset, H5T_NATIVE_FLOAT, H5S_ALL, H5S_ALL, H5P_DEFAULT,
                &(detector->data[dp3d(0,0,planeIndex,detector->width,detector->height)]));
    if (status<0)
        printf("HDF5 Error: %d\n",(int)status);
    H5Dclose (dset);
    H5Sclose (space);
    H5Fclose (file);
}

void saveHDF5VolumeProjection(sis_Detector* detector, char* filename)
{
    hid_t       file, space, dset;
    herr_t      status;
    hsize_t     dims[3];
    dims[0] = detector->depth;
    dims[1] = detector->width;
    dims[2] = detector->height;
    file = H5Fcreate (filename, H5F_ACC_TRUNC, H5P_DEFAULT, H5P_DEFAULT);
    space = H5Screate_simple (3, dims, NULL);
    dset = H5Dcreate (file, "images", H5T_NATIVE_FLOAT, space, H5P_DEFAULT,
                H5P_DEFAULT, H5P_DEFAULT);
    status = H5Dwrite (dset, H5T_NATIVE_FLOAT, H5S_ALL, H5S_ALL, H5P_DEFAULT,
                detector->data);
    if (status<0)
        printf("HDF5 Error: %d\n",(int)status);
    H5Dclose (dset);
    H5Sclose (space);
    H5Fclose (file);
}

void saveHDF5Volume(sis_Volume* volume, char* filename)
{
    hid_t       file, space, dset;
    herr_t      status;
    hsize_t     dims[3];
    dims[0] = volume->size;
    dims[1] = volume->size;
    dims[2] = volume->size;
    file = H5Fcreate (filename, H5F_ACC_TRUNC, H5P_DEFAULT, H5P_DEFAULT);
    space = H5Screate_simple (3, dims, NULL);
    dset = H5Dcreate (file, "real", H5T_NATIVE_FLOAT, space, H5P_DEFAULT,
                H5P_DEFAULT, H5P_DEFAULT);
    status = H5Dwrite (dset, H5T_NATIVE_FLOAT, H5S_ALL, H5S_ALL, H5P_DEFAULT,
                volume->data);
    if (status<0)
        printf("HDF5 Error: %d\n",(int)status);
    if (volume->dataImaginary!=NULL)
    {
        printf("Saving Imaginary\n");
        H5Dclose (dset);
        H5Sclose (space);
        space = H5Screate_simple (3, dims, NULL);

        dset = H5Dcreate (file, "complex", H5T_NATIVE_FLOAT, space, H5P_DEFAULT,
                H5P_DEFAULT, H5P_DEFAULT);
        status = H5Dwrite (dset, H5T_NATIVE_FLOAT, H5S_ALL, H5S_ALL, H5P_DEFAULT,
                volume->dataImaginary);
        if (status<0)
            printf("HDF5 Error: %d\n",(int)status);
        H5Dclose (dset);
        H5Sclose (space);
    }
    H5Fclose (file);
}

sis_ConeBeam *createConeBeam(int distancearg,float aarg,float barg,int x,int y, int z)
{
    sis_ConeBeam* ret = (sis_ConeBeam *)malloc(sizeof(sis_ConeBeam));
    ret->distance = distancearg;
    ret->a = aarg;
    ret->b = barg;
    ret->c = (ret->distance/(pow((ret->a/2),2)/pow(ret->a,2) + pow((ret->b/2),2)/pow(ret->b,2)))/2;
    ret->pos[0] = x;
    ret->pos[1] = y;
    ret->pos[2] = z;
    return ret;  
}





void spreadElementComplex(int xin,int yin,int zin,float anglecos,float anglesin,float addx, float addy, float addz, float a, float b,float c,
int (*form)(float a,float b,float c,float d, float e, float f),
float gf1,float gf2,float gf3,
float *volData,float *volDataImaginary,size_t volSize,float volMiddle,float *element,float *baseElement)
{
    sis_StackHead head;
    head.first = NULL;
    head.last = NULL;
    head.size = 0;
    ParticlePoint *actual;
    actual = (ParticlePoint*)malloc(sizeof(ParticlePoint));
    actual->x = xin;
    actual->y = yin;
    actual->z = zin;
    
    sis_stackPut(&head,actual);
    
    int x,y,z;
    float distance;
    float wx = 0;
    float wy = 0;
    float wz = 0;
    float vol =0;
    float val = 0;
    float rz,ry,rx;
    
    //printf("%f %f \n", element[0], baseElement[0]);
    if (baseElement[0]==element[0])
    {
        free(actual);
        return;
    }
            
    
    while((actual = (ParticlePoint *)sis_stackPop(&head))!=NULL)
    {
        x = actual->x;
        y = actual->y;
        z = actual->z;
        //printf("%d %d %d \n",x,y,z);
        free(actual);
        if ( !(x>=0 && y>=0 && z>=0 && x < volSize && y < volSize && z < volSize) )
            continue;

        distance = sqrt (pow(x-xin,2) + pow(y-yin,2) + pow(z-xin,2));
        if ( ( ( pow(x-xin,2)/(a*a) ) + ( pow(y-yin,2)/(b*b) ) + ( pow(z-zin,2)/(c*c) ) >1) )
            continue;

        wx = (x - volMiddle)/volMiddle;
        wy = (y - volMiddle)/volMiddle;
        wz = (z - volMiddle)/volMiddle;
        if ( form(wx,wy,wz,gf1,gf2,gf3) )
        {
            
            rz = nearbyint( (( (wz*anglecos) - (wy*anglesin) )+addz)*volMiddle)+volMiddle;
            ry = nearbyint( (( (wz*anglesin) + (wy*anglecos) )+addy)*volMiddle)+volMiddle;
            rx = ((wx+addx)*volMiddle)+volMiddle;
            if (rx>=0 && rx<volSize && ry>=0 && ry<volSize && rz>=0 && rz<volSize)
            {
                val = volData[p3d((size_t)rx,(size_t)ry,(size_t)rz,volSize)];
                if (val != 0 && val!=baseElement[0])
                    continue;
                volData[p3d((size_t)rx,(size_t)ry,(size_t)rz,volSize)] = element[0];
                volDataImaginary[p3d((size_t)rx,(size_t)ry,(size_t)rz,volSize)] = element[1];

                for(int xi=-1;xi<=1;xi++)
                    for(int yi=-1;yi<=1;yi++)
                        for(int zi=-1;zi<=1;zi++)
                        {
                            actual = (ParticlePoint*)malloc(sizeof(ParticlePoint));
                            actual->x = (x+xi);
                            actual->y = (y+yi);
                            actual->z = (z+zi);
                            sis_stackPut(&head,actual);
                        }
            }
        } 
    }
}




void *makeGeometricFormJobComplexAngle(void * params)
{
    DonutsJobConfig *config = (DonutsJobConfig*)params;
    int (*form)(float a,float b,float c,float d, float e, float f) = config->insideform;
    float gf1 = config->gf1;
    float gf2 = config->gf2;
    float gf3 = config->gf3;

    int myId;
    sis_Volume* volume = config->volume;
    size_t elemAmount = config->elemAmount;
    float ** elements = config->elements;
    pthread_mutex_t* resultMutex = config->resultMutex;
    int padding = config->padding;
    size_t linesPerThread = config->linesPerThread;
    long int seed = config->seed;
    float angle = config->angle;
    float addx = config->addx;
    float addy = config->addy;
    float addz = config->addz;

    pthread_mutex_lock(resultMutex);
    myId =config->threadIds;
    config->threadIds++;
    pthread_mutex_unlock(resultMutex);

    struct drand48_data randBuffer;
    int rand1 = ( seed+ ( myId*seed ) ) % MAXPRIMES;
    srand48_r(primes[rand1],&randBuffer);

    double wx = 0;
    double wy = 0;
    double wz = 0;
    double distance=0;
    long int x,y,z;

    double vol =0;
    double volMiddle = volume->middle;
    size_t volSize = volume->size;
    float *volData = volume->data;
    float *volDataImaginary = volume->dataImaginary;
    int randomRangeMin = 4;
    int randomRangeMax = 5+volSize/100.0;
    int objectsDistributionChance = uniformRand(0,250 * volSize,&randBuffer); // 1 in 200 * volSize chance of creating a object
    int randomBuddy;
    int element = uniformRand(1,elemAmount,&randBuffer);

    long int start, end;
    start = myId*linesPerThread;
    end = (myId+1)*linesPerThread;

    float rz,ry,rx, anglecos,anglesin;
    anglecos = cos(angle);
    anglesin = sin(angle);
    for (y=start; y<end;y++)
    {
        wy = (y/volMiddle) -1;
        for (z=0; z<volSize;z++)
        {
            wz = (z/volMiddle) -1;
            for (x=0; x<volSize;x++)
            {
                wx = (x/volMiddle) -1;
                if (form(wx,wy,wz,gf1,gf2,gf3))
                {
                    rz = nearbyint( (( (wz*anglecos) - (wy*anglesin) )+addz+1)*volMiddle);
                    ry = nearbyint( (( (wz*anglesin) + (wy*anglecos) )+addy+1)*volMiddle);
                    rx = (wx+addx+1)*volMiddle;
                    if (volData[p3d(rx,ry,rz,volSize)]==0 && rx>=0 && rx<volSize
                    && ry>=0 && ry<volSize && rz>=0 && rz<volSize)
                    {
                        volData[p3d(rx,ry,rz,volSize)] = elements[0][0];
                        volDataImaginary[p3d(rx,ry,rz,volSize)] = elements[0][1];
                    }
                    
                    randomBuddy = uniformRand(0,objectsDistributionChance,&randBuffer);
                    if (randomBuddy == 1 && elemAmount>1)
                    {
                        
                        element = uniformRand(1,elemAmount-1,&randBuffer);
                        spreadElementComplex(x,y,z,anglecos,anglesin,addx,addy,addz,uniformRand(randomRangeMin,randomRangeMax,&randBuffer),uniformRand(randomRangeMin,randomRangeMax,&randBuffer),uniformRand(randomRangeMin,randomRangeMax,&randBuffer),
                        form,gf1,gf2,gf3,
                        volData,volDataImaginary,volSize,volMiddle,elements[element],elements[0]);
                    }
                }
            }
        } 
    }
    pthread_mutex_lock(resultMutex);
    (*(config->threadCount))--;
    pthread_mutex_unlock(resultMutex);
}

void makeGeometricFormVolumeComplex(sis_Volume* volume,
int (*form)(float a,float b,float c,float d, float e, float f),
float gf1,float gf2,float gf3,
float tiltAngle,float addx,float addy,float addz,size_t elemAmount, float ** elements, int maxThreads, long int seed)
{
    pthread_mutex_t resultMutex;
    pthread_t threadReference;
    pthread_mutex_init(&resultMutex, NULL);

    float wx = 0;
    float wy = 0;
    float wz = 0;
    float distance=0;
    size_t x,ybase,yfine,y,z;
    int threadCount=0;
    int localThreadCount = 0;

    struct drand48_data randBuffer;
    srand48_r(seed,&randBuffer);

    float vol =0;
    float volMiddle = volume->middle;
    size_t volSize = volume->size;
    float *volData = volume->data;
    int randomRangeMin = 4;
    int randomRangeMax = 5+volSize/100.0;
    int objectsDistributionChance = uniformRand(0,250 * volSize,&randBuffer); // 1 in 200 * volSize chance of creating a object
    int randomBuddy;
    int element = uniformRand(1,elemAmount,&randBuffer);
    
    int padding=randomRangeMax;
    size_t linesPerThread = volSize/maxThreads;
    while(padding>=linesPerThread)
    {
        maxThreads--;
        linesPerThread = volSize/maxThreads;
    }

    DonutsJobConfig config;
    config.addx = addx;
    config.addy = addy;
    config.addz = addz;
    config.angle = tiltAngle;
    config.volume=volume;
    config.insideform=form;
    config.gf1=gf1;
    config.gf2=gf2;
    config.gf3=gf3;
    config.elemAmount=elemAmount;
    config.elements=elements;
    
    config.padding=padding;
    config.linesPerThread=linesPerThread;

    config.resultMutex = &resultMutex;
    config.threadCount = &threadCount;
    config.threadIds=0;
    config.seed = seed;
    printf("Starting %d Threads to build Phantom\n",maxThreads);
    for(int i=0;i<maxThreads;i++)
    {
        if(pthread_create(&threadReference, NULL, makeGeometricFormJobComplexAngle, &config)) 
        {
            fprintf(stderr, "Error creating thread\n");
        }else
        {
            pthread_mutex_lock(&resultMutex);
            threadCount++;
            pthread_mutex_unlock(&resultMutex); 
        }
    }

    localThreadCount=1;
    while(localThreadCount>0)
    {
        pthread_mutex_lock(&resultMutex);
        localThreadCount = threadCount;
        pthread_mutex_unlock(&resultMutex);
        usleep(500);
    }
}

int torusInside(float x,float y, float z,float sphereR, float smallSphereR,float d3)
{
    float smallSphereR2 = smallSphereR*smallSphereR;
    float vol = (pow(sqrt(x*x + y*y)-sphereR,2) + z*z);
    if(vol<smallSphereR2)
        return 1;
    return 0;
};

void makeDonutsVolumeComplex(sis_Volume* volume,float R, float r,float tiltAngle,float addx,float addy,float addz,size_t elemAmount, float ** elements, int maxThreads, long int seed)
{
    makeGeometricFormVolumeComplex(volume,torusInside,R,r,0,
    tiltAngle,addx,addy,addz,elemAmount,elements,maxThreads,seed);
}

int sphereInside(float x,float y, float z,float sphereR, float d2,float d3)
{
    float vol = pow(sqrt(x*x + y*y + z*z),2);
    if(vol<sphereR)
        return 1;
    return 0;
};

void makeSphereVolumeComplex(sis_Volume* volume,float R,float tiltAngle,float addx,float addy,float addz,size_t elemAmount, float ** elements, int maxThreads, long int seed)
{
    //printf("RBase: %f\n",R);
    makeGeometricFormVolumeComplex(volume,sphereInside,R,0,0,
    tiltAngle,addx,addy,addz,elemAmount,elements,maxThreads,seed);
}

int cubeInside(float x,float y, float z,float size, float d2,float d3)
{  
    float nsize = -size;
    if(x<size && y < size && z < size && x>nsize && y>nsize && z>nsize)
        return 1;
    return 0;
};

void makeCubeVolumeComplex(sis_Volume* volume,float size,float tiltAngle,float addx,float addy,float addz,size_t elemAmount, float ** elements, int maxThreads, long int seed)
{
    makeGeometricFormVolumeComplex(volume,cubeInside,size,0,0,
    tiltAngle,addx,addy,addz,elemAmount,elements,maxThreads,seed);
}

int discInside(float x,float y, float z,float R, float size,float d3)
{  
    float nsize = -size;
    float vol = pow(sqrt(x*x + y*y),2);
    if(vol<R && z<size && z > nsize)
        return 1;
    return 0;
};

void makeDiscVolumeComplex(sis_Volume* volume,float R,float size,float tiltAngle,float addx,float addy,float addz,size_t elemAmount, float ** elements, int maxThreads, long int seed)
{
    makeGeometricFormVolumeComplex(volume,discInside,R,size,0,
    tiltAngle,addx,addy,addz,elemAmount,elements,maxThreads,seed);
}