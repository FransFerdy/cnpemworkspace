#include <stdio.h>
#include <curl/curl.h>
#include <string>
#include <vector>
#include <iostream>
#include <fstream>
#include "cJSON.h"
#include <stdlib.h>
#include "json.hpp"
#include "material.h"

using json = nlohmann::json;

#define MOL 6.02214179E23;

bool fileExists(std::string fileName)
{
    ;FILE * pFile;
    pFile = fopen ( fileName.c_str() , "rb" );
    if (pFile==NULL)
        return false;
    fclose(pFile);
    return true;
}


static size_t WriteCallback(void *contents, size_t size, size_t nmemb, void *userp)
{
    ((std::string*)userp)->append((char*)contents, size * nmemb);
    return size * nmemb;
}

class AtomFormFactor
{
public:
    float E,f1,f2,lambda;
    float Photoelectric_mu_divby_rho,
          CohPlusInc_sigma_divby_rho,
          Total_mu_divby_rho,
          mu_divby_rhoK;
    AtomFormFactor()
    {
        E=0;
        f1=0;
        f2=0;
        lambda=0;
        Photoelectric_mu_divby_rho=0;
        CohPlusInc_sigma_divby_rho=0;
        Total_mu_divby_rho=0;
        mu_divby_rhoK=0;
    }   
    void selfPrint()
    {
        std::cout << "E " << E << std::endl;
        std::cout << "f1 " << f1 << std::endl;
        std::cout << "f2 " << f2 << std::endl; 
        std::cout << "Photoelectric_mu_divby_rho " << Photoelectric_mu_divby_rho << std::endl;
        std::cout << "CohPlusInc_sigma_divby_rho " << CohPlusInc_sigma_divby_rho << std::endl;
        std::cout << "Total_mu_divby_rho " << Total_mu_divby_rho << std::endl;
        std::cout << "mu_divby_rhoK " << mu_divby_rhoK << std::endl;
        std::cout << "lambda " << lambda << std::endl;
        std::cout << "##########" << std::endl;
    }  
};

void internalGetMaterialInfo(std::string element)
{
    CURL *curl;
    CURLcode res;
    std::string readBuffer;
    std::string fileName(element);
    std::string editText;
    std::string editText2;
    fileName = "materials/"+fileName + ".json";
    size_t pos,pos2;
    system("mkdir materials 2> mkdirlog.log");

    float Z_electronsPerAtom;
    float P_elementDensity;
    float A_atomicWeight;
    float Sigma_divisionVal;
    float Eev_divisionVal;
    float Energy_edgesNum;
    float A_atomicMass;
    std::pair<std::string, float> Energy_edges[32];
    float NuclearThomsonCorrection;
    float RelativisticCorrectionValue1;
    float RelativisticCorrectionValue2;
    std::vector <AtomFormFactor> atomFormFactors;
    AtomFormFactor actualAtom;
    int count;

    json jsonOut = json::object();
    json jsonObject = json::object();

    if (!fileExists(fileName))
    {
        curl = curl_easy_init();
        if(curl) 
        {
            std::string url;
            url = "https://physics.nist.gov/cgi-bin/ffast/ffast.pl?Formula="+element+"&gtype=4&range=S&lower=0.01&upper=200&density=";
            std::cout << "Getting Data From: " << url << std::endl;
            curl_easy_setopt(curl, CURLOPT_URL, url.c_str());
            curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, WriteCallback);
            curl_easy_setopt(curl, CURLOPT_WRITEDATA, &readBuffer);
            res = curl_easy_perform(curl);
            curl_easy_cleanup(curl);

            try
            {
                editText = readBuffer;
                pos = editText.find("Invalid Element or Compound");
                if (pos != std::string::npos)
                {
                    std::cout << "Element not found on Nist Database: " << element << std::endl;
                    exit(1);
                }

                editText = readBuffer;
                pos = editText.find("Z =");
                pos +=3;
                editText = editText.substr(pos);
                pos = editText.find(")");
                editText = editText.substr(0,pos);
                Z_electronsPerAtom = std::stof(editText);

                editText = readBuffer;
                pos = editText.find("A<sub>r</sub></i> = ");
                pos +=20;
                editText = editText.substr(pos);
                pos = editText.find(" g");
                editText = editText.substr(0,pos);
                A_atomicMass = std::stof(editText);

                editText = readBuffer;
                pos = editText.find("g cm");
                editText = editText.substr(pos);
                pos = editText.find("=");
                pos+=1;
                editText = editText.substr(pos);

                pos = editText.find("<dd>");
                editText = editText.substr(0,pos);
                P_elementDensity = std::stof(editText);

                editText = readBuffer;
                pos = editText.find("<i>E</i>");
                editText = editText.substr(pos);
                pos = editText.find("<dd>");
                pos+=4;
                editText = editText.substr(pos);

                pos = editText.find("edges");
                editText = editText.substr(0,pos);
                Energy_edgesNum = std::stof(editText);

                editText = readBuffer;
                pos = editText.find("barns/atom");
                editText = editText.substr(pos);
                pos = editText.find("&#215;&#160;");
                pos+=12;
                editText = editText.substr(pos);

                pos = editText.find("<dd>");
                editText = editText.substr(0,pos);
                Sigma_divisionVal = std::stof(editText);

                editText = readBuffer;
                pos = editText.find("E</i>(eV)");
                editText = editText.substr(pos);
                pos = editText.find("&#215;&#160;");
                pos+=12;
                editText = editText.substr(pos);

                pos = editText.find("<dd>");
                editText = editText.substr(0,pos);
                Eev_divisionVal = std::stof(editText);

                editText = readBuffer;
                pos = editText.find("Nuclear Thomson correction");
                editText = editText.substr(pos);
                pos = editText.find("=");
                pos+=1;
                editText = editText.substr(pos);

                pos = editText.find("<i>");
                editText = editText.substr(0,pos);
                NuclearThomsonCorrection = std::stof(editText);

                editText = readBuffer;
                pos = editText.find("Relativistic correction estimate");
                editText = editText.substr(pos);
                pos = editText.find("=");
                pos+=1;
                editText = editText.substr(pos);

                pos = editText.find(",");
                editText = editText.substr(0,pos);
                RelativisticCorrectionValue1 = std::stof(editText);
                
                editText = readBuffer;
                pos = editText.find("Relativistic correction estimate");
                editText = editText.substr(pos);
                pos = editText.find("=");
                pos+=1;
                editText = editText.substr(pos);
                pos = editText.find(",");
                pos+=1;
                editText = editText.substr(pos);

                pos = editText.find(",");
                editText = editText.substr(0,pos);
                RelativisticCorrectionValue2 = std::stof(editText);


                editText = readBuffer;
                pos = editText.find("<pre>");
                pos+=7;
                editText = editText.substr(pos);

                pos = editText.find("</pre>");
                editText = editText.substr(0,pos);
                //std::cout << editText << std::endl;
                count = 0;
                while(editText.length()>0)
                {
                    editText2 = editText.substr(2);
                    editText2 = editText2.substr(0,5);
                    editText2.erase(remove_if(editText2.begin(), editText2.end(), isspace), editText2.end());

                    Energy_edges[count].first = editText2;
                    
                    
                    editText2 = editText.substr(9);
                    editText2 = editText2.substr(0,11);

                    Energy_edges[count].second = std::stof(editText2);
                    editText = editText.substr(20);

                    //std::cout << Energy_edges[count].first << " " << Energy_edges[count].second << std::endl;
                    count++;

                    pos = editText.find("\n");
                    if (pos==std::string::npos)
                        break;
                    if (pos==1)
                        editText = editText.substr(pos+1);
                }

                editText = readBuffer;
                pos = editText.find("Form Factors, Attenuation");
                editText = editText.substr(pos);
                pos = editText.find("nm");
                pos+=3;
                editText = editText.substr(pos);
                pos = editText.find("</BODY>");
                editText = editText.substr(0,pos);

                while(editText.length()>0)
                {
                    pos = editText.find("  ");
                    editText2 = editText.substr(0,pos);
                    actualAtom.E = std::stof(editText2);
                    editText = editText.substr(pos+2);

                    pos = editText.find("  ");
                    editText2 = editText.substr(0,pos);
                    actualAtom.f1 = std::stof(editText2);
                    editText = editText.substr(pos+2);

                    pos = editText.find("  ");
                    editText2 = editText.substr(0,pos);
                    actualAtom.f2 = std::stof(editText2);
                    editText = editText.substr(pos+2);

                    pos = editText.find("  ");
                    editText2 = editText.substr(0,pos);
                    actualAtom.Photoelectric_mu_divby_rho = std::stof(editText2);
                    editText = editText.substr(pos+2);

                    pos = editText.find("  ");
                    editText2 = editText.substr(0,pos);
                    actualAtom.CohPlusInc_sigma_divby_rho = std::stof(editText2);
                    editText = editText.substr(pos+2);

                    pos = editText.find("  ");
                    editText2 = editText.substr(0,pos);
                    actualAtom.Total_mu_divby_rho = std::stof(editText2);
                    editText = editText.substr(pos+2);

                    pos = editText.find("  ");
                    editText2 = editText.substr(0,pos);
                    actualAtom.mu_divby_rhoK = std::stof(editText2);
                    editText = editText.substr(pos+2);

                    pos = editText.find("\n");
                    if (pos!=std::string::npos)
                    {
                        editText2 = editText.substr(0,pos);
                        actualAtom.lambda = std::stof(editText2);
                        atomFormFactors.push_back(actualAtom);
                        if (editText.length()>pos+1)
                            editText = editText.substr(pos+1);
                        else
                            break;
                    }
                    else
                    {
                        actualAtom.lambda = std::stof(editText);
                        atomFormFactors.push_back(actualAtom);
                        break;
                    }
                }
                /*
                std::cout << Z_electronsPerAtom << std::endl;
                std::cout << P_elementDensity << std::endl;
                std::cout << Sigma_divisionVal << std::endl;
                std::cout << Eev_divisionVal << std::endl;
                std::cout << RelativisticCorrectionValue1 << std::endl;
                std::cout << RelativisticCorrectionValue2 << std::endl;
                std::cout << NuclearThomsonCorrection << std::endl;
                std::cout << Energy_edgesNum << std::endl;
                */
            
            }catch(std::exception e)
            {
                std::cout << "There was an error parsing the element from Nist Database" <<std::endl;
                exit(1);
            }
        }else
        {
            std::cout << "Failed to Start Curl" << std::endl;
            exit(1);
        }
        jsonOut["A_atomicMass"] = A_atomicMass; 
        jsonOut["Element"] = element;
        jsonOut["Z_electronsPerAtom"] = Z_electronsPerAtom;
        jsonOut["P_elementDensity"] = P_elementDensity;
        jsonOut["Sigma"] = Sigma_divisionVal;
        jsonOut["Eev"] = Eev_divisionVal;
        jsonOut["RelativisticCorrectionValue1"] = RelativisticCorrectionValue1;
        jsonOut["RelativisticCorrectionValue2"] = RelativisticCorrectionValue2;
        jsonOut["NuclearThomsonCorrection"] = NuclearThomsonCorrection;
        jsonOut["Energy_edgesNum"] = Energy_edgesNum;
        jsonOut["EdgeEnergies"] = json::array();

        
        for (int i=0; i< count; i++)
        {
            jsonObject = json::object();
            jsonObject["Level"] = Energy_edges[i].first;
            jsonObject["Value"] = Energy_edges[i].second;
            jsonOut["EdgeEnergies"].push_back(jsonObject);
        }

        jsonOut["FormFactors"] = json::array();
        for (int i=0; i< atomFormFactors.size(); i++)
        {
            jsonObject = json::object();
            jsonObject["E"] = atomFormFactors[i].E;
            jsonObject["f1"] = atomFormFactors[i].f1;
            jsonObject["f2"] = atomFormFactors[i].f2;
            jsonObject["lambda"] = atomFormFactors[i].lambda;
            jsonObject["Photoelectric_mu_divby_rho"] = atomFormFactors[i].Photoelectric_mu_divby_rho;
            jsonObject["CohPlusInc_sigma_divby_rho"] = atomFormFactors[i].CohPlusInc_sigma_divby_rho;
            jsonObject["Total_mu_divby_rho"] = atomFormFactors[i].Total_mu_divby_rho;
            jsonObject["mu_divby_rhoK"] = atomFormFactors[i].mu_divby_rhoK;
            
            jsonOut["FormFactors"].push_back(jsonObject);
        }

        std::ofstream outFile(fileName,std::ofstream::out);
        outFile << jsonOut;
    }

    std::cout<< "==============================================================="<<std::endl; 
    std::cout<< "Material " << element<<std::endl; 
}

float interpolate(float myX,float xA,float xB,float yA,float yB)
{
    return yA + ( yB - yA ) * ( (myX - xA) / ( xB - xA )  );
}

PhantomMaterialAtEnergy * internalGetMaterialAtEnergy(std::string elementArg,float energy)
{
    internalGetMaterialInfo(elementArg);

    std::ifstream input("materials/"+elementArg+".json");
    json jsonin;
    input >> jsonin;

    PhantomMaterialAtEnergy *ret = (PhantomMaterialAtEnergy*)malloc(sizeof(PhantomMaterialAtEnergy));
    ret->A_atomicMass = jsonin["A_atomicMass"];
    ret->density = jsonin["P_elementDensity"]; //TODO MATH TO GET ACTUAL electron density
    ret->Z_electronsPerAtom = jsonin["Z_electronsPerAtom"];
    
    size_t maxFormFactors = jsonin["FormFactors"].size();
    size_t pos=-1;

    float closestEnergy=9999;
    float nowEnergy=9999;

    for(size_t i=0; i<maxFormFactors; i++)
    {
        nowEnergy = jsonin["FormFactors"][i]["E"];
        nowEnergy = std::abs(nowEnergy-energy);
        if ( nowEnergy<closestEnergy )
        {
            closestEnergy = nowEnergy;
            pos = i;
        }
    }
    if (pos==-1)
    {
        std::cout << "Failed to find that energy on the element table" <<std::endl;
        exit(1);
    }

    std::cout << "For " << elementArg <<" Using energy: " << jsonin["FormFactors"][pos]["E"] <<std::endl;
    ret->lambda = jsonin["FormFactors"][pos]["lambda"];
    ret->f1 = jsonin["FormFactors"][pos]["f1"];
    ret->f2 = jsonin["FormFactors"][pos]["f2"];

    return ret;
}


PhantomMaterialAtEnergy * internalGetMaterialAtEnergyInterp(std::string elementArg,float energy)
{
    internalGetMaterialInfo(elementArg);

    std::ifstream input("materials/"+elementArg+".json");
    json jsonin;
    input >> jsonin;

    PhantomMaterialAtEnergy *ret = (PhantomMaterialAtEnergy*)malloc(sizeof(PhantomMaterialAtEnergy));
    ret->A_atomicMass = jsonin["A_atomicMass"];
    ret->density = jsonin["P_elementDensity"]; //TODO MATH TO GET ACTUAL electron density
    ret->Z_electronsPerAtom = jsonin["Z_electronsPerAtom"];
    size_t maxFormFactors = jsonin["FormFactors"].size();
    size_t posMin=-1;
    size_t posMax=-1;

    float closestEnergyMin=9999;
    float closestEnergyMax=9999;
    float nowEnergy=9999;
    float nowEnergyPure=9999;

    for(size_t i=0; i<maxFormFactors; i++)
    {
        nowEnergyPure = jsonin["FormFactors"][i]["E"];
        nowEnergy = nowEnergyPure;
        nowEnergy = std::abs(nowEnergy-energy);

        if ( nowEnergyPure>energy)
        {
            if (nowEnergy<closestEnergyMax)
            {
                closestEnergyMax = nowEnergy;
                posMax = i;
            }
        }else
        {
            if (nowEnergy<closestEnergyMin)
            {
                closestEnergyMin = nowEnergy;
                posMin = i;
            }
        }
    }
    if (posMax==-1 || posMin==-1)
    {
        std::cout << "For " << elementArg <<" Failed to find that energy (min,max) on the element table" <<std::endl;
        exit(1);
    }

    //std::cout << "For " << elementArg <<" Using energy Max: " << jsonin["FormFactors"][posMax]["E"] <<std::endl;
    //std::cout << "For " << elementArg <<" Using energy Min: " << jsonin["FormFactors"][posMin]["E"] <<std::endl;
    std::cout << "Energy: " << energy << " kev " <<" For " << elementArg <<" Interpolated Between Max: " << jsonin["FormFactors"][posMax]["E"] <<" Min:" << jsonin["FormFactors"][posMin]["E"] <<std::endl;
    
    ret->lambda = interpolate(energy,
    jsonin["FormFactors"][posMin]["E"],jsonin["FormFactors"][posMax]["E"],
    jsonin["FormFactors"][posMin]["lambda"],jsonin["FormFactors"][posMax]["lambda"] );

    ret->f1 = interpolate(energy,
    jsonin["FormFactors"][posMin]["E"],jsonin["FormFactors"][posMax]["E"],
    jsonin["FormFactors"][posMin]["f1"],jsonin["FormFactors"][posMax]["f1"] );

    ret->f2 = interpolate(energy,
    jsonin["FormFactors"][posMin]["E"],jsonin["FormFactors"][posMax]["E"],
    jsonin["FormFactors"][posMin]["f2"],jsonin["FormFactors"][posMax]["f2"] );

    //std::cout << "Lambda " << ret->lambda <<std::endl;
    //std::cout << "F1 " << ret->f1 <<std::endl;
    //std::cout << "F2 " << ret->f2 <<std::endl;

    return ret;
}
 

void getMaterialInfo(char * elementArg)
{
    return internalGetMaterialInfo(elementArg);
}

//you have to free the PhantomMaterialAtEnergy after using it
PhantomMaterialAtEnergy * getMaterialAtEnergy(char * elementArg,float energy)
{
    return internalGetMaterialAtEnergyInterp(elementArg,energy);
}

//you have to free the ComplexNumber after using it
float * getMaterialRefractiveIndexAtEnergy(char * elementArg,float energy,int interp)
{
    PhantomMaterialAtEnergy *material;
    if (interp)
        material = internalGetMaterialAtEnergyInterp(elementArg,energy);
    else
        material = internalGetMaterialAtEnergy(elementArg,energy);

    float *ret = (float*)malloc(sizeof(float)*3);
    double classicalElectronRadius;
    double lambda;
    double atomicMass = material->A_atomicMass/MOL;
    double atomDensity = (material->density/atomicMass)*1E8; //cm3
    lambda = material->lambda*1E-9;//nm
    classicalElectronRadius = CLASSICAL_ELECTRON_RADIUS;//E-15 E11 //m

    std::cout << "Material "<< " atomicMass " << atomicMass << " atomic density "  << atomDensity <<" a/m" << std::endl;

    //double classicalElectronRadius = CLASSICAL_ELECTRON_RADIUS * 100; //to centimeters
    //double lambda =  ( (double)material->lambda) * 0.0000001; // to centimeters

    ret[0] = ( classicalElectronRadius * pow(lambda,2) ) / ( 2 * PI );
    ret[1] = ret[0];

    ret[0] = ret[0] * ( ( material->Z_electronsPerAtom + material->f1 ) * atomDensity );

    //ret[0] = 1 - ret[0];

    ret[1] = ret[1] * ( material->f2 * atomDensity );
    
    ret[2] = material->lambda;

    std::cout << "Refractive Index "<< "n = 1 - " << ret[0]  << " + " << ret[1] << "i" <<std::endl;
    std::cout<< "==============================================================="<<std::endl; 
    free(material);

    return ret;
}


