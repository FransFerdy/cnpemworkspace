

#include "sis_datatypes.h"
#include "sis_util.h"
#include "sis_forms.h"
#include "cJSON.h"

#include <stdlib.h>
#include <stdbool.h>
#include <stdio.h>
#include <string.h>
#include <math.h>
#include <unistd.h>
#include <errno.h>
#include <pthread.h>
#include <getopt.h>
//#include <cuda.h>
#include "cudaFun.h"


#include "material.h"

#include "sis_cpu.h"

#ifndef M_PI
#    define M_PI 3.14159265358979323846
#endif
#define CPUGPU_RATIO 23

#define HC 1.23984193
//planck*speed of light ev * micron

float *vectors[V_VECTORVECTOR_SIZE];
float memoryCopyTime=0;
float rayCastingTime=0;
float propagationTime=0;
float phantomBuildingTime=0;

bool loud=true;
bool plotting=false;
bool showGeometry=false;
bool useCPU=true;
bool singleSinogram=false;
int singleSinogramY = 0;
int maxGPU=8;
char outputFolder[1000]="./"; 
char emissionNames[EMISSION_VECTOR_SIZE][30] = { //for printing purposes only
    "pointwise","pyramidal","spherical","pyramidal hybrid"};
/*
Get 3D cube coordinate in 1D array
@x, x coordinate
@y, y coordinate
@z, z coordinate
*/
static size_t p3d(size_t x,size_t y, size_t z, size_t size)
{
    return (x+y*size+(z*size*size));// ((x*d1)+y*d2)+z;
}

/*
Get 3D coordinate in 1D array
@x, x coordinate
@y, y coordinate
@z, z coordinate
@height, dimension 1
@depth dimension 2
*/
static size_t dp3d(size_t x,size_t y, size_t z, size_t width,size_t height)
{
    return (x+y*width+(z*width*height));// ((x*d1)+y*d2)+z;
}
/*
Get 2D plane coordinate in 1D array
@x, x coordinate
@y, y coordinate
*/
static size_t p2d(size_t x,size_t y, size_t width)
{
    return (x+y*width);// ((x*d1)+y*d2)+z;
}


#define VOLUME_START_INDEX 0
#define VOLUME_MIDDLE_INDEX 1
#define VOLUME_FINAL_INDEX 2
void electValidPointsPerSlice(int *ints, float *floats, float *validPoints, int startYarg,int sliceHeight, size_t *resSize, size_t *resStart)
{
    int myPosX=0;
    int lineZ=0;
    int maxPoints = ints[V_VALIDDETECTORPOINTS]; 
    float raySamplingDouble = floats[V_RAYSAMPLINGDOUBLE];
    int raySamplingInt = ints[V_RAYSAMPLING];
    float conePosZMinusVolumeZ = floats[V_CONEPOSZ_MINUS_VOLUMEZ];
    float volumeYMinusVolumeMiddle = floats[V_VOLUMEY] - (ints[V_VOLUMEMIDDLE]*floats[V_VOXEL_SIZE]);
    float t=0,tempPoint[9];
    float volumey,volumez;
    volumey = floats[V_VOLUMEY];
    volumez = floats[V_VOLUMEZ];

    float currentY = validPoints[p2d(DETBASEY,myPosX,POINTS_VARS_AMOUNT)];
    float lastY = -11;
    //int realBottomY, realTopY;
    size_t retVal = 0;
    size_t startPoint=0;
    int pointValid=0; 
    int amountPoints=0;
    int endY;

    float effectiveVolumeZ;
    if (floats[V_VOLUMEZFINAL]>floats[V_VOLUMEZ])
        effectiveVolumeZ = floats[V_VOLUMEZFINAL];
    else
        effectiveVolumeZ = floats[V_VOLUMEZ];

    float volumex = floats[V_VOLUMEX];
    float conePosZ = floats[V_CONEPOSZ];
    float volumeMiddle = ints[V_VOLUMEMIDDLE]*floats[V_VOXEL_SIZE];
    float volumeStartZ[3];
    float volumeEndZ[3];
    float rayStartY[3],rayEndY[3];
    float tVec[9];
    float baseTVec[3];

    volumeStartZ[VOLUME_START_INDEX] = (floats[V_VOLUMEZ] + ( (float)(ints[V_VOLUMEMIDDLE]-1) * floats[V_VOXEL_SIZE]) );
    volumeEndZ[VOLUME_START_INDEX] = (floats[V_VOLUMEZ] - ( (float)(ints[V_VOLUMEMIDDLE]) * floats[V_VOXEL_SIZE]) );
    volumeStartZ[VOLUME_MIDDLE_INDEX] = ( ((floats[V_VOLUMEZ]+floats[V_VOLUMEZFINAL])/2 ) + ( (float)(ints[V_VOLUMEMIDDLE]-1) * floats[V_VOXEL_SIZE]) );
    volumeEndZ[VOLUME_MIDDLE_INDEX] = ( ((floats[V_VOLUMEZ]+floats[V_VOLUMEZFINAL])/2 ) - ( (float)(ints[V_VOLUMEMIDDLE]) * floats[V_VOXEL_SIZE]) );
    volumeStartZ[VOLUME_FINAL_INDEX] = (floats[V_VOLUMEZFINAL] + ( (float)(ints[V_VOLUMEMIDDLE]-1) * floats[V_VOXEL_SIZE]) );
    volumeEndZ[VOLUME_FINAL_INDEX] = (floats[V_VOLUMEZFINAL] - ( (float)(ints[V_VOLUMEMIDDLE]) * floats[V_VOXEL_SIZE]) );

    int startY = startYarg;
    endY = startYarg+sliceHeight;
    startY = startY*floats[V_VOXEL_SIZE];
    endY = endY*floats[V_VOXEL_SIZE];

    int volumeSize = ints[V_VOLUMESIZE]*floats[V_VOXEL_SIZE];
    while(myPosX < maxPoints)
    {
        currentY = validPoints[p2d(DETBASEY,myPosX,POINTS_VARS_AMOUNT)];
        if (currentY!=lastY)
        {
            if (pointValid==1)
            {
                retVal+=amountPoints;
            }
            else
            {
                if (retVal!=0)
                   break;
                else
                    startPoint+=amountPoints;
            }
            amountPoints=0;
            pointValid=0;
        }
        else
        {
            amountPoints++;
            if (myPosX==maxPoints-1)
            {
                if (pointValid)
                    retVal+=amountPoints;
                amountPoints=0;
                pointValid=0;
            }
            myPosX++;
            continue;
        }
        lastY = currentY;
        
        lineZ=0;
        t=0;
        baseTVec[1] = validPoints[p2d(DETREALY,myPosX,POINTS_VARS_AMOUNT)];
        baseTVec[2] = validPoints[p2d(DETREALZ,myPosX,POINTS_VARS_AMOUNT)] - conePosZ;

        t = (volumeStartZ[VOLUME_START_INDEX]-conePosZ) / baseTVec[2];
        rayStartY[VOLUME_START_INDEX] = (t*baseTVec[1]) + 0;
        t = (volumeEndZ[VOLUME_START_INDEX]-conePosZ) / baseTVec[2];
        rayEndY[VOLUME_START_INDEX] = (t*baseTVec[1]) + 0;
        tVec[1 +VOLUME_START_INDEX*3] = rayStartY[VOLUME_START_INDEX] - rayEndY[VOLUME_START_INDEX];
        tVec[2 +VOLUME_START_INDEX*3] = volumeStartZ[VOLUME_START_INDEX] - volumeEndZ[VOLUME_START_INDEX];

        t = (volumeStartZ[VOLUME_MIDDLE_INDEX]-conePosZ) / baseTVec[2];
        rayStartY[VOLUME_MIDDLE_INDEX] = (t*baseTVec[1]) + 0;
        t = (volumeEndZ[VOLUME_MIDDLE_INDEX]-conePosZ) / baseTVec[2];
        rayEndY[VOLUME_MIDDLE_INDEX] = (t*baseTVec[1]) + 0;
        tVec[1 +VOLUME_MIDDLE_INDEX*3] = rayStartY[VOLUME_MIDDLE_INDEX] - rayEndY[VOLUME_MIDDLE_INDEX];
        tVec[2 +VOLUME_MIDDLE_INDEX*3] = volumeStartZ[VOLUME_MIDDLE_INDEX] - volumeEndZ[VOLUME_MIDDLE_INDEX];

        t = (volumeStartZ[VOLUME_FINAL_INDEX]-conePosZ) / baseTVec[2];
        rayStartY[VOLUME_FINAL_INDEX] = (t*baseTVec[1]) + 0;
        t = (volumeEndZ[VOLUME_FINAL_INDEX]-conePosZ) / baseTVec[2];
        rayEndY[VOLUME_FINAL_INDEX] = (t*baseTVec[1]) + 0;
        tVec[1 +VOLUME_FINAL_INDEX*3] = rayStartY[VOLUME_FINAL_INDEX] - rayEndY[VOLUME_FINAL_INDEX];
        tVec[2 +VOLUME_FINAL_INDEX*3] = volumeStartZ[VOLUME_FINAL_INDEX] - volumeEndZ[VOLUME_FINAL_INDEX];

        //printf("tvec[1] %f tvec[2] %f tvec[4] %f tvec[5] %f tvec[7] %f, tvec[8] %f\n",
        //tVec[1], tVec[2], tVec[4], tVec[5], tVec[7], tVec[8] );
        while(lineZ<raySamplingInt)
        {
            t = lineZ / raySamplingDouble;  
            tempPoint[2 + VOLUME_START_INDEX*3] = t*tVec[2+VOLUME_START_INDEX*3] + volumeEndZ[VOLUME_START_INDEX] - volumez;//z
            tempPoint[2 + VOLUME_MIDDLE_INDEX*3] = t*tVec[2+VOLUME_MIDDLE_INDEX*3] + volumeEndZ[VOLUME_MIDDLE_INDEX] - volumez;//z
            tempPoint[2 + VOLUME_FINAL_INDEX*3] = t*tVec[2+VOLUME_FINAL_INDEX*3] + volumeEndZ[VOLUME_FINAL_INDEX] - volumez;//z

            if (ints[V_BEAMTYPE]!=B_CONE)
            {
                tempPoint[1 + VOLUME_START_INDEX*3] = validPoints[p2d(DETREALY,myPosX,POINTS_VARS_AMOUNT)] - volumeYMinusVolumeMiddle;//y
                tempPoint[1 + VOLUME_MIDDLE_INDEX*3] = tempPoint[1];
                tempPoint[1 + VOLUME_FINAL_INDEX*3] = tempPoint[1];
            }
            else
            {
                tempPoint[1 + VOLUME_START_INDEX*3] = ( (t*tVec[1+VOLUME_START_INDEX*3]) + rayEndY[VOLUME_START_INDEX] - volumey)+ volumeMiddle;//y
                tempPoint[1 + VOLUME_MIDDLE_INDEX*3] = ( (t*tVec[1+VOLUME_MIDDLE_INDEX*3]) + rayEndY[VOLUME_MIDDLE_INDEX] - volumey)+ volumeMiddle;//y
                tempPoint[1 + VOLUME_FINAL_INDEX*3] = ( (t*tVec[1+VOLUME_FINAL_INDEX*3]) + rayEndY[VOLUME_FINAL_INDEX] - volumey)+ volumeMiddle;//y
            }

            if ( (tempPoint[1]>=startY && tempPoint[2]>=0 && tempPoint[1]<endY && tempPoint[2]<volumeSize) ||
            (tempPoint[1+ VOLUME_MIDDLE_INDEX*3]>=startY && tempPoint[2+ VOLUME_MIDDLE_INDEX*3]>=0 && tempPoint[1+ VOLUME_MIDDLE_INDEX*3]<endY && tempPoint[2+ VOLUME_MIDDLE_INDEX*3]<volumeSize) ||
            (tempPoint[1+ VOLUME_FINAL_INDEX*3]>=startY && tempPoint[2+ VOLUME_FINAL_INDEX*3]>=0 && tempPoint[1+ VOLUME_FINAL_INDEX*3]<endY && tempPoint[2+ VOLUME_FINAL_INDEX*3]<volumeSize) )
            {
                pointValid = 1;
                break;
            }
            lineZ++;
        }
        
        amountPoints++;
        if (myPosX==maxPoints-1)
        {
            if (pointValid)
                retVal+=amountPoints;
            amountPoints=0;
            pointValid=0;
        }
        myPosX++;
    }
        //printf("retVal %lu, start %lu\n",retVal,startPoint);
    (*resSize) = retVal;
    (*resStart) = startPoint;
}      

//TODO make it to be like the function above ^
void electValidPointsSingleSinogram(int *ints, float *floats, float *validPoints, int startYarg,int sliceHeight, size_t *resSize, size_t *resStart,int sinogramY)
{
    int myPosX=0;
    int lineZ=0;
    int maxPoints = ints[V_VALIDDETECTORPOINTS]; 
    float raySamplingDouble = floats[V_RAYSAMPLINGDOUBLE];
    int raySamplingInt = ints[V_RAYSAMPLING];
    float conePosZMinusVolumeZ = floats[V_CONEPOSZ_MINUS_VOLUMEZ];
    float volumeYMinusVolumeMiddle = floats[V_VOLUMEY] - (ints[V_VOLUMEMIDDLE]*floats[V_VOXEL_SIZE]);
    float t=0,tempPoint[3],tempPoint2[3];
    float volumey,volumez;
    volumey = floats[V_VOLUMEY];
    volumez = floats[V_VOLUMEZ];

    float currentY = validPoints[p2d(DETBASEY,myPosX,POINTS_VARS_AMOUNT)];
    float lastY = -11;
    //int realBottomY, realTopY;
    size_t retVal = 0;
    size_t startPoint=0;
    int pointValid=0;
    int amountPoints=0;
    int endY;
    float effectiveVolumeZ;
    if (floats[V_VOLUMEZFINAL]>floats[V_VOLUMEZ])
        effectiveVolumeZ = floats[V_VOLUMEZFINAL];
    else
        effectiveVolumeZ = floats[V_VOLUMEZ];

    float volumex = floats[V_VOLUMEX];
    float conePosZ = floats[V_CONEPOSZ];
    float volumeMiddle = ints[V_VOLUMEMIDDLE]*floats[V_VOXEL_SIZE];
    float volumeStartZ = (effectiveVolumeZ + ( (float)(ints[V_VOLUMEMIDDLE]-1) * floats[V_VOXEL_SIZE]) );
    float volumeEndZ = (effectiveVolumeZ - ( (float)(ints[V_VOLUMEMIDDLE]) * floats[V_VOXEL_SIZE]) );
    float rayStartY,rayEndY;
    float tVec[3];

    int startY = startYarg;
    endY = startYarg+sliceHeight;
    startY = startY*floats[V_VOXEL_SIZE];
    endY = endY*floats[V_VOXEL_SIZE];

    int volumeSize = ints[V_VOLUMESIZE]*floats[V_VOXEL_SIZE];
    while(myPosX < maxPoints)
    {
        currentY = validPoints[p2d(DETBASEY,myPosX,POINTS_VARS_AMOUNT)];
        
        if (currentY!=lastY)
        {
            if (pointValid==1)
            {
                retVal+=amountPoints;
            }
            else
            {
                if (retVal!=0)
                   break;
                else
                    startPoint+=amountPoints;
            }
            amountPoints=0;
            pointValid=0;
        }
        else
        {
            amountPoints++;
            if (myPosX==maxPoints-1)
            {
                if (pointValid)
                    retVal+=amountPoints;
                amountPoints=0;
                pointValid=0;
            }
            myPosX++;
            continue;
        }
        lastY = currentY;
        if (currentY<sinogramY || currentY>sinogramY+1)
        {
            myPosX++;
            continue;
        }
        
        lineZ=0;
        t=0;
        tVec[1] = validPoints[p2d(DETREALY,myPosX,POINTS_VARS_AMOUNT)];
        tVec[2] = validPoints[p2d(DETREALZ,myPosX,POINTS_VARS_AMOUNT)] - conePosZ;
        t = (volumeStartZ-conePosZ) / tVec[2];
        rayStartY = (t*tVec[1]) + 0;
        t = (volumeEndZ-conePosZ) / tVec[2];
        rayEndY = (t*tVec[1]) + 0;
        tVec[1] = rayStartY - rayEndY;
        tVec[2] = volumeStartZ - volumeEndZ;

        while(lineZ<raySamplingInt)
        {
            t = lineZ / raySamplingDouble;
            tempPoint2[1] = ( (t*tVec[1]) + rayEndY - volumey)+ volumeMiddle;//y
            tempPoint[2] = t*tVec[2]+ volumeEndZ - volumez;//z

            if (ints[V_BEAMTYPE]!=B_CONE)
                tempPoint2[1] = validPoints[p2d(DETREALY,myPosX,POINTS_VARS_AMOUNT)] - volumeYMinusVolumeMiddle;//y
            
            if (tempPoint2[1]>=startY && tempPoint2[2]>=0 && tempPoint2[1]<endY && tempPoint2[2]<volumeSize)
            {
                pointValid = 1;
                break;
            }
            lineZ++;
        }
        
        amountPoints++;
        if (myPosX==maxPoints-1)
        {
            if (pointValid)
                retVal+=amountPoints;
            amountPoints=0;
            pointValid=0;
        }
        myPosX++;
    }
    (*resSize) = retVal;
    (*resStart) = startPoint;
}

void *GPUJob(void *arg)
{
    sis_TimeMeasure memoryTime;
    sis_TimeMeasure castTime;
    sis_TimeMeasure propagateTime;
    int *deviceInts;
    float *deviceVolume;
    float *deviceDetector;
    void *deviceCufftDetector;
    void *deviceKernelMatrix;
    float *deviceDetectorPixelValues;
    float *deviceDetectorImaginary=NULL;
    float *devicefloats;
    float *deviceValidPoints;
    double hsumx,hsumy;
    
    float angleNow;
    int planeIndex =0;
    int i=0;
    float angleStep;
    
    sis_DeviceConfig* deviceConfig = (sis_DeviceConfig*)arg;

    float *floats = deviceConfig->floats;
    int *ints = deviceConfig->ints;
    int myId = deviceConfig->myId;
    size_t myMemory = deviceConfig->myMemory;
    float *validPoints = deviceConfig->validPoints;
    int rotations = deviceConfig->rotations;
    float *detectorDataImaginary = deviceConfig->detector->dataImaginary;
    float *detectorData = deviceConfig->detector->data;
    float *volumeData = deviceConfig->volume->data;
    float *volumeDataImaginary = deviceConfig->volume->dataImaginary;
    pthread_mutex_t *resultMutex = deviceConfig->resultMutex;
    int gpuDevices = deviceConfig->gpuDevices; 
    float startAngle = deviceConfig->startAngle;
    float allowedMemoryConsumption = deviceConfig->allowedMemoryConsumption;
    int myJobId = deviceConfig->myJobId;
    int localStage = CPU_STAGE_MAKING_FRESNEL_KERNEL;
    int counted = 0;

    if (rotations<=1)
        angleStep = 0;
    else
        angleStep = floats[V_VOLUMEROTATIONFINAL] / ((float)rotations-1);
    startTimer(&memoryTime);
    mcudaSetDevice(myId);
    myMemory = myMemory * allowedMemoryConsumption;
    
    size_t uMemory = myMemory - (((ints[V_DETECTORPLANESIZE]*2)+(ints[V_VALIDDETECTORPOINTS]*POINTS_VARS_AMOUNT))*sizeof(float));
    if (uMemory<600000000)//600MB safety
    {
        printf("Not enough video memory, exiting. \n");
        exit(EXIT_FAILURE);
    }
    uMemory = uMemory - 600000000;//600MB safety
    size_t sliceHeight = floor(uMemory / (float)(pow(ints[V_VOLUMESIZE],2)*sizeof(float)) );

    sliceHeight = sliceHeight/2.0;

    if (sliceHeight>ints[V_VOLUMESIZE])
        sliceHeight = ints[V_VOLUMESIZE];
    int amountSlices = ceil(ints[V_VOLUMESIZE]/(float)sliceHeight);
    printf("Available Memory For Phantom Section GPU #%d %lu PhantomSliceHeight %lu #Slices %d\n",myId,uMemory,sliceHeight,amountSlices);
    //printf("Starting To Copy Parameters GPU#%d\n",myId);
    copyToDeviceMemory(ints,(void**)&deviceInts,(size_t)V_INTVECTOR_SIZE*sizeof(int));
    copyToDeviceMemory(floats,(void**)&devicefloats,(size_t)V_DOUBLEVECTOR_SIZE*sizeof(float));
    //printf("Finished Copying Parameters GPU#%d\n",myId);
    //printf("Starting To Copy Cone Surface GPU#%d\n",myId);
    copyToDeviceMemory(validPoints,(void**)&deviceValidPoints,(size_t)ints[V_VALIDDETECTORPOINTS]*(size_t)POINTS_VARS_AMOUNT*sizeof(float));
    //printf("Finished Copying Cone Surface GPU#%d\n",myId);

    //printf("Allocating Volume Section GPU#%d\n",myId);
    void *cudaarray;
    void *cudaarrayImaginary;
    cudaarray = malloc3DArrayOnDevice(ints[V_VOLUMESIZE],sliceHeight+2,ints[V_VOLUMESIZE]);  
    cudaarrayImaginary = malloc3DArrayOnDevice(ints[V_VOLUMESIZE],sliceHeight+2,ints[V_VOLUMESIZE]);
    //printf("Finished Allocating Volume Section GPU#%d\n",myId);
    //printf("Allocating OutPut Zone(Detector) GPU#%d\n",myId);
    mallocOnDevice((void**)&deviceDetector,(size_t)ints[V_DETECTORPLANESIZE]*sizeof(float));
    mallocOnDevice((void**)&deviceDetectorImaginary,(size_t)ints[V_DETECTORPLANESIZE]*sizeof(float));
    //printf("Finished Allocating OutPut Zone(Detector) GPU#%d\n",myId);

    int startY,endY;
    size_t *resSize = (size_t*)malloc(amountSlices*sizeof(size_t));
    size_t *resStart = (size_t*)malloc(amountSlices*sizeof(size_t));
    float zDelta = floats[V_VOLUMEZFINAL]-floats[V_VOLUMEZ];
    float zStep = zDelta / ((float)rotations-1);
    float zNow;
    if (myJobId==0)
        memoryCopyTime += timeDiff(&memoryTime);
    //printf("Detector Middle H %d singleSingromamY %d\n",ints[V_DETECTOR_MIDDLEHEIGHT],singleSinogramY);
    for(i=0;i<amountSlices;i++)
    {
        startY = i*sliceHeight;
        if (singleSinogram)
           electValidPointsSingleSinogram(ints,floats,validPoints,startY,sliceHeight, &resSize[i], &resStart[i],singleSinogramY);
        else
           electValidPointsPerSlice(ints,floats,validPoints,startY,sliceHeight, &resSize[i], &resStart[i]);  
    }

    int step=0;
    for(i=0;i<amountSlices;i++)
    {
        planeIndex = myJobId;
        startY = i*sliceHeight;
        endY = startY+sliceHeight;
        if (endY>ints[V_VOLUMESIZE])
            endY=ints[V_VOLUMESIZE];
        if (resSize[i]<=0)
            continue;

        startTimer(&memoryTime);
        printf("Copying Volume Section GPU#%d\n",myId);
        cpyVolumeSliceToDeviceTexture(cudaarray,startY,endY,sliceHeight,volumeData,ints[V_VOLUMESIZE]);
        cpyVolumeSliceToDeviceTexture(cudaarrayImaginary,startY,endY,sliceHeight,volumeDataImaginary,ints[V_VOLUMESIZE]);
        
        printf("Finished Copying Volume Section GPU#%d\n",myId);
        if (myJobId==0)
            memoryCopyTime += timeDiff(&memoryTime);
        while(planeIndex<rotations)
        {
            angleNow = (angleStep*planeIndex) + startAngle;
            zNow = floats[V_VOLUMEZ] + (planeIndex*zDelta);
            //if (loud)printf("Angle: %f\n",angleNow);
            //printf("Copying Detector Data GPU#%d\n",myId);
            startTimer(&memoryTime);
            memcpyToDevice(deviceDetector,&(detectorData)[dp3d(0,0,planeIndex,ints[V_DETECTORWIDTH],ints[V_DETECTORHEIGHT])],((size_t)ints[V_DETECTORWIDTH])*((size_t)ints[V_DETECTORHEIGHT])*sizeof(float));
            memcpyToDevice(deviceDetectorImaginary,&(detectorDataImaginary)[dp3d(0,0,planeIndex,ints[V_DETECTORWIDTH],ints[V_DETECTORHEIGHT])],((size_t)ints[V_DETECTORWIDTH])*((size_t)ints[V_DETECTORHEIGHT])*sizeof(float));
            if (myJobId==0)
                memoryCopyTime += timeDiff(&memoryTime);
            startTimer(&castTime);
            //printf("Finished Copying Detector Data GPU#%d\n",myId);
            if (ints[V_GRIDWIDTH]!=0 && ints[V_GRIDHEIGHT]!=0)
            {
                for(int l=-ints[V_GRIDWIDTH];l<ints[V_GRIDWIDTH];l++)
                {
                    for (int k=-ints[V_GRIDHEIGHT];k<ints[V_GRIDHEIGHT];k++)
                        cudaCalculateProjection(l,k,resStart[i],resSize[i],planeIndex,angleNow,zNow,startY,endY,sliceHeight,ints,deviceInts,floats,devicefloats,detectorData,detectorDataImaginary,deviceDetector,deviceDetectorImaginary,cudaarray,cudaarrayImaginary,deviceValidPoints,deviceConfig->detectorPixelValues);
                }
            }else
                cudaCalculateProjection(0,0,resStart[i],resSize[i],planeIndex,angleNow,zNow,startY,endY,sliceHeight,ints,deviceInts,floats,devicefloats,detectorData,detectorDataImaginary,deviceDetector,deviceDetectorImaginary,cudaarray,cudaarrayImaginary,deviceValidPoints,deviceConfig->detectorPixelValues);

            planeIndex+=gpuDevices;
            if (myJobId==0)
                rayCastingTime += timeDiff(&castTime);
        }
    }
   // validateProjection(0,0,resStart[0],resSize[0],0,0,zNow,startY,endY,sliceHeight,ints,deviceInts,floats,devicefloats,detectorData,detectorDataImaginary,deviceDetector,deviceDetectorImaginary,cudaarray,cudaarrayImaginary,deviceValidPoints,deviceConfig->detectorPixelValues);

    freeArrayOnDevice(cudaarray);
    freeArrayOnDevice(cudaarrayImaginary);

    
    pthread_mutex_lock(deviceConfig->resultMutex);
    (*(deviceConfig->stageCounter))++;
    pthread_mutex_unlock(deviceConfig->resultMutex);

    while(localStage==CPU_STAGE_MAKING_FRESNEL_KERNEL)
    {
        pthread_mutex_lock(deviceConfig->resultMutex);
        localStage = (*(deviceConfig->stage));
        pthread_mutex_unlock(deviceConfig->resultMutex);
        usleep(500);
    }

    startTimer(&memoryTime);
    size_t complexCufftDetectorSize = getSizeOfCufftComplex() * ((size_t)ints[V_DETECTORPLANESIZE])*FFT_PADDINGX*FFT_PADDINGY;

    mallocOnDevice((void**)&deviceCufftDetector, complexCufftDetectorSize);
    if (deviceConfig->sharedVars->fresnelKernel==NULL && ints[V_FRESNEL_START]!=-1)
        exit(EXIT_FAILURE);
    
    mallocOnDevice((void**)&deviceKernelMatrix,complexCufftDetectorSize);
    
    copyToDeviceMemory(deviceConfig->detectorPixelValues,(void **)&deviceDetectorPixelValues,(size_t)PV_VECTORSIZE*(size_t)ints[V_DETECTORPLANESIZE]*sizeof(float));
    if (myJobId==0)
        memoryCopyTime += timeDiff(&memoryTime);

    float zLast;
    zDelta = floats[V_VOLUMEZFINAL] - floats[V_VOLUMEZ];
    zStep = zDelta/(float)(ints[V_IMAGE_ITERATIONS]);
    zLast = 9992345;//random number initialization
    planeIndex = myJobId;
    startTimer(&propagateTime);
    while(planeIndex<rotations)
    {
        zNow = floats[V_VOLUMEZ] + (zStep*planeIndex);
        if (zNow!=zLast)
        {
            printf("Calculating Projection Kernel #%d\n",planeIndex);
            memsetInDevice((void *) deviceKernelMatrix, 0, complexCufftDetectorSize);
            cudaCalculateFresnelKernel(deviceKernelMatrix,ints,floats,zNow,&hsumx,&hsumy);
            applyForwardFFTtoCufftComplex(deviceKernelMatrix,((size_t)ints[V_DETECTORWIDTH])*FFT_PADDINGX,((size_t)ints[V_DETECTORHEIGHT])*FFT_PADDINGY);

        }
        memsetInDevice((void *) deviceCufftDetector, 0, complexCufftDetectorSize);
        cudaComposeRadiation(planeIndex,ints,floats, detectorData,detectorDataImaginary,deviceDetector,deviceDetectorImaginary,deviceCufftDetector,deviceValidPoints,deviceKernelMatrix,deviceConfig->f10Vec,deviceDetectorPixelValues,hsumx,hsumy);
        planeIndex+=gpuDevices;
        zLast=zNow;
    }
    if (myJobId==0)
        propagationTime += timeDiff(&propagateTime);

    freeOnDevice(deviceValidPoints);

    free(resSize);
    free(resStart);
    freeOnDevice(deviceDetector);
    
    freeOnDevice(deviceDetectorImaginary);
    freeOnDevice(devicefloats);
    freeOnDevice(deviceInts);
    freeOnDevice(deviceKernelMatrix);
    freeOnDevice(deviceDetectorPixelValues);

    pthread_mutex_lock(deviceConfig->resultMutex);
    (*(deviceConfig->threadCount))--;
    pthread_mutex_unlock(deviceConfig->resultMutex);
    printf("GPU#%d Finished \n",myId);
    return NULL;
}
 
void *CPUJob(void *arg)
{
    sis_DeviceConfig* deviceConfig = (sis_DeviceConfig*)arg;
    float *floats = deviceConfig->floats;
    int *ints = deviceConfig->ints;
    int rotations = deviceConfig->rotations;
    float *detectorData = deviceConfig->detector->data;
    float *volumeData = deviceConfig->volume->data;
    int *stage = deviceConfig->stage;
    int *stageCounter = deviceConfig->stageCounter;
    float startAngle = deviceConfig->startAngle;
    pthread_mutex_t *resultMutex = deviceConfig->resultMutex;
    float *f10Vec = deviceConfig->f10Vec;
    
    int localStage = 0;

    float f10alt,f10,zNow,zStep,zDelta,propDistance,zeff,z1,z2,z1_plus_z2,magnification,sourceSize,effectivePixelSize,resolution,userAInput;
    zDelta = floats[V_VOLUMEZFINAL] - floats[V_VOLUMEZ];
    zStep = zDelta/(float)(ints[V_IMAGE_ITERATIONS]);
    z1_plus_z2 = floats[V_CONEDISTANCE];
    int firstIt,lastIt;
    firstIt = -1;
    lastIt = -1;

    for(int i=0;i<ints[V_IMAGE_ITERATIONS];i++)
    {
        zNow = floats[V_VOLUMEZ] + (zStep*i);
        propDistance = (zNow*(-1))-((float)ints[V_VOLUMEMIDDLE]*floats[V_VOXEL_SIZE]);
        if (propDistance<0)
            propDistance=0;
        userAInput=10;
        z1 =  -(floats[V_CONEPOSZ]-zNow);
        z2 = -zNow;
        magnification = z1_plus_z2/z1;
        if(ints[V_BEAMTYPE]==B_PARALLEL)
        {
            zeff = propDistance;
            effectivePixelSize = floats[V_DETECTOR_PIXEL_WIDTH];
        }
        if(ints[V_BEAMTYPE]==B_CONE)
        {
            zeff = propDistance / magnification;
            effectivePixelSize = floats[V_DETECTOR_PIXEL_WIDTH]/magnification;
        }
        sourceSize = 0.1;//100 nm
        resolution = sqrt( (pow(1.0 - (1.0/magnification),2) * pow(sourceSize,2) ) + pow(effectivePixelSize, 2));
        f10 = pow((effectivePixelSize*userAInput),2) / (floats[V_LAMBDA]*zeff);
        f10alt = pow(ints[V_VOLUMESIZE]*floats[V_VOXEL_SIZE],2)/ (floats[V_LAMBDA]*zeff);
        printf("f10: %f f10obj %f zNow: %f zeff %f mag %f z1 %f z2 %f\n",f10,f10alt,zNow,zeff,magnification,z1,z2);
        if (f10<floats[V_FRESNEL_NEARCUT])// && f10>FRESNEL_START_F10REGION)
        {
            if (firstIt==-1)
                firstIt=i;
            lastIt=i;
        }
        f10Vec[i] = f10;
    }

    if (floats[V_VOLUMEZ]==floats[V_VOLUMEZFINAL])
    {
        firstIt = 0;
        lastIt = 0;
    }
    /*
    
    if (firstIt!=-1 && lastIt!=-1)//no fresnel
    {
        deviceConfig->sharedVars->fresnelKernel = doFresnelImage(ints,floats,(lastIt-firstIt)+1,firstIt);
        ints[V_FRESNEL_START]=firstIt;
        ints[V_FRESNEL_END]=lastIt+1;
        printf("First %d Last %d", ints[V_FRESNEL_START],ints[V_FRESNEL_END]);
    }
    */
    

    pthread_mutex_lock(deviceConfig->resultMutex);
    (*(deviceConfig->stageCounter))++;
    (*(deviceConfig->stage))++;
    pthread_mutex_unlock(deviceConfig->resultMutex);
    
    while(localStage!=CPU_STAGE_END)
    {
        pthread_mutex_lock(deviceConfig->resultMutex);
        if ( (*stageCounter)==(*(deviceConfig->threadCount)) )
        {
            (*(deviceConfig->stageCounter)) = 0;
            (*(deviceConfig->stage))++;
        }
        localStage = (*stageCounter);
        pthread_mutex_unlock(deviceConfig->resultMutex);
        if ( (*(deviceConfig->stage))== CPU_STAGE_END)
            break;

        if ( (*(deviceConfig->stage))== CPU_STAGE_FREEING_VOLUME)
        {
            deleteVolume(deviceConfig->volume);
            deviceConfig->volume=NULL;
            pthread_mutex_lock(deviceConfig->resultMutex);
            (*(deviceConfig->stageCounter))++;
            (*(deviceConfig->stage))++;
            pthread_mutex_unlock(deviceConfig->resultMutex);
        }
        usleep(500);
    }
    
    pthread_mutex_lock(deviceConfig->resultMutex);
    (*(deviceConfig->threadCount))--;
    pthread_mutex_unlock(deviceConfig->resultMutex);
    printf("CPU Finished \n");
    return NULL;
}



void volumeProjection(int *ints,float *floats,int maxThreads,int rotations,float startAngle,float rayPrecisionarg,sis_ConeBeam* cone, sis_Detector* detector, sis_Volume* volume,int pointMode, int *gpuArrayArg, int gpuArraySize,float allowedMemoryConsumption)
{ 
    sis_TimeMeasure deltaT;
    pthread_t threadReference;
    pthread_mutex_t resultMutex;
    pthread_mutex_init(&resultMutex, NULL);
    startTimer(&deltaT);
    float *coneProjection;
    float *detectorPixelValues;
    float *validPoints;
    int i;
    int deviceCount=0;
    float *devicePerfomanceRatio;
    int jobsPerDevice = 0;
    int cpuJobs = 0;
    int usingDevices=0;
    int threadCount=0;
    int localThreadCount=0;
    int gpuDevices=1;
    int stage = 0;
    int stageCounter = 0;
    float *f10Vec;
    char outFileName[1000];

    int myGPUArray[20];
    

    sis_DeviceConfig *devicesConfig;
    sis_SharedVars sharedVars;
    sharedVars.fresnelKernel = NULL;

    cudaPrepareCone(ints,floats,&coneProjection,&detectorPixelValues,&validPoints);

    
    size_t *dMemory;
    size_t maxMemory=0;
    deviceCount = getCudaDevicesAndMemory(&dMemory);
    gpuDevices = deviceCount;
    for(i=0;i<deviceCount;i++)
    {
        printf("Available Memory GPU#%d %lu \n",i,dMemory[i]);
    }
    devicesConfig = (sis_DeviceConfig*)malloc((deviceCount+1)*sizeof(sis_DeviceConfig));
    if (devicesConfig==NULL){printf("Malloc Failed to Allocate Memory \n");exit(EXIT_FAILURE);}

    f10Vec = (float*)malloc(rotations*sizeof(float));
    if (f10Vec==NULL)
        {printf("Malloc Failed to Allocate Memory \n");exit(EXIT_FAILURE);}
    
    jobsPerDevice = rotations/(deviceCount+1);
    if (jobsPerDevice<1)
        jobsPerDevice=1;

    for(int i=0;i<gpuArraySize;i++)
    {
        myGPUArray[i] = gpuArrayArg[i];
        if (myGPUArray[i]>deviceCount)
        {
            printf("Invalid GPU Id %d, max GPU id = %d\n",myGPUArray[i],deviceCount-1);
            exit(1);
        }

    }
    if (gpuArraySize==0)
    {
        for(int i=0;i<maxGPU;i++)
        {
            myGPUArray[i] = i;
        }
    }
    cpuJobs = 0;
    
    int myJobId = 0;
    int bottomGPU=0;
    int topGPU = ints[V_TOPGPU];
    if (gpuArraySize==0)
    {
        if (deviceCount<=maxGPU)
        {
            bottomGPU=0;
            printf("Using Max Gpu Available: %d \n",deviceCount);
        }
        else
            bottomGPU = deviceCount - maxGPU;
        
        if (topGPU==-1)
            topGPU = deviceCount-1;
        else
        {
            if (topGPU>=deviceCount)
            {
                printf("Invalid First GPU ID, exiting\n");
                exit(EXIT_FAILURE);
            }
            else
            {
                bottomGPU = topGPU - maxGPU +1;
                if (bottomGPU<0)
                    bottomGPU =0;
            }
        }
        gpuDevices = maxGPU;
        if (maxGPU>deviceCount)
            gpuDevices = deviceCount;
        if(gpuDevices<=0)
            gpuDevices=1; 
    }else
    {
        gpuDevices=gpuArraySize;
        bottomGPU=0;
        topGPU=gpuArraySize-1;
    }

    printf("===============================================================\n");

    for(i=topGPU;i>=bottomGPU;i--)
    {
        devicesConfig[i].volume = volume;
        devicesConfig[i].sharedVars = &sharedVars;
        devicesConfig[i].threadCount = &threadCount;
        devicesConfig[i].stage = &stage;
        devicesConfig[i].stageCounter = &stageCounter;
        devicesConfig[i].gpuDevices = gpuDevices;
        devicesConfig[i].resultMutex = &resultMutex;
        devicesConfig[i].rotations = rotations;
        devicesConfig[i].myMemory = dMemory[i];
        devicesConfig[i].myId = myGPUArray[i];
        devicesConfig[i].myJobId = myJobId;
        devicesConfig[i].validPoints = validPoints;
        devicesConfig[i].detector = detector;
        devicesConfig[i].startAngle = startAngle;
        devicesConfig[i].f10Vec = f10Vec;
        devicesConfig[i].floats = floats;
        devicesConfig[i].ints = ints;
        devicesConfig[i].detectorPixelValues = detectorPixelValues;
        devicesConfig[i].allowedMemoryConsumption = allowedMemoryConsumption;
        if(pthread_create(&threadReference, NULL, GPUJob, &devicesConfig[i])) 
        {
            fprintf(stderr, "Error creating thread\n");
            exit(EXIT_FAILURE);
        }else
        {
            pthread_mutex_lock(&resultMutex);
            threadCount++;
            pthread_mutex_unlock(&resultMutex);
        }
        myJobId++;
    }
    devicesConfig[deviceCount].sharedVars = &sharedVars;
    devicesConfig[deviceCount].volume = volume;
    devicesConfig[deviceCount].stage = &stage;
    devicesConfig[deviceCount].stageCounter = &stageCounter;
    devicesConfig[deviceCount].threadCount = &threadCount;
    devicesConfig[deviceCount].gpuDevices = gpuDevices;
    devicesConfig[deviceCount].resultMutex = &resultMutex;
    devicesConfig[deviceCount].rotations = rotations;
    devicesConfig[deviceCount].myId = deviceCount;
    devicesConfig[deviceCount].cpuJobs = cpuJobs;
    devicesConfig[deviceCount].validPoints = validPoints;
    devicesConfig[deviceCount].detector = detector;
    devicesConfig[deviceCount].startAngle = startAngle;
    devicesConfig[deviceCount].f10Vec = f10Vec;
    devicesConfig[deviceCount].floats = floats;
    devicesConfig[deviceCount].ints = ints;
    devicesConfig[deviceCount].detectorPixelValues = detectorPixelValues;
    devicesConfig[deviceCount].allowedMemoryConsumption = allowedMemoryConsumption;
    if(pthread_create(&threadReference, NULL, CPUJob, &devicesConfig[deviceCount])) 
    {
        fprintf(stderr, "Error creating thread\n");
        exit(EXIT_FAILURE);
    }else
    {
        pthread_mutex_lock(&resultMutex);
        threadCount++;
        pthread_mutex_unlock(&resultMutex); 
    }
    
    localThreadCount=1;
    while(localThreadCount>0)
    {
        pthread_mutex_lock(&resultMutex);
        localThreadCount = threadCount;
        pthread_mutex_unlock(&resultMutex);
        sleep(1);
    }
    finishTimer(&deltaT);
    
    
    free(devicesConfig);
    free(validPoints);
    free(detectorPixelValues);
    free(coneProjection);


    strcpy(outFileName, outputFolder);
    strcat(outFileName, "firstProjectionFrame.h5");
    saveHDF5PlaneProjection(detector,0,outFileName);

    if (singleSinogram)
    {
        sis_Detector *singleSinogramProjection = createDetector(ints[V_DETECTORWIDTH],rotations,1,0,0,0,0);
        
        float * sinogramData = singleSinogramProjection->data;
        float * detectorData = detector->data;
        for(i=0;i<rotations;i++)
        {
            memcpy(&sinogramData[p2d(0,i,ints[V_DETECTORWIDTH])],&detectorData[dp3d(0,singleSinogramY,i,ints[V_DETECTORWIDTH],ints[V_DETECTORHEIGHT])],(size_t)ints[V_DETECTORWIDTH]*sizeof(float));
        }

        strcpy(outFileName, outputFolder);
        strcat(outFileName, "projectionSinogram.h5");
        saveHDF5PlaneProjection(singleSinogramProjection,0,outFileName);
        
        if (plotting)
        {
            {
                char *pythonArgs[]={"python","python/plotsinh5.py",outFileName,NULL};
                callPython(pythonArgs);
            }
        }  
    }
    else
    {
        strcpy(outFileName, outputFolder);
        strcat(outFileName, "volumeSinogram.h5");
        saveHDF5VolumeProjection(detector, outFileName);  
    }

    if (plotting)
    {
        {
            strcpy(outFileName, outputFolder);
            strcat(outFileName, "volumeSinogram.h5");
            char *pythonArgs[]={"python","python/animateVolume.py",outFileName,NULL};
            callPython(pythonArgs);
        }
    }   
}


void print_usage() {
    printf("Usage: sis -c -t Threads -g MaxGpus -z discretization -s Samples -e Energy -f filename.h5 -d BeamDistance -p VolX,VolY,VolZ,VolZDelta -D PhantomSizePixel -P PixelWidth,PixelHeight,VoxelSize -o LateralOpening -v VerticalOpening -q -l DetectorWidth -a DetectorHeight -r NumberOfRotations \n");
    printf("-O                      Output folder, all files will be saved here\n");
    printf("-M                      Maximum GPU Memory Consumption Allowed in %% (for example -M 80 for 80%% of memory) \n");
    printf("-A                      Final Phantom Horizontal Angle in radians(Default: PI)\n");
    printf("-P                      Simulation Dimensions in microns: (input DetectorPixelWidth,DetectorPixelHeight,PhantomVoxelWidth  , default 55,55,55)\n");
    printf("-R                      Random Seed (Default: 7)\n");
    printf("-E                      Phantom definition string (e.g \"cylinder,0.4,1.57,0.72,0,0.5,Cu,Fe,Al:cylinder,0.4,1.57,-0.72,0,0.5,Cu,Br\") \n");
    printf("-e                      Energy kev (default 10 kev) \n");
    printf("-D                      Phantom Volume Dimension Size \n");
    printf("-S                      Save Phantom Volume \n");
    printf("-B                      Beam Rays Type, values: cone , parallel (Default: cone)\n");
    printf("-x                      Single Sinogram Y\n");
    printf("-w                      First GPU ID(will count from this GPU downards)\n");
    printf("-k                      Beam grid, input: Width,Height \n");
    printf("-z                      Discretization, values = nearestneighbor, trilinear (default trilinear)\n");
    printf("-c                      Chart Simulation Results\n");
    printf("-t                      Max CPU threads\n");
    printf("-g                      Max GPU used\n");
    printf("-Q                      Propagation filter, pixel emission area, possible values: spherical (not recommended), pointwise (default), pyramidal(experimental, recommended best),pyramidal_hybrid(experimenal,recommended best)\n");
    printf("-s                      Sampling per ray (# of points in a line sum, 500-1000-10000)\n");
    printf("-f                      hdf5 filename, loads 'real','complex' datasets\n");
    printf("-d                      Beam distance from detector(default=512)\n");
    printf("-p                      X,Y,Z,ZDelta position of volume center in relation to source <__ob__|(default 0,0,(VolumeSize)*4),0\n"); 
    printf("-o                      Cone beam lateral opening in Radians (default PI/6)\n");
    printf("-v                      Cone beam vertical opening in Radians (default PI/6)\n");
    printf("-q                      Quiet, no progress printing\n");
    printf("-a                      Detector height (default=512)\n");
    printf("-l                      Detector width (default=512)\n");
    printf("-r                      Number of object projection images(Default = 1)\n");
    printf("-i                      Initial Object Angle in radians\n");
    printf("-y                      Show Problem Geometry\n");
    printf("-j                      Json Object with Parameters\n");
    printf("-m                      Detector pitch Angle Variation (in Radians)\n");
    printf("-n                      Detector yaw Angle Variation (in Radians)\n");
    printf("-L                      Detector roll Angle Variation (in Radians)\n");
    printf("-b                      Use -b to roll the detector after applying yaw and pitch. (it will roll first otherwise)\n");
    printf("-h                      Show this help print\n");
}

cJSON * loadJsonFile(char * filename)
{
    ;FILE * pFile;
    long lSize;
    char * buffer;
    size_t result;
    cJSON * ret;
    const char * jsonErr = NULL;
    pFile = fopen ( filename , "rb" );
        if (pFile==NULL) {fputs ("Opening Json File error",stderr); exit (1);}
    fseek (pFile , 0 , SEEK_END);
    lSize = ftell (pFile);
    rewind (pFile);
    buffer = (char*) malloc (sizeof(char)*lSize);
        if (buffer == NULL) {fputs ("Memory Allocation error",stderr); exit (2);}
    result = fread (buffer,1,lSize,pFile);
    if (result != lSize) {fputs ("File Reading error",stderr); exit (3);}
    fclose (pFile);  
    ret = cJSON_Parse(buffer);
    jsonErr =cJSON_GetErrorPtr();
    if (jsonErr!=NULL)
        printf("Error Parsing JSON At: %s\n",jsonErr);  
    free (buffer);
    return ret;
}

int main(int argc,  char** argv)
{
    int ints[V_INTVECTOR_SIZE];
    float floats[V_DOUBLEVECTOR_SIZE];
    char posInput[1000]; 
    char outFileName[1000];
    int gpuArray[20];
    int gpuArraySize=0;
    sis_TimeMeasure totalExecution;
    sis_TimeMeasure phantomTime;
    startTimer(&totalExecution);
    
    int maxThreads,detectorH=512,detectorW=512,rotations=1,volSize=128, pointMode=PT_TRILINEAR, detectorRollMode=ROLL_FIRST;
    float coneDistance=512,volX=-1, volY=-1, volZ=-1,sampling=1000,coneLateralOpening=1.0,coneVerticalOpening=1.0,openingAngleLateral=0.52359877559,openingAngleVertical=0.52359877559,
    initialObjectAngle=0, detectorYZAngle=0,detectorXZAngle=0,detectorXYAngle=0;
    char* filename = NULL;
    char c;
    cJSON *jsonin =NULL; 
    char *jsonPrint;
    FILE * pFile;

    int phantomAmount=0;
    int elemAmount[20];
    char elements[20][100][12];
    float *elementsIndexes[100];
    size_t phantomVolumeSize=128;
    bool savePhantom=false;
    long int randomSeed = 7;
    float allowedMemoryConsumption=0.95;
    
    ints[V_GRIDWIDTH]=0;
    ints[V_GRIDHEIGHT]=0;
    ints[V_BEAMTYPE] = B_CONE;
    floats[V_PROPAGATION_DISTANCE] = 0;
    ints[V_POINTS_PER_PIXEL] = 1;
    floats[V_DETECTOR_PIXEL_WIDTH] = 55;
    floats[V_DETECTOR_PIXEL_HEIGHT] = 55;
    floats[V_VOXEL_SIZE] = 35;
    floats[V_VOLUMEROTATIONFINAL] = M_PI;
    ints[V_FRESNEL_START]=-1;
    ints[V_FRESNEL_END]=-1;
    ints[V_PROP_EMISSION_TYPE]=E_POINTWISE;
    sis_TimeMeasure deltaT;
    float energy = 10;
    
    maxThreads = util_getCores();
    //maxThreads = maxThreads/2;
    if (maxThreads<1)
        maxThreads=1;
    ints[V_TOPGPU] = -1;

    pFile = fopen ( "asciiart.txt" , "r" );
    if (pFile!=NULL)
    {
        int count=0;
        char c='a';
        while (c != EOF)
        {
            c = fgetc (pFile);
            if (c!=EOF)
                printf("%c",c);
            count++;
            if (count==772)
                break;
        };
        fclose(pFile);
        printf("\n");
    }


    srand(randomSeed);
    while (optind < argc) 
    {
        if ((c = getopt(argc, argv, "t:s:L:f:p:d:o:qcbSyG:a:l:r:hv:j:i:m:n:z:g:e:x:w:k:B:R:E:D:P:A:Q:O:M:")) != -1) 
        {
            // Option argument
            switch (c)
            {
                case 'O':
                    strcpy(outputFolder,optarg);
                    strcat(outputFolder,"/");
                break;
                case 'M':
                     allowedMemoryConsumption = (float)atof(optarg);
                     allowedMemoryConsumption = allowedMemoryConsumption/100.0;
                break;
                case 'z': 
                if (strcmp(optarg,"nearestneighbor")==0)
                    pointMode=PT_NEAREST_NEIGHBOR;
                if (strcmp(optarg,"trilinear")==0)
                    pointMode=PT_TRILINEAR;
                break;
                case 'A': floats[V_VOLUMEROTATIONFINAL] = atof(optarg);
                break;
                case 'B': 
                if (strcmp(optarg,"cone")==0)
                    ints[V_BEAMTYPE] = B_CONE;
                if (strcmp(optarg,"parallel")==0)
                    ints[V_BEAMTYPE] = B_PARALLEL;
                break;
                case 'Q': 
                if (strcmp(optarg,"pointwise")==0)
                    ints[V_PROP_EMISSION_TYPE] = E_POINTWISE;
                if (strcmp(optarg,"pyramidal")==0)
                    ints[V_PROP_EMISSION_TYPE] = E_PYRAMIDAL;
                if (strcmp(optarg,"spherical")==0)
                    ints[V_PROP_EMISSION_TYPE] = E_SPHERICAL;
                if (strcmp(optarg,"pyramidal_hybrid")==0)
                    ints[V_PROP_EMISSION_TYPE] = E_PYRAMIDAL_POINTWISE;
                break;
                case 'G': 
                    ;
                    {             
                        int argcounter = 0;
                        int pos=0,lastPos=0;
                        char* initialPoint=optarg;
                        for(pos=0; optarg[pos]!='\0';pos++)
                        {
                            if (optarg[pos]==',' || optarg[pos+1]=='\0')
                            {
                                strcpy(posInput,initialPoint);
                                if (pos!=0)
                                    posInput[pos-lastPos] = '\0';
                                lastPos=pos;
                                gpuArray[argcounter] =atoi(posInput);
                                argcounter++;
                                initialPoint = &optarg[pos+1]; 
                            }
                        }
                        gpuArraySize = argcounter;     
                        
                        if (argcounter==0)
                        {
                            printf("You need to specify at least one gpu with -G. You specified: %d \n",argcounter);
                            exit(1);             
                        }
                    }
                break;
                case 't': maxThreads = (int)atoi(optarg);
                break;
                case 'D': phantomVolumeSize = (int)atoi(optarg);
                break;
                case 'R': randomSeed = atoi(optarg); srand(randomSeed);
                break;
                case 'w': ints[V_TOPGPU] = (int)atoi(optarg);
                break;
                case 'f': filename = optarg;
                break;
                case 'd': coneDistance = atoi(optarg);
                break;
                case 'x': 
                    singleSinogram = true;
                    singleSinogramY = atoi(optarg);
                break;
                case 'o': openingAngleLateral = atof(optarg);
                break;
                case 'e': energy = atof(optarg);
                break;
                case 'v': openingAngleVertical = atof(optarg);
                break;
                case 'i': initialObjectAngle = atof(optarg);
                break;
                case 'm': detectorYZAngle = atof(optarg);
                break;
                case 'n': detectorXZAngle = atof(optarg);
                break;
                case 'g': maxGPU = atoi(optarg);
                break;
                case 'S': savePhantom=true;
                break;
                case 'L': detectorXYAngle = atof(optarg);
                break;
                case 'b': detectorRollMode = ROLL_LATER;
                break;
                case 'q': loud=false;
                break;
                case 'c': plotting=true;
                break;
                case 'a': detectorH=atoi(optarg);
                break;
                case 'l': detectorW=atoi(optarg);
                break;
                case 'r': rotations=atoi(optarg);
                break;
                case 's': 
                        sampling=atof(optarg);
                break;
                case 'y': showGeometry=true;
                break;
                case 'h': print_usage(); exit(0);
                break;
                case 'j': 
                    jsonin = loadJsonFile(optarg);
                break;
                case 'p' :
                    ;             
                    int countv = 0;
                    int pos=0,lastPos=0;
                    char* initialPoint=optarg;
                    for(pos=0; optarg[pos]!='\0';pos++)
                    {
                        if (optarg[pos]==',' || optarg[pos+1]=='\0')
                        {
                            strcpy(posInput,initialPoint);
                            posInput[pos-lastPos] = '\0';
                            lastPos=pos;
                            if (countv==0)
                                volX = atof(posInput);
                            if (countv==1)
                                volY = atof(posInput);
                            if (countv==2)
                                volZ = atof(posInput);
                            if (countv==3)
                                floats[V_VOLUMEZFINAL] = atof(posInput);
                            countv++;
                            initialPoint = &optarg[pos+1]; 
                        }
                    }     
                    if (countv!=4)
                    {
                        printf("%d,Wrong number of points X,Y,Z,ZFinal in argument p \n",countv);
                        exit(1);             
                    }
                break;
                case 'P' :
                    ;     
                        {        
                        int countv = 0;
                        int pos=0,lastPos=0;
                        char* initialPoint=optarg;
                        for(pos=0; optarg[pos]!='\0';pos++)
                        {
                            if (optarg[pos]==',' || optarg[pos+1]=='\0')
                            {
                                strcpy(posInput,initialPoint);
                                posInput[pos-lastPos] = '\0';
                                lastPos=pos;
                                if (countv==0)
                                    floats[V_DETECTOR_PIXEL_WIDTH] = atof(posInput);
                                if (countv==1)
                                    floats[V_DETECTOR_PIXEL_HEIGHT] = atof(posInput);
                                if (countv==2)
                                    floats[V_VOXEL_SIZE] = atof(posInput);
                                countv++;
                                initialPoint = &optarg[pos+1]; 
                            }
                        }     
                        if (countv!=3)
                        {
                            printf("%d,Wrong number of dimensions DetectorPixelWidth,DetectorPixelHeight,VoxelWidth in argument P \n",countv);
                            exit(1);             
                        }
                    }
                break;
                case 'E' :
                {
                    ;             
                    int countv = 0;
                    int object=0;
                    int pos=0,lastPos=0;
                    char* initialPoint=optarg;
                    phantomAmount=1;
                    for(pos=0; true;pos++)
                    {
                        if (optarg[pos]==',' || optarg[pos]=='\0' ||  optarg[pos]==':' )
                        {
                            strcpy(posInput,initialPoint);
                            posInput[pos-lastPos] = '\0';
                            lastPos=pos+1;
                            strcpy(elements[object][countv],posInput);
                            countv++;
                            initialPoint = &optarg[lastPos]; 
                            if (optarg[pos]==':')
                            {
                                phantomAmount++;
                                elemAmount[object] = countv;
                                object++;
                                countv=0;
                            }
                        }
                        if ( optarg[pos]=='\0')
                            break;
                    }    
                    elemAmount[object] = countv;
                break;
                }
                case 'k' :
                {
                    ;             
                    int countv = 0;
                    int pos=0,lastPos=0;
                    char* initialPoint=optarg;
                    for(pos=0; optarg[pos]!='\0';pos++)
                    {
                        if (optarg[pos]==',' || optarg[pos+1]=='\0')
                        {
                            strcpy(posInput,initialPoint);
                            posInput[pos-lastPos] = '\0';
                            lastPos=pos;
                            if (countv==0)
                                ints[V_GRIDWIDTH] = atoi(posInput)/2;
                            if (countv==1)
                                ints[V_GRIDHEIGHT] = atoi(posInput)/2;
                            countv++;
                            initialPoint = &optarg[pos+1]; 
                        }
                    }     
                    if (countv!=2)
                    {
                        printf("%d,Wrong number of points W,H in argument k \n",countv);
                        exit(1);             
                    }
                break;
                }
            }
        }
    }


    
    if (jsonin!=NULL)
    {
        cJSON *param;
        jsonPrint = cJSON_Print(jsonin);
        if (jsonPrint!=NULL)
            printf("Json Input %s\n",jsonPrint);
        param = cJSON_GetObjectItemCaseSensitive(jsonin, "chart");
        if (cJSON_IsTrue(param))
            plotting = true;
        param = cJSON_GetObjectItemCaseSensitive(jsonin, "save_phantom");
        if (cJSON_IsTrue(param))
            savePhantom = true;
        param = cJSON_GetObjectItemCaseSensitive(jsonin, "silent");
        if (cJSON_IsTrue(param))
            loud = false;
        param = cJSON_GetObjectItemCaseSensitive(jsonin, "chart_beam_only");
        if (cJSON_IsTrue(param))
            showGeometry = true;
        param = cJSON_GetObjectItemCaseSensitive(jsonin, "threads");
        if (cJSON_IsNumber(param))
            maxThreads = param->valueint;
        param = cJSON_GetObjectItemCaseSensitive(jsonin, "ray_sampling");
        if (cJSON_IsNumber(param))
            sampling = param->valueint;
        param = cJSON_GetObjectItemCaseSensitive(jsonin, "volumehdf5");
        if (cJSON_IsString(param) && param->valuestring!=NULL)
            filename = param->valuestring;
        
        param = cJSON_GetObjectItemCaseSensitive(jsonin, "discretization");
        if (cJSON_IsString(param) && param->valuestring!=NULL)
        {
            if (strcmp(param->valuestring,"nearestneighbor")==0)
                pointMode=PT_NEAREST_NEIGHBOR;
            if (strcmp(param->valuestring,"trilinear")==0)
                pointMode=PT_TRILINEAR;
        }

        param = cJSON_GetObjectItemCaseSensitive(jsonin, "output_folder");
        if (cJSON_IsString(param) && param->valuestring!=NULL)
        {
            char *originalString = param->valuestring;
            strcpy(outputFolder,originalString);
            strcat(outputFolder,"/");
        }

        

        param = cJSON_GetObjectItemCaseSensitive(jsonin, "propagation_emission_type");
        if (cJSON_IsString(param) && param->valuestring!=NULL)
        {
            if (strcmp(param->valuestring,"pointwise")==0)
                    ints[V_PROP_EMISSION_TYPE] = E_POINTWISE;
            if (strcmp(param->valuestring,"pyramidal")==0)
                ints[V_PROP_EMISSION_TYPE] = E_PYRAMIDAL;
            if (strcmp(param->valuestring,"spherical")==0)
                ints[V_PROP_EMISSION_TYPE] = E_SPHERICAL;
            if (strcmp(param->valuestring,"pyramidal_hybrid")==0)
                ints[V_PROP_EMISSION_TYPE] = E_PYRAMIDAL_POINTWISE;
        }
        
        param = cJSON_GetObjectItemCaseSensitive(jsonin, "maximum_allowed_gpu_memory");
        if (cJSON_IsNumber(param))
        {
            allowedMemoryConsumption = param->valuedouble;
            allowedMemoryConsumption = allowedMemoryConsumption/100;
        }
        

        param = cJSON_GetObjectItemCaseSensitive(jsonin, "volume_x");
        if (cJSON_IsNumber(param))
            volX = param->valuedouble;
        param = cJSON_GetObjectItemCaseSensitive(jsonin, "volume_y");
        if (cJSON_IsNumber(param))
            volY = param->valuedouble;
        param = cJSON_GetObjectItemCaseSensitive(jsonin, "volume_z");
        if (cJSON_IsNumber(param))
            volZ = param->valuedouble;
        param = cJSON_GetObjectItemCaseSensitive(jsonin, "volume_z_movedelta");
        if (cJSON_IsNumber(param))
            floats[V_VOLUMEZFINAL] = param->valuedouble;

        param = cJSON_GetObjectItemCaseSensitive(jsonin, "beam_distance");
        if (cJSON_IsNumber(param))
            coneDistance = param->valuedouble;
        param = cJSON_GetObjectItemCaseSensitive(jsonin, "beam_lateral_opening_angle");
        if (cJSON_IsNumber(param))
            openingAngleLateral = param->valuedouble;
        param = cJSON_GetObjectItemCaseSensitive(jsonin, "final_frame_angle");
        if (cJSON_IsNumber(param))
            floats[V_VOLUMEROTATIONFINAL] = param->valuedouble;
        param = cJSON_GetObjectItemCaseSensitive(jsonin, "beam_vertical_opening_angle");
        if (cJSON_IsNumber(param))
            openingAngleVertical = param->valuedouble;
        param = cJSON_GetObjectItemCaseSensitive(jsonin, "detector_height_pixels");
        if (cJSON_IsNumber(param))
            detectorH = param->valueint;
        param = cJSON_GetObjectItemCaseSensitive(jsonin, "detector_width_pixels");
        if (cJSON_IsNumber(param))
            detectorW = param->valueint;

        param = cJSON_GetObjectItemCaseSensitive(jsonin, "detector_pixel_height_microns");
        if (cJSON_IsNumber(param))
            floats[V_DETECTOR_PIXEL_HEIGHT] = param->valuedouble;
        param = cJSON_GetObjectItemCaseSensitive(jsonin, "detector_pixel_width_microns");
        if (cJSON_IsNumber(param))
            floats[V_DETECTOR_PIXEL_WIDTH] = param->valuedouble;
        param = cJSON_GetObjectItemCaseSensitive(jsonin, "voxel_size_microns");
        if (cJSON_IsNumber(param))
            floats[V_VOXEL_SIZE] = param->valuedouble;
        
        param = cJSON_GetObjectItemCaseSensitive(jsonin, "frame_sampling");
        if (cJSON_IsNumber(param))
            rotations = param->valueint;
        param = cJSON_GetObjectItemCaseSensitive(jsonin, "volume_initial_rotation");
        if (cJSON_IsNumber(param))
            initialObjectAngle = param->valuedouble;  
        param = cJSON_GetObjectItemCaseSensitive(jsonin, "detector_pitch");
        if (cJSON_IsNumber(param))
            detectorYZAngle = param->valuedouble;  
        param = cJSON_GetObjectItemCaseSensitive(jsonin, "detector_yaw");
        if (cJSON_IsNumber(param))
            detectorXZAngle = param->valuedouble; 
        param = cJSON_GetObjectItemCaseSensitive(jsonin, "detector_roll");
        if (cJSON_IsNumber(param))
            detectorXYAngle = param->valuedouble;      
        param = cJSON_GetObjectItemCaseSensitive(jsonin, "detector_roll_later");
        if (cJSON_IsTrue(param))
            detectorRollMode = ROLL_LATER;
        param = cJSON_GetObjectItemCaseSensitive(jsonin, "max_gpus");
        if (cJSON_IsNumber(param))
            maxGPU = param->valueint;     
        param = cJSON_GetObjectItemCaseSensitive(jsonin, "gpu_only");
        if (cJSON_IsTrue(param))
            useCPU = false;
        param = cJSON_GetObjectItemCaseSensitive(jsonin, "single_sinogram_y");
        if (cJSON_IsNumber(param))
        {
            singleSinogram = true;
            singleSinogramY = param->valueint; 
        }
        param = cJSON_GetObjectItemCaseSensitive(jsonin, "randomSeed");
        if (cJSON_IsNumber(param))
        {
            randomSeed = param->valueint; 
            srand(randomSeed);
        }
        param = cJSON_GetObjectItemCaseSensitive(jsonin, "gpu_id");
        if (cJSON_IsNumber(param))
            ints[V_TOPGPU] = param->valueint;
        param = cJSON_GetObjectItemCaseSensitive(jsonin, "beam_source_width");
        if (cJSON_IsNumber(param))
            ints[V_GRIDWIDTH] = param->valueint/2;
        param = cJSON_GetObjectItemCaseSensitive(jsonin, "beam_source_height");
        if (cJSON_IsNumber(param))
            ints[V_GRIDHEIGHT] = param->valueint/2;
        param = cJSON_GetObjectItemCaseSensitive(jsonin, "beam_type");
        if (cJSON_IsString(param) && param->valuestring!=NULL)
        {
            if (strcmp(param->valuestring,"cone")==0)
                ints[V_BEAMTYPE] = B_CONE;
            if (strcmp(param->valuestring,"parallel")==0)
                ints[V_BEAMTYPE] = B_PARALLEL;
        } 

        param = cJSON_GetObjectItemCaseSensitive(jsonin, "phantom_volume_size");
        if (cJSON_IsNumber(param))
            phantomVolumeSize = param->valueint;

        param = cJSON_GetObjectItemCaseSensitive(jsonin, "beam_energy");
        if (cJSON_IsNumber(param))
            energy = param->valuedouble;

        param = cJSON_GetObjectItemCaseSensitive(jsonin, "gpu_array_string");
        if (cJSON_IsString(param) && param->valuestring!=NULL)
        {
            char* initialPoint=param->valuestring;
            char *originalString = param->valuestring;
            {             
                int argcounter = 0;
                int pos=0,lastPos=0;
                char* initialPoint=originalString;
                for(pos=0; originalString[pos]!='\0';pos++)
                {
                    if (originalString[pos]==',' || originalString[pos+1]=='\0')
                    {
                        strcpy(posInput,initialPoint);
                        if (pos!=0)
                            posInput[pos-lastPos] = '\0';
                        lastPos=pos;
                        gpuArray[argcounter] =atoi(posInput);
                        argcounter++;
                        initialPoint = &originalString[pos+1]; 
                    }
                }
                gpuArraySize = argcounter;     
                
                if (argcounter==0)
                {
                    printf("You need to specify at least one gpu with gpu_array_string. You specified: %d \n",argcounter);
                    exit(1);             
                }
            }
        }  

        
        param = cJSON_GetObjectItemCaseSensitive(jsonin, "phantom_objects");
        if (cJSON_IsString(param) && param->valuestring!=NULL)
        {
            {
                ;
                int countv = 0;
                int object=0;
                int pos=0,lastPos=0;
                char* initialPoint=param->valuestring;
                char *originalString = param->valuestring;
                phantomAmount=1;
                for(pos=0; true;pos++)
                {
                    if (originalString[pos]==',' || originalString[pos]=='\0' ||  originalString[pos]==':' )
                    {
                        strcpy(posInput,initialPoint);
                        posInput[pos-lastPos] = '\0';
                        lastPos=pos+1;
                        strcpy(elements[object][countv],posInput);
                        countv++;
                        initialPoint = &originalString[lastPos]; 
                        if (originalString[pos]==':')
                        {
                            phantomAmount++;
                            elemAmount[object] = countv;
                            object++;
                            countv=0;
                        }
                    }
                    if ( originalString[pos]=='\0')
                        break;
                }    
                elemAmount[object] = countv;
            }
        }  
    }

    double lambda = (HC/energy)*1E-3;//keep micrometers, kev to ev
    floats[V_LAMBDA] = lambda;
    floats[V_WAVEK] = 2*M_PI/floats[V_LAMBDA];

/*
    char fe[3] = "Fe";
    elementsIndexes[0] = getMaterialRefractiveIndexAtEnergy(fe,energy,true);
    floats[V_LAMBDA] = elementsIndexes[0][2] * 0.001;//multiply to have number in micrometers
    floats[V_WAVEK] = 2*M_PI/floats[V_LAMBDA];
    free(elementsIndexes[0]);
*/
    


    if (rotations<1)
        rotations=1;
    

    if (maxGPU<1)
        maxGPU=1;

    sis_Volume* volume;
    sis_Detector* detector;
    
    if (filename==NULL)
    { 
        startTimer(&phantomTime);
        if (elemAmount[0]==0)
        {
            printf("You must specify the volume elements, with -E option, read -h for details.\n");
            exit(0);
        }
        printf("===============================================================\n");
        printf("----Building Phantom----\n");
        startTimer(&deltaT);
        volume = createVolumeComplex(phantomVolumeSize,volX,volY,volZ);
        volSize = phantomVolumeSize;
        printf("Phantom Objects Amount %i\n",phantomAmount);
        int realElemAmount=0;
        for (int k=0; k<phantomAmount;k++)
        {
            realElemAmount = elemAmount[k]-6;
            for (int i=6;i<elemAmount[k];i++)
            {
                elementsIndexes[i-6] = getMaterialRefractiveIndexAtEnergy(elements[k][i],energy,true);
            }
            if (strcmp(elements[k][0],"torus")==0)
                makeDonutsVolumeComplex(volume,0.9*atof(elements[k][1]), 0.2*atof(elements[k][1]),atof(elements[k][2]),atof(elements[k][3]),atof(elements[k][4]),atof(elements[k][5]),realElemAmount,elementsIndexes,maxThreads,randomSeed);
            if (strcmp(elements[k][0],"sphere")==0)
                makeSphereVolumeComplex(volume,0.9*atof(elements[k][1]),atof(elements[k][2]),atof(elements[k][3]),atof(elements[k][4]),atof(elements[k][5]),realElemAmount,elementsIndexes,maxThreads,randomSeed);
            if (strcmp(elements[k][0],"cube")==0)
                makeCubeVolumeComplex(volume,0.9*atof(elements[k][1]),atof(elements[k][2]),atof(elements[k][3]),atof(elements[k][4]),atof(elements[k][5]),realElemAmount,elementsIndexes,maxThreads,randomSeed);
            if (strcmp(elements[k][0],"disc")==0)
                makeDiscVolumeComplex(volume,0.9*atof(elements[k][1]), 0.4*atof(elements[k][1]),atof(elements[k][2]),atof(elements[k][3]),atof(elements[k][4]),atof(elements[k][5]),realElemAmount,elementsIndexes,maxThreads,randomSeed);
            if (strcmp(elements[k][0],"cylinder")==0)
                makeDiscVolumeComplex(volume,0.1*atof(elements[k][1]), 1*atof(elements[k][1]),(M_PI/2)+atof(elements[k][2]),atof(elements[k][3]),atof(elements[k][4]),atof(elements[k][5]),realElemAmount,elementsIndexes,maxThreads,randomSeed);
                    
            for(int i=0; i<realElemAmount;i++)
                free(elementsIndexes[i]);
        }

        finishTimer(&deltaT);
        phantomBuildingTime+= timeDiff(&phantomTime);

        printf("Finished Building Phantom\n");
        if (savePhantom)
        {
            printf("Saving Phantom.h5\n");
            strcpy(outFileName, outputFolder);
            strcat(outFileName, "phantom.h5");
            saveHDF5Volume(volume, outFileName);
        }
        printf("===============================================================\n");

    }


    if (filename!=NULL)
    {
        if(loud)printf("Loading %s\n",filename);
        volume = loadHDF5floatsVolume(filename,1);
        volSize = volume->size;
    }
    if (volSize>2050)
    {
        size_t memoryUsage = (size_t)(pow((size_t)volSize,3) *(size_t)2* (size_t)sizeof(float) )/(size_t)10E9;
        printf("----WARNING- TOO MANY RESOURCES NEEDED TO PERFORM SIMULATION\n");
        printf("----WARNING- THIS SIMULATION MAY LAG THE MACHINE\n");  
        printf("----WARNING- DO NOT ALLOW A 100%% GPU MEMORY USAGE FOR THIS SIMULATION, SEE -M \n");  
        printf("----WARNING- THIS SIMULATION WILL USE MORE THAN 70 GBs of MEMORY\n");  
        printf("----WARNING- YOU HAVE 5 SECONDS TO CANCEL THE SIMULATION BY PRESSING CTRL+C\n");
        printf("----this warning will appear any time a phantom volume bigger than 2050^3 voxels in size is used.\n");
        sleep(5);
    }
    
    if (volX==-1 && volY==-1 && volZ==-1)
    {
        if(loud)printf("Using Default X,Y,Z\n");
        if (filename==NULL)
            volSize = 128;
        volX = 0; volY=0; volZ= -coneDistance + (volSize*floats[V_VOXEL_SIZE]*4);
    }
    volY = volY*-1;
    volZ = -coneDistance + volZ;

    
    
    volume->pos[0] = volX;
    volume->pos[1] = volY;
    volume->pos[2] = volZ;
    floats[V_VOLUMEX] = volX;
    floats[V_VOLUMEY] = volY;
    floats[V_VOLUMEZ] = volZ;
    floats[V_VOLUMEZFINAL] = floats[V_VOLUMEZFINAL] + floats[V_VOLUMEZ];
    ints[V_VOLUMESIZE] = volume->size;
    ints[V_VOLUMEMIDDLE] = volume->middle;
    vectors[V_VOLUMEDATA] = volume->data;
    floats[V_GRIDPOINTS]=ints[V_GRIDHEIGHT]*ints[V_GRIDWIDTH]*4;//grid height and grid widthare divided by two in the begging, since they go from -val to +val

    if (floats[V_GRIDPOINTS]==0)
        floats[V_GRIDPOINTS] = 1;

    if(loud)
    {
        printf("===============================================================\n");
        printf("----Computational----\n");
        printf("Amount of Projections %d\n",rotations);
        if (gpuArraySize==0)
        {
        if (maxGPU!=8)
            printf("GPUs(desired): %d\n",maxGPU);
        else
            printf("GPUs(desired): Max\n");
        if (ints[V_TOPGPU]!=-1)
            printf("First GPU ID(desired): %d\n",ints[V_TOPGPU]);
        else
            printf("First GPU ID(desired): Last GPU\n");
        }else
        {
            for(int i=0;i<gpuArraySize;i++)
            {
                printf("Using GPU: #%d\n",gpuArray[i]);
            }
        }
        printf("Max CPU Threads %d\n",maxThreads);
        printf("Random Seed %li\n",randomSeed);
        if (pointMode==PT_NEAREST_NEIGHBOR)
            printf("Discretization Nearest Neighbor\n");
        if (pointMode==PT_TRILINEAR)
            printf("Discretization Trilinear\n");
        if (singleSinogram)
            printf("Single Sinogram: Yes | Detector Y: %d\n",singleSinogramY);
        else
        printf("Single Sinogram: No\n");
        if (showGeometry)
            printf("Will Print Beam/Object Geometry");
        printf("Result Output Folder %s\n",outputFolder);
        printf("===============================================================\n");
        printf("----Beam----\n");
        printf("Lambda %f microns | Energy %f kev\n",lambda,energy);
        printf("Ray Sampling %g\n",sampling);
        printf("Distance from Detector %f microns\n",coneDistance);
        printf("Lateral Opening %f radians| Vertical Opening %f radians\n",openingAngleLateral,openingAngleVertical);
        printf("Source Grid Width %d | Source Grid Height %d\n",ints[V_GRIDWIDTH],ints[V_GRIDHEIGHT]);
        if (ints[V_BEAMTYPE]==B_CONE)
            printf("Type Conical\n");
        if (ints[V_BEAMTYPE]==B_PARALLEL)
            printf("Type Parallel\n");
        printf("Propagation Emission Type: %s \n",emissionNames[ints[V_PROP_EMISSION_TYPE]]);
        printf("===============================================================\n");
        printf("----Phantom Volume----\n");
        float physicalVolumesize = floats[V_VOXEL_SIZE]*volSize;
        float distanceFromSource = volZ + coneDistance;
        float movingDelta = floats[V_VOLUMEZFINAL] - floats[V_VOLUMEZ];
        printf("Discrete Size %d cubic voxels\n",volSize);
        printf("Voxel Size %f cubic microns\n",floats[V_VOXEL_SIZE]);
        printf("Physical Size %f cubic microns\n",physicalVolumesize);
        printf("Starting XZ(Yaw) Angle %f radians\n",initialObjectAngle);
        printf("X %f Y %f Z %f in microns\n",volX,volY,volZ);
        printf("Starting Distance from Detector |%f| microns\n",volZ);
        printf("Starting Distance from Beam Source |%f| microns\n",distanceFromSource);
        printf("Moving Delta |%f| microns\n",movingDelta);
        printf("Final Z |%f| microns\n",floats[V_VOLUMEZFINAL]);
        printf("Final XZ Angle |%f| radians\n",floats[V_VOLUMEROTATIONFINAL]);
        printf("===============================================================\n");
        printf("----Detector----\n");
        printf("Discrete Width %d | Height %d pixels\n",detectorW,detectorH);
        printf("Pixel Width %f    | Height %f microns\n",floats[V_DETECTOR_PIXEL_WIDTH],floats[V_DETECTOR_PIXEL_HEIGHT]);
        size_t totalAmountOfPixelsInDetector = (size_t)detectorW*(size_t)detectorH;
        printf("Total Amount of Pixels: %lu\n",totalAmountOfPixelsInDetector);
        printf("Physical Width %f | Height %f microns\n",detectorW*floats[V_DETECTOR_PIXEL_WIDTH],detectorH*floats[V_DETECTOR_PIXEL_HEIGHT]);
        printf("Yaw %f | Pitch %f | Roll %f radians\n",detectorXZAngle,detectorYZAngle,detectorXYAngle);
        if (detectorRollMode==ROLL_LATER)
        printf("RollMode: Roll Later\n");
        else
            printf("RollMode: Roll First\n");
        printf("===============================================================\n");
    }

    if(loud)printf("Creating Detector \n");

    detector = createDetectorComplex(detectorW,detectorH,rotations,detectorXZAngle,detectorYZAngle,detectorXYAngle,detectorRollMode);
    ints[V_DETECTORWIDTH] = detector->width;
    ints[V_DETECTORHEIGHT] = detector->height;
    ints[V_DETECTORPLANESIZE] = detector->width*detector->height;
    ints[V_DETECTORDEPTH] = detector->depth;
    ints[V_DETECTOR_MIDDLEWIDTH] = detector->middleW;
    ints[V_DETECTOR_MIDDLEHEIGHT] = detector->middleH;
    ints[V_DETECTOR_ROLLMODE] = detector->rollMode;
    floats[V_DETECTORPITCH] = detector->pitch;
    floats[V_DETECTORYAW] = detector->yaw;
    floats[V_DETECTORROLL] = detector->roll;
    vectors[V_DETECTORDATA] = detector->data;

    ints[V_SHOW_GEOMETRY] = showGeometry;
    
    if(loud)
    {
        printf("===============================================================\n");
        printf("Creating Cone Beam \n");
    }
    
    floats[V_CONE_LATERAL_OPENING] = openingAngleLateral;

    coneLateralOpening = (openingAngleLateral * coneDistance) /2;
    coneVerticalOpening = (openingAngleVertical * coneDistance) /2;
    
    if(loud)printf("Beam Width %f Beam Height %f at Origin (Detector) \n",coneLateralOpening*2,coneVerticalOpening*2);
    //(int distance, int a, int b, int x, int y, int z)
    sis_ConeBeam* cone = createConeBeam(coneDistance,coneLateralOpening,coneVerticalOpening,0,0,-coneDistance);
    floats[V_CONEDISTANCE] = cone->distance;
    floats[V_CONEA] = cone->a;
    floats[V_CONEB] = cone->b;
    floats[V_CONEC] = cone->c;
    
    ints[V_RAYSAMPLING] = sampling;
    floats[V_RAYSAMPLINGDOUBLE] = ints[V_RAYSAMPLING];
    ints[V_POINTMODE] = pointMode;
    ints[V_MAXTHREADS] = maxThreads;

    floats[V_CONEPOSZ] = (-floats[V_CONEDISTANCE]);
    floats[V_CONEPOSZ_MINUS_VOLUMEZ] = floats[V_CONEPOSZ]-floats[V_VOLUMEZ];
    floats[V_VOLUMEY_MINUS_VOLUMEMIDDLE] =  floats[V_VOLUMEY] - (ints[V_VOLUMEMIDDLE]*floats[V_VOXEL_SIZE]);
    ints[V_IMAGE_ITERATIONS] = rotations;
    floats[V_FRESNEL_NEARCUT] = -0.1 -1.35465 - (0.04693*sin(energy) )+( 1.93177*log(energy) )+(0.02643*energy);

    if(loud)
    {
        printf("===============================================================\n");
        printf("Starting Simulation \n");
    }
    volumeProjection(ints,floats,maxThreads,rotations,initialObjectAngle,sampling,cone, detector, volume,pointMode,gpuArray,gpuArraySize,allowedMemoryConsumption);
    
    float totalExecTime = timeDiff(&totalExecution);

    printf("Total Execution Time: "); secondsPrettyPrint(totalExecTime); printf("\n");
    printf("Total Memory Time: "); secondsPrettyPrint(memoryCopyTime); printf("\n");
    printf("Total Ray Tracing Time: "); secondsPrettyPrint(rayCastingTime); printf("\n");
    printf("Total Propagation Time: "); secondsPrettyPrint(propagationTime); printf("\n");
    printf("Total Phantom Building Time: "); secondsPrettyPrint(phantomBuildingTime); printf("\n");

    cJSON_Delete(jsonin);
    deleteDetector(detector);
    
    free(cone);    
}
