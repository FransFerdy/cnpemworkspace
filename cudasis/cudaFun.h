
#ifndef CUDAFUN_HEADERGUARD
#define CUDAFUN_HEADERGUARD

//trilinear points constants (used in sis.c and cudaFun.cu)
#define CALC_POINTS_SIZE 15
#define AMOUNT_POINTS_INFO 28
#define POS_OFFSET 2
#define INTENSITY_OFFSET 8

//multiplier to zero pad image
#define FFT_PADDINGX 4.0
#define FFT_PADDINGY 4.0
#define FRESNEL_START_F10REGION 0.01

enum point_mode{
    PT_NEAREST_NEIGHBOR,
    PT_TRILINEAR
};

enum beam_type{
    B_CONE,
    B_PARALLEL
};

enum rollMode{
    ROLL_FIRST,
    ROLL_LATER
};

enum emission_types{
    E_POINTWISE,
    E_PYRAMIDAL,
    E_SPHERICAL,
    E_PYRAMIDAL_POINTWISE,
    EMISSION_VECTOR_SIZE
};

enum v_point_indexes{
    DETBASEX,
    DETBASEY,
    DETREALX,
    DETREALY,
    DETREALZ,
    POINTS_VARS_AMOUNT
};

enum detector_pixel_values{
    PV_PRESENCE,
    PV_X,
    PV_Y,
    PV_Z,
    PV_STEPSIZE,

    PV_VECTORSIZE
};

enum int_vars{
    V_VOLUMESIZE,
    V_VOLUMEMIDDLE,
    V_RAYSAMPLING,
    V_DETECTOR_ROLLMODE,
    V_DETECTORWIDTH,
    V_DETECTORHEIGHT,
    V_DETECTORDEPTH,
    V_DETECTOR_MIDDLEWIDTH,  
    V_DETECTOR_MIDDLEHEIGHT,
    V_DETECTORPLANESIZE,
    V_SHOW_GEOMETRY,
    V_VALIDDETECTORPOINTS, 
    V_POINTMODE,
    V_MAXTHREADS,
    V_TOPGPU,
    V_GRIDWIDTH,
    V_GRIDHEIGHT,
    V_BEAMTYPE,
    V_POINTS_PER_PIXEL,
    V_IMAGE_ITERATIONS,
    V_FRESNEL_START,
    V_FRESNEL_END,
    V_PROP_EMISSION_TYPE,
    
    
    V_INTVECTOR_SIZE
};
enum double_vars{
    V_VOLUMEX,
    V_VOLUMEY,
    V_VOLUMEZ,
    V_VOLUMEZFINAL,
    V_VOLUMEROTATIONFINAL,
    V_CONEA,
    V_CONEB,
    V_CONEC,
    V_CONEDISTANCE,
    V_DETECTORPITCH,
    V_DETECTORROLL,
    V_DETECTORYAW,
    V_RAYSAMPLINGDOUBLE,
    V_CONEPOSZ_MINUS_VOLUMEZ,
    V_VOLUMEY_MINUS_VOLUMEMIDDLE,
    V_CONEPOSZ,
    V_LAMBDA,
    V_GRIDPOINTS,
    V_WAVEK,
    V_PROPAGATION_DISTANCE,
    V_DETECTOR_PIXEL_WIDTH,
    V_DETECTOR_PIXEL_HEIGHT,
    V_VOXEL_SIZE,
    V_CONE_LATERAL_OPENING,
    V_F10,
    V_PIXEL_RESOLUTION,
    V_EFFECTIVE_PIXEL_SIZE,
    V_FRESNEL_NEARCUT,

    V_DOUBLEVECTOR_SIZE
};

enum vector_vars{
    V_VOLUMEDATA,
    V_DETECTORDATA,
    
    
    V_VECTORVECTOR_SIZE
};

enum param_ints
{
    VP_STARTY,
    VP_ENDY,
    VP_DELTAY,
    VP_SLICEHEIGHT,
    VP_REALTOPY,
    VP_REALBOTTOMY,

    VP_INTVECTOR_SIZE
};

enum param_floats
{
    VP_OBJECTTHETACOS,
    VP_OBJECTTHETASIN,
    VP_DOUBLEVECTOR_SIZE
};

enum param_floats_cone_project
{
    VCP_DETECTOR_ROLL_COS,
    VCP_DETECTOR_ROLL_SIN,
    VCP_DETECTOR_PITCH_COS,
    VCP_DETECTOR_PITCH_SIN,
    VCP_DETECTOR_YAW_COS,
    VCP_DETECTOR_YAW_SIN,
    VCP_DETECTOR_POWCONEA,
    VCP_DETECTOR_POWCONEB,

    VCP_DOUBLEVECTOR_SIZE
};

enum cpu_stages
{
    CPU_STAGE_MAKING_FRESNEL_KERNEL,
    CPU_STAGE_WAITING,
    CPU_STAGE_FREEING_VOLUME,
    CPU_STAGE_END,

    CPU_STAGES_SIZE
};



void freeArrayOnDevice(void *deviceMemoryPointer);
/*
Allocate the necessary memory and copy the contents of @source(from host) to @destination(on device).
*/
void copyToDeviceMemory(void *sourceElement,void **destinationPointer,size_t size);
void freeOnDevice(void *deviceMemoryPointer);
void mallocOnDevice(void ** devPtr,size_t size);
void memcpyToDevice(void * devPtr,void * srcPtr,size_t size);
void memsetInDevice(void *deviceMemoryPointer, int value, size_t count);

void memcpyToDeviceCufftComplexPos(void * devPtr,void * srcPtr,size_t size,size_t pos);


void cudaCalculateProjection(float conePosXarg, float conePosYarg, size_t pointsStart,size_t pointsAmount,int planeIndex,
float theta,float zNow, int startYa,int endYa,int sliceHeight,int * intsa,int * deviceIntsa,float *floatsa,
float *devicefloatsa,float * detector,float * detectorImaginary,float *deviceDetector,float *deviceDetectorImaginary,
void *d_volumeArrayar,void *d_volumeArrayarImaginary,float *deviceValidPoints, float *detectorPixelData);

void cudaComposeRadiation(int planeIndex,int * intsa,float *floatsa,
    float * detector,float * detectorImaginary,float *deviceDetector,float *deviceDetectorImaginary, void *deviceCufftDetectorarg,
    float *deviceValidPoints, void *deviceKernelMatrix, float *f10Vec, float *detectorPixelData, double hsumx,double hsumy);

void cpyVolumeSliceToDeviceTexture(void * cudaArrayarg,int startY,int endY,int sliceHeight,float *volume,int volumeSizearg);

void *malloc3DArrayOnDevice(size_t width,size_t height,size_t depth);

void cudaPrepareCone(int * intsa,float *floatsa,float **coneProjection,float **detectorPixelValues,float **validPoints);
int getCudaDevicesAndMemory(size_t **devicesMemory);
void cpyVolumeSliceToDevice(int startY,int endY,int sliceHeight,float *deviceVolume,float *volume,int volumeSize);
void mcudaSetDevice(int dev);

void *doFresnelImage(int * ints,float *floats,int amount, int startIt);
    void cudaCalculateFresnelKernel(void *deviceKernel, int * ints,float *floats, float zNow,double *hsumx,double *hsumy);


void deleteCufftComplex(void *cudaComplex);
size_t getSizeOfCufftComplex();
void *mallocCufftComplex(size_t elements);
void applyForwardFFTtoCufftComplex(void *complex,size_t width,size_t height);


void validateProjection(float conePosXarg, float conePosYarg, size_t pointsStart,size_t pointsAmount,int planeIndex,
float theta,float zNow, int startYa,int endYa,int sliceHeight,int * intsa,int * deviceIntsa,float *floatsa,
float *devicefloatsa,float * detector,float * detectorImaginary,float *deviceDetector,float *deviceDetectorImaginary,
void *d_volumeArrayar,void *d_volumeArrayarImaginary,float *deviceValidPoints, float *detectorPixelData);



#endif /* CUDAFUN_HEADERGUARD */