
#include <stdio.h>
#include "cufft.h"

extern "C"
{
#include "cudaFun.h"
#include "sis_util.h"
}
#include <pthread.h>
#include <unistd.h>
#include <math.h>


#define HANDLE_ERROR( call ){cudaError_t err = call;if ( cudaSuccess != err ){printf("CUDA2 error: %s\n", cudaGetErrorString(err)); exit(EXIT_FAILURE);}}



#define PT_VARIATION 0.0000000001   
#define Cvacuum 299792458
/*
Get 2D plane coordinate in 1D array
@x, x coordinate
@y, y coordinate
*/
__device__ __forceinline__
static size_t cup2d(size_t x,size_t y, size_t width)
{
    return (x+y*width);// ((x*d1)+y*d2)+z;
}
__forceinline__
static size_t p2d(size_t x,size_t y, size_t width)
{
    return (x+y*width);// ((x*d1)+y*d2)+z;
}

/*
Get 3D cube coordinate in 1D array
@x, x coordinate
@y, y coordinate
@z, z coordinate
*/
__forceinline__
static size_t p3dh(size_t x,size_t y, size_t z, size_t size)
{
    return (x+y*size+(z*size*size));// ((x*d1)+y*d2)+z;
}
//#define P3D(x,y,z,size) ((x)+(y)*(size)+((z)*(size)*(size)))

/*
Get 3D coordinate in 1D array
@x, x coordinate
@y, y coordinate
@z, z coordinate
@height, dimension 1
@depth dimension 2
*/
static size_t dp3d(size_t x,size_t y, size_t z, size_t width,size_t height)
{
    return (x+y*width+(z*width*height));// ((x*d1)+y*d2)+z;
}

__device__ __forceinline__
static size_t cudp3d(size_t x,size_t y, size_t z, size_t width,size_t height)
{
    return (x+y*width+(z*width*height));// ((x*d1)+y*d2)+z;
}

#define BLOCKXSIZE 8
#define BLOCKYSIZE 128

    typedef float VolumeType;
    texture<VolumeType, 3, cudaReadModeElementType> tex;
    texture<VolumeType, 3, cudaReadModeElementType> texImaginary;

    __constant__ int volumeSize;
    __constant__ int volumeMiddle;
    __constant__ int detectorWidth;
    __constant__ int detectorHeightC;

    __constant__ int startY;
    __constant__ int endY;
    
    __constant__ float raySamplingDouble;
    __constant__ int raySamplingInt;
    __constant__ float volumex;
    __constant__ float volumey;
    __constant__ float volumez;
    __constant__ int validPointAmount;
    __constant__ size_t detectorSize;

    __constant__ float objThetaSin;
    __constant__ float objThetaCos;
    __constant__ float conePosZMinusVolumeZ;
    __constant__ float volumeYMinusVolumeMiddle;
    __constant__ float conePosZ;
    __constant__ float one_Div_voxelSize;
    __constant__ float volumeStartZ;
    __constant__ float volumeEndZ;


    __constant__ float conePosX;
    __constant__ float conePosY;
    __constant__ float conePosYMinusvolumeY;
    __constant__ float conePosXMinusvolumeX;
    __constant__ float propagations2Pi_div_lambda;
    __constant__ float propagationE0;
    __constant__ float timesPixelSizeDivByGridPoints;
    __constant__ float lambda;

    void printCufftComplexFromDevice(cufftComplex *deviceData, size_t width, size_t height)
    {
        size_t imageSize = width*height;
        char widthStr[24];
        char heightStr[24];
        sprintf(widthStr,"%lu",width);
        sprintf(heightStr,"%lu",height);
        
        cufftComplex * hostData = (cufftComplex *)malloc(imageSize*sizeof(cufftComplex));
        if (hostData==NULL){printf("hostData Malloc Failed to Allocate Memory \n");exit(EXIT_FAILURE);}
        
        HANDLE_ERROR(  cudaMemcpy(hostData,deviceData,imageSize*sizeof(cufftComplex),cudaMemcpyDeviceToHost); )

        float * real = (float *)malloc(imageSize*sizeof(float));
        if (real==NULL){printf("real Malloc Failed to Allocate Memory \n");exit(EXIT_FAILURE);}

        float * imaginary = (float *)malloc(imageSize*sizeof(float));
        if (imaginary==NULL){printf("imaginary Malloc Failed to Allocate Memory \n");exit(EXIT_FAILURE);}

        for(int i=0;i<imageSize;i++)
        {
            real[i] = hostData[i].x;
            imaginary[i] = hostData[i].y;
        }
        free(hostData);
        FILE * rFile,*iFile;
        rFile = fopen ("realtemp1.data","wb");
        if (rFile!=NULL)
        {
            fwrite (real , sizeof(float), imageSize, rFile);
            fclose (rFile);
            printf("Finished Saving Real to file\n");
        }
        free(real);
        {
            char *pythonArgs[]={"python","python/plotimbin.py","realtemp1.data","","",NULL};
            pythonArgs[3] = widthStr;
            pythonArgs[4] = heightStr;
            callPython(pythonArgs);
        }
        iFile = fopen ("imaginarytemp2.data","wb");
        if (iFile!=NULL)
        {
            fwrite (imaginary , sizeof(float), imageSize, iFile);
            fclose (iFile);
            printf("Finished Saving Imaginary to file\n");
        }
        free(imaginary);
        {
            char *pythonArgs[]={"python","python/plotimbin.py","imaginarytemp2.data","","",NULL};
            pythonArgs[3] = widthStr;
            pythonArgs[4] = heightStr;
            callPython(pythonArgs);
        }
    }


    void printCufftComplexArctanFromDevice(cufftComplex *deviceData, size_t width, size_t height)
    {
        size_t imageSize = width*height;
        char widthStr[24];
        char heightStr[24];
        sprintf(widthStr,"%lu",width);
        sprintf(heightStr,"%lu",height);
        
        cufftComplex * hostData = (cufftComplex *)malloc(imageSize*sizeof(cufftComplex));
        if (hostData==NULL){printf("hostData Malloc Failed to Allocate Memory \n");exit(EXIT_FAILURE);}
        
        HANDLE_ERROR(  cudaMemcpy(hostData,deviceData,imageSize*sizeof(cufftComplex),cudaMemcpyDeviceToHost); )

        float * real = (float *)malloc(imageSize*sizeof(float));
        if (real==NULL){printf("real Malloc Failed to Allocate Memory \n");exit(EXIT_FAILURE);}

        float * imaginary = (float *)malloc(imageSize*sizeof(float));
        if (imaginary==NULL){printf("imaginary Malloc Failed to Allocate Memory \n");exit(EXIT_FAILURE);}

        for(int i=0;i<imageSize;i++)
        {
            real[i] = hostData[i].x;
            imaginary[i] = hostData[i].y;
        }
        free(hostData);
        FILE * rFile,*iFile;
        rFile = fopen ("realtemp1.data","wb");
        if (rFile!=NULL)
        {
            fwrite (real , sizeof(float), imageSize, rFile);
            fclose (rFile);
            printf("Finished Saving Real to file\n");
        }
        free(real);
       
        iFile = fopen ("imaginarytemp2.data","wb");
        if (iFile!=NULL)
        {
            fwrite (imaginary , sizeof(float), imageSize, iFile);
            fclose (iFile);
            printf("Finished Saving Imaginary to file\n");
        }
        free(imaginary);
        {
            char *pythonArgs[]={"python","python/plotimbinarctan.py","imaginarytemp2.data","realtemp1.data","","",NULL};
            pythonArgs[4] = widthStr;
            pythonArgs[5] = heightStr;
            callPython(pythonArgs);
        }
    }

    void printCufftComplexFromHost(cufftComplex *hostData, size_t width, size_t height)
    {
        size_t imageSize = width*height;
        char widthStr[24];
        char heightStr[24];
        sprintf(widthStr,"%lu",width);
        sprintf(heightStr,"%lu",height);
        
        float * real = (float *)malloc(imageSize*sizeof(float));
        if (real==NULL){printf("real Malloc Failed to Allocate Memory \n");exit(EXIT_FAILURE);}

        float * imaginary = (float *)malloc(imageSize*sizeof(float));
        if (imaginary==NULL){printf("imaginary Malloc Failed to Allocate Memory \n");exit(EXIT_FAILURE);}

        for(int i=0;i<imageSize;i++)
        {
            real[i] = hostData[i].x;
            imaginary[i] = hostData[i].y;
        }
        FILE * rFile,*iFile;
        rFile = fopen ("realtemp1.data","wb");
        if (rFile!=NULL)
        {
            fwrite (real , sizeof(float), imageSize, rFile);
            fclose (rFile);
            printf("Finished Saving Real to file\n");
        }
        free(real);
        {
            char *pythonArgs[]={"python","python/plotimbin.py","realtemp1.data","","",NULL};
            pythonArgs[3] = widthStr;
            pythonArgs[4] = heightStr;
            callPython(pythonArgs);
        }
        iFile = fopen ("imaginarytemp2.data","wb");
        if (iFile!=NULL)
        {
            fwrite (imaginary , sizeof(float), imageSize, iFile);
            fclose (iFile);
            printf("Finished Saving Imaginary to file\n");
        }
        free(imaginary);
        {
            char *pythonArgs[]={"python","python/plotimbin.py","imaginarytemp2.data","","",NULL};
            pythonArgs[3] = widthStr;
            pythonArgs[4] = heightStr;
            callPython(pythonArgs);
        }
    }

    void printFloatImageFromHost(float *real, size_t width, size_t height)
    {
        size_t imageSize = width*height;
        char widthStr[24];
        char heightStr[24];
        sprintf(widthStr,"%lu",width);
        sprintf(heightStr,"%lu",height);
        
        FILE * rFile;
        rFile = fopen ("realtemp1.data","wb");
        if (rFile!=NULL)
        {
            fwrite (real , sizeof(float), imageSize, rFile);
            fclose (rFile);
            printf("Finished Saving Real to file\n");
        }
        {
            char *pythonArgs[]={"python","python/plotimbin.py","realtemp1.data","","",NULL};
            pythonArgs[3] = widthStr;
            pythonArgs[4] = heightStr;
            callPython(pythonArgs);
        }
    }

    void printFloat2RadonFromHost(float *real,float *real2, size_t width, size_t height)
    {
        size_t imageSize = width*height;
        char widthStr[24];
        char heightStr[24];
        sprintf(widthStr,"%lu",width);
        sprintf(heightStr,"%lu",height);
        
        FILE * rFile;
        rFile = fopen ("realtemp1.data","wb");
        if (rFile!=NULL)
        {
            fwrite (real , sizeof(float), imageSize, rFile);
            fclose (rFile);
            printf("Finished Saving Real to file\n");
        }
        rFile = fopen ("realtemp2.data","wb");
        if (rFile!=NULL)
        {
            fwrite (real2 , sizeof(float), imageSize, rFile);
            fclose (rFile);
            printf("Finished Saving Real2 to file\n");
        }

        {
            char *pythonArgs[]={"python","python/plot2sinbin.py","realtemp1.data","realtemp2.data","","",NULL};
            pythonArgs[4] = widthStr;
            pythonArgs[5] = heightStr;
            callPython(pythonArgs);
        }
    }

    void printProblemGeometry(float angle, float distance, float dataCubeSize, float dataCubeZ, float detectorWidth, float detectorHeight)
    {
        char cangle[24];
        char cdistance[24];
        char cdataCubeSize[24];
        char cdataCubeZ[24];
        char cdetectorWidth[24];
        char cdetectorHeight[24];
        sprintf(cangle,"%f",angle);
        sprintf(cdistance,"%f",distance);
        sprintf(cdataCubeSize,"%f",dataCubeSize);
        sprintf(cdataCubeZ,"%f",dataCubeZ);
        sprintf(cdetectorWidth,"%f",detectorWidth);
        sprintf(cdetectorHeight,"%f",detectorHeight);
        {
            char *pythonArgs[]={"python","python/plotformat.py","","","","","","",NULL};
            pythonArgs[2] = cangle;
            pythonArgs[3] = cdistance;
            pythonArgs[4] = cdataCubeSize;
            pythonArgs[5] = cdataCubeZ;
            pythonArgs[6] = cdetectorWidth;
            pythonArgs[7] = cdetectorHeight;
            callPython(pythonArgs);
        }
    }

    float interpolate2(float myX,float xA,float xB,float yA,float yB)
    {
        return yA + ( yB - yA ) * ( (myX - xA) / ( xB - xA )  );
    }

    typedef struct FresnelJobConfig
    {
        pthread_mutex_t* resultMutex;
        int *threadCount;
        int threadIds;
        int imageIndex;
        float effectivePixelSizeW;
        float effectivePixelSizeH;
        float propDistance;

        float *floats;
        int *ints;
        cufftComplex *kernelMatrix;
        size_t linesPerThread;
    } FresnelJobConfig;

    void *calculateFresnelKernelMatrix(void *params)
    {
        FresnelJobConfig *config = (FresnelJobConfig*)params;

        int myId;
        float *floats=config->floats;
        int *ints=config->ints;
        int imageIndex = config->imageIndex;
        cufftComplex *kernelMatrix = config->kernelMatrix;
        pthread_mutex_t* resultMutex = config->resultMutex;
        size_t linesPerThread = config->linesPerThread;

        pthread_mutex_lock(resultMutex);
        myId =config->threadIds;
        config->threadIds++;
        pthread_mutex_unlock(resultMutex);
        
        
        double x,y,magX,exponent,propagationDistance, detectorPixelWidth,detectorPixelHeight,detectorPixelWidthCenter,detectorPixelHeightCenter;
        double cosExp,sinExp;
        int width = ints[V_DETECTORWIDTH];
        int halfwidth = ints[V_DETECTOR_MIDDLEWIDTH];
        int halfheight = ints[V_DETECTOR_MIDDLEHEIGHT];
        propagationDistance = config->propDistance;
        detectorPixelWidth = config->effectivePixelSizeW;
        detectorPixelHeight = config->effectivePixelSizeH;
        detectorPixelWidthCenter = detectorPixelWidth/2.0;
        detectorPixelHeightCenter = detectorPixelHeight/2.0;

        long int start, end,outStartX,outStartY,pointPosition;

        start = myId*linesPerThread;
        end = (myId+1)*linesPerThread;
        if (end>ints[V_DETECTORHEIGHT])
            end = ints[V_DETECTORHEIGHT];
        
        outStartX = (FFT_PADDINGX-1)*ints[V_DETECTORWIDTH]/2.0;
        outStartY = (FFT_PADDINGY-1)*ints[V_DETECTORHEIGHT]/2.0;

        for(int k=start;k<end;k++)
        {
            y = detectorPixelHeightCenter+ (((k - halfheight)*(-1))*detectorPixelHeight);
            for(int i=0;i<width;i++)
            {
                x = detectorPixelWidthCenter + ((i-halfwidth)*detectorPixelWidth);
                magX = sqrt((x*x) + (y*y) + (propagationDistance*propagationDistance)); //d*d
                    
                exponent = magX*floats[V_WAVEK];// -i*K*||X||
                pointPosition = dp3d(i+outStartX,k+outStartY,imageIndex,ints[V_DETECTORWIDTH]*FFT_PADDINGX,ints[V_DETECTORHEIGHT]*FFT_PADDINGY);
                sincos(exponent,&sinExp,&cosExp);
                kernelMatrix[pointPosition].x = cosExp/magX;
                kernelMatrix[pointPosition].y = -sinExp/magX;

                //printf("pointPosition %lu magX %f x %f y %f exponent %f cos %f sin %f  real %f imaginary %f\n",
                //pointPosition, magX,x,y,exponent,cosExp,sinExp,kernelMatrix[pointPosition].x,kernelMatrix[pointPosition].y);

            }
        }

        pthread_mutex_lock(resultMutex);
        (*(config->threadCount))--;
        pthread_mutex_unlock(resultMutex);
        return NULL;
    }

    extern "C"
    void * doFresnelImage(int * ints,float *floats,int amount, int startIt)
    {
        pthread_mutex_t resultMutex;
        pthread_t threadReference;
        pthread_mutex_init(&resultMutex, NULL);
        int threadCount=0;
        int localThreadCount = 0;
        int maxThreads = ints[V_MAXTHREADS];

        FresnelJobConfig config;
        cufftComplex *kernelMatrix = (cufftComplex*)calloc((size_t)((size_t)ints[V_DETECTORWIDTH]*(size_t)ints[V_DETECTORHEIGHT])*FFT_PADDINGX*FFT_PADDINGY*(size_t)amount,sizeof(cufftComplex));
        if (kernelMatrix==NULL){printf("KernelMatrix Malloc Failed to Allocate Memory \n");exit(EXIT_FAILURE);}
        
        float zNow,zStep,zDelta,propDistance,z1,z1_plus_z2,magnification,effectivePixelSizeW,effectivePixelSizeH;
        zDelta = floats[V_VOLUMEZFINAL] - floats[V_VOLUMEZ];
        zStep = zDelta/(float)(ints[V_IMAGE_ITERATIONS]);
        z1_plus_z2 = floats[V_CONEDISTANCE];
        
        for(int k=0;k<amount;k++)
        {
            zNow = floats[V_VOLUMEZ] + ( zStep*(k+startIt) );
            propDistance = (zNow*(-1))-((float)ints[V_VOLUMEMIDDLE]*floats[V_VOXEL_SIZE]);
            if (propDistance<0) 
                propDistance=0;
            z1 =  -(floats[V_CONEPOSZ]-zNow);
            magnification = z1_plus_z2/z1;
            effectivePixelSizeW = floats[V_DETECTOR_PIXEL_WIDTH]/magnification;
            effectivePixelSizeH = floats[V_DETECTOR_PIXEL_HEIGHT]/magnification;

            config.floats=floats;
            config.ints=ints;
            config.kernelMatrix=kernelMatrix;
            config.linesPerThread=ceil((float)ints[V_DETECTORHEIGHT]/(float)maxThreads);

            config.resultMutex = &resultMutex;
            config.threadCount = &threadCount;
            config.threadIds=0;
            config.imageIndex=k;
            config.effectivePixelSizeW = effectivePixelSizeW;
            config.effectivePixelSizeH = effectivePixelSizeH;
            config.propDistance = propDistance;

            for(int i=0;i<maxThreads;i++)
            {
                if(pthread_create(&threadReference, NULL, calculateFresnelKernelMatrix, &config)) 
                {
                    fprintf(stderr, "Error creating thread\n");
                }else
                {
                    pthread_mutex_lock(&resultMutex);
                    threadCount++;
                    pthread_mutex_unlock(&resultMutex); 
                    //printf("thread # %d started\n",threadCount);
                }
            }
            localThreadCount=1;
            while(localThreadCount>0)
            {
                pthread_mutex_lock(&resultMutex);
                localThreadCount = threadCount;
                pthread_mutex_unlock(&resultMutex);
                usleep(500);
            }
            sleep(2);
            //printCufftComplexFromHost(&kernelMatrix[dp3d(0,0,k,ints[V_DETECTORWIDTH]*FFT_PADDINGX, ints[V_DETECTORHEIGHT]*FFT_PADDINGY)], ints[V_DETECTORWIDTH]*FFT_PADDINGX, ints[V_DETECTORHEIGHT]*FFT_PADDINGY);
        }

        
        printf("Kernel Matrix is Ready\n");


        return (void*)kernelMatrix;
    }


    __global__ void fft_shift_x(cufftComplex *data,
        int Nrays,
        int Nslices,
        int blockSize)
    {
    int tx = threadIdx.x + blockIdx.x*blockDim.x;
    int ty = threadIdx.y + blockIdx.y*blockDim.y;
    int tz = threadIdx.z + blockIdx.z*blockDim.z;
    float tmpx, tmpy;
        if( (tx < Nrays/2) && (ty<Nslices) && (tz < blockSize) )
        {
        tmpx = data[ tz*Nrays*Nslices + ty*Nrays + tx].x;
        tmpy = data[ tz*Nrays*Nslices + ty*Nrays + tx].y;

        data[ tz*Nrays*Nslices + ty*Nrays + tx].x =  data[ tz*Nrays*Nslices + ty*Nrays + (tx + Nrays/2)].x;
        data[ tz*Nrays*Nslices + ty*Nrays + tx].y =  data[ tz*Nrays*Nslices + ty*Nrays + (tx + Nrays/2)].y;
        
        data[ tz*Nrays*Nslices + ty*Nrays + (tx + Nrays/2)].x =  tmpx;
        data[ tz*Nrays*Nslices + ty*Nrays + (tx + Nrays/2)].y =  tmpy;
        }
    }

    __global__ void fft_shift_y(cufftComplex *data,
        int Nrays,
        int Nslices,
        int blockSize)
    {
    int tx = threadIdx.x + blockIdx.x*blockDim.x;
    int ty = threadIdx.y + blockIdx.y*blockDim.y;
    int tz = threadIdx.z + blockIdx.z*blockDim.z;
    float tmpx, tmpy;

        if( (tx < Nrays) && (ty<Nslices/2) && (tz < blockSize) )
        {
        tmpx = data[ tz*Nrays*Nslices + ty*Nrays + tx].x;
        tmpy = data[ tz*Nrays*Nslices + ty*Nrays + tx].y;

        data[ tz*Nrays*Nslices + ty*Nrays + tx].x =  data[ tz*Nrays*Nslices + (ty + Nslices/2)*Nrays + tx].x;
        data[ tz*Nrays*Nslices + ty*Nrays + tx].y =  data[ tz*Nrays*Nslices + (ty + Nslices/2)*Nrays + tx].y;

        data[ tz*Nrays*Nslices + (ty + Nslices/2)*Nrays + tx].x =  tmpx;
        data[ tz*Nrays*Nslices + (ty + Nslices/2)*Nrays + tx].y =  tmpy;
        }

    }

    __constant__ double fk_halfWidth;
    __constant__ double fk_halfHeight;
    __constant__ double fk_propagationDistanceSquare;
    __constant__ double fk_propagationDistance;
    __constant__ double fk_detectorPixelHeightCenter;
    __constant__ double fk_detectorPixelWidthCenter;
    __constant__ double fk_detectorPixelHeight;
    __constant__ double fk_detectorPixelWidth;
    __constant__ int fk_outStartX;
    __constant__ int fk_outStartY;
    __constant__ int fk_kernelWidth;
    __constant__ double fk_lambda;
    __constant__ double fk_waveK;
    __constant__ double fk_magnification;

    template<bool PYRAMID, bool POINTWISE>
    __global__ void fresnelKernel(cufftComplex *deviceKernelMatrix, cufftComplex *hsum)
    {
        int tx = threadIdx.x + blockIdx.x*blockDim.x;
        int ty = threadIdx.y + blockIdx.y*blockDim.y;
        if( (tx < detectorWidth) && (ty < detectorHeightC) )
        {
            double x,y,magX,exponent,alpha;
            size_t pointPosition;
            
            y = fk_detectorPixelHeightCenter+ ( (-(ty - fk_halfHeight) )*fk_detectorPixelHeight);
            x = fk_detectorPixelWidthCenter + ((tx-fk_halfWidth)*fk_detectorPixelWidth);
            magX = sqrt((x*x) + (y*y) + (fk_propagationDistanceSquare) ); //d*d
            pointPosition = cup2d(tx+fk_outStartX,ty+fk_outStartY,fk_kernelWidth);

            if (POINTWISE && !PYRAMID)
                alpha=1;
            else
                alpha=1;
            
            if( POINTWISE && sqrt (x*x + y*y) > alpha*( (fk_propagationDistance * fk_lambda) / ( fk_detectorPixelHeight ) ) )
            {
                if (pointPosition == cup2d( (detectorWidth/2)+fk_outStartX,(detectorHeightC/2)+fk_outStartY,fk_kernelWidth))
                {
                    deviceKernelMatrix[pointPosition].x = 1/fk_propagationDistance;
                    atomicAdd ( &(hsum->x), deviceKernelMatrix[pointPosition].x );
                }
                else
                    deviceKernelMatrix[pointPosition].x = 0; 

                deviceKernelMatrix[pointPosition].y = 0;
            }
            else
            {
                double rectSincX,rectSincY;
                if (PYRAMID)
                {
                    rectSincX = (x*fk_detectorPixelWidth*M_PI)/(magX*fk_lambda);
                    rectSincX = sin(rectSincX)/rectSincX;
                    rectSincX = rectSincX*rectSincX;

                    rectSincY = (y*fk_detectorPixelHeight*M_PI)/ (magX*fk_lambda);
                    rectSincY = sin(rectSincY)/rectSincY;
                    rectSincY = rectSincY*rectSincY;
                }else
                {
                    rectSincY = 1;
                    rectSincX = 1;
                }
                

                exponent = magX*fk_waveK;// -i*K*||X||
                deviceKernelMatrix[pointPosition].x  = (rectSincX*rectSincY*cos(exponent) )/magX;
                deviceKernelMatrix[pointPosition].y  = (rectSincX*rectSincY*(-sin(exponent)) )/magX;
                atomicAdd ( &(hsum->x), deviceKernelMatrix[pointPosition].x );
                atomicAdd ( &(hsum->y), deviceKernelMatrix[pointPosition].y );
            }
        }
        
    }


    extern "C"
    void cudaCalculateFresnelKernel(void *deviceKernel, int * ints,float *floats, float zNow,double *hsumx,double *hsumy)
    {
        cufftComplex *deviceKernelMatrix = (cufftComplex*)deviceKernel;

        double propDistance,z1,z1_plus_z2,magnification,effectivePixelSizeW,effectivePixelSizeH;

        dim3 blocksPerGridFresnelKernel(1,1,1);
        dim3 threadsPerBlockFresnelKernel(1,1,1);
        cudaError_t err;
        
        dim3 blocksPerGridShiftKernel(1,1,1);
        dim3 threadsPerBlockShiftKernel(1,1,1);
        
        threadsPerBlockShiftKernel.x = 32;
        threadsPerBlockShiftKernel.y = 32;
        blocksPerGridShiftKernel.x = ceil((double)ints[V_DETECTORWIDTH]*FFT_PADDINGX/(double)threadsPerBlockShiftKernel.x);
        blocksPerGridShiftKernel.y = ceil((double)ints[V_DETECTORHEIGHT]*FFT_PADDINGY/(double)threadsPerBlockShiftKernel.y);

        cufftComplex *device_hsum;
        cudaMalloc((void **)&device_hsum,sizeof(cufftComplex));
        cufftComplex host_hsum;
        host_hsum.x = 0;
        host_hsum.y = 0;
        HANDLE_ERROR(cudaMalloc((void **)&device_hsum,sizeof(cufftComplex));)
        HANDLE_ERROR(cudaMemcpy((void *)device_hsum,(void *)&host_hsum,sizeof(cufftComplex),cudaMemcpyHostToDevice);)

        threadsPerBlockFresnelKernel.x = 32;
        threadsPerBlockFresnelKernel.y = 32;
        blocksPerGridFresnelKernel.x = ceil((double)ints[V_DETECTORWIDTH]/(double)threadsPerBlockFresnelKernel.x);
        blocksPerGridFresnelKernel.y = ceil((double)ints[V_DETECTORHEIGHT]/(double)threadsPerBlockFresnelKernel.y);

        z1_plus_z2 = floats[V_CONEDISTANCE]; 
        z1 =  -(floats[V_CONEPOSZ]-zNow);
        magnification = z1_plus_z2/z1;

        propDistance = (zNow*(-1))-((float)ints[V_VOLUMEMIDDLE]*floats[V_VOXEL_SIZE]);
        
        if (ints[V_BEAMTYPE]==B_CONE)
        {
            propDistance = propDistance/magnification;
            effectivePixelSizeW = floats[V_DETECTOR_PIXEL_WIDTH] /magnification;
            effectivePixelSizeH = floats[V_DETECTOR_PIXEL_HEIGHT] /magnification;
        }
        if (ints[V_BEAMTYPE]==B_PARALLEL)
        {
            propDistance = propDistance;
            effectivePixelSizeW = floats[V_DETECTOR_PIXEL_WIDTH];
            effectivePixelSizeH = floats[V_DETECTOR_PIXEL_HEIGHT];
        }
        if (propDistance<0)
            propDistance=0;

        //printf("BeamD %g PhantomD %g mangnfication %g width %f height %f \n",z1_plus_z2,z1,magnification, effectivePixelSizeW, effectivePixelSizeH);

        double tempdetectorPixelWidthCenter = effectivePixelSizeW/2.0;
        double tempdetectorPixelHeightCenter = effectivePixelSizeH/2.0;
        double tempfk_propagationDistanceSquare = (double)propDistance*(double)propDistance;
        double tempfk_propagationDistance = propDistance;
        long int tempoutStartX = (FFT_PADDINGX-1)*ints[V_DETECTORWIDTH]/2.0; //beggining of kernel frame, centralized in 0 padding
        long int tempoutStartY = (FFT_PADDINGY-1)*ints[V_DETECTORHEIGHT]/2.0;
        int tempKernelWidth = ints[V_DETECTORWIDTH]*FFT_PADDINGX;
        int tempDeviceKernelHeight = ints[V_DETECTORHEIGHT];
        int tempDeviceKernelWidth = ints[V_DETECTORWIDTH];
        double halfwidth = ints[V_DETECTOR_MIDDLEWIDTH];
        double halfheight = ints[V_DETECTOR_MIDDLEHEIGHT];
        double templambda = floats[V_LAMBDA];
        double tempwaveK = floats[V_WAVEK];

        //printf("magnification %g lambda %g wc %f wh %f pSquared %f startX %li startY %li kernelWidth %i halfwidth %f halfHeight %f zNow %f blocks x %u blocks y %u \n",
        //magnification,templambda,tempdetectorPixelWidthCenter,tempdetectorPixelHeightCenter,tempfk_propagationDistanceSquare,
        //tempoutStartX,tempoutStartY,tempKernelWidth,halfwidth,halfheight,zNow,blocksPerGridFresnelKernel.x,blocksPerGridFresnelKernel.y);

        
        HANDLE_ERROR(cudaMemcpyToSymbol(fk_outStartX,&tempoutStartX,sizeof(int)); )   
        HANDLE_ERROR(cudaMemcpyToSymbol(fk_outStartY,&tempoutStartY,sizeof(int)); ) 
        HANDLE_ERROR(cudaMemcpyToSymbol(fk_kernelWidth,&tempKernelWidth,sizeof(int)); ) 

        HANDLE_ERROR(cudaMemcpyToSymbol(fk_lambda,&templambda,sizeof(double)); ) 
        HANDLE_ERROR(cudaMemcpyToSymbol(fk_waveK,&tempwaveK,sizeof(double)); ) 
        HANDLE_ERROR(cudaMemcpyToSymbol(fk_magnification,&magnification,sizeof(double)); ) 

        HANDLE_ERROR(cudaMemcpyToSymbol(fk_propagationDistanceSquare,&tempfk_propagationDistanceSquare,sizeof(double)); )
        HANDLE_ERROR(cudaMemcpyToSymbol(fk_propagationDistance,&tempfk_propagationDistance,sizeof(double)); )

        HANDLE_ERROR(cudaMemcpyToSymbol(fk_detectorPixelWidthCenter,&tempdetectorPixelWidthCenter,sizeof(double)); ) 
        HANDLE_ERROR(cudaMemcpyToSymbol(fk_detectorPixelHeightCenter,&tempdetectorPixelHeightCenter,sizeof(double)); ) 

        HANDLE_ERROR(cudaMemcpyToSymbol(fk_detectorPixelWidth,&effectivePixelSizeW,sizeof(double)); ) 
        HANDLE_ERROR(cudaMemcpyToSymbol(fk_detectorPixelHeight,&effectivePixelSizeH,sizeof(double)); ) 

        HANDLE_ERROR(cudaMemcpyToSymbol(fk_halfWidth,&halfwidth,sizeof(double)); ) 
        HANDLE_ERROR(cudaMemcpyToSymbol(fk_halfHeight,&halfheight,sizeof(double)); ) 

        HANDLE_ERROR(cudaMemcpyToSymbol(detectorWidth,&tempDeviceKernelWidth,sizeof(int)); ) 
        HANDLE_ERROR(cudaMemcpyToSymbol(detectorHeightC,&tempDeviceKernelHeight,sizeof(int)); ) 

        if (ints[V_PROP_EMISSION_TYPE]==E_POINTWISE)
            fresnelKernel<false,true><<<blocksPerGridFresnelKernel,threadsPerBlockFresnelKernel>>>(deviceKernelMatrix,device_hsum);
        if (ints[V_PROP_EMISSION_TYPE]==E_PYRAMIDAL)
            fresnelKernel<true,false><<<blocksPerGridFresnelKernel,threadsPerBlockFresnelKernel>>>(deviceKernelMatrix,device_hsum);
        if (ints[V_PROP_EMISSION_TYPE]==E_PYRAMIDAL_POINTWISE)
            fresnelKernel<true,true><<<blocksPerGridFresnelKernel,threadsPerBlockFresnelKernel>>>(deviceKernelMatrix,device_hsum);
        if (ints[V_PROP_EMISSION_TYPE]==E_SPHERICAL)
            fresnelKernel<false,false><<<blocksPerGridFresnelKernel,threadsPerBlockFresnelKernel>>>(deviceKernelMatrix,device_hsum);

        cudaDeviceSynchronize();
        
        err = cudaPeekAtLastError();
        if( err != cudaSuccess)
        {
           printf("Kernel2 CUDA error: %s\n", cudaGetErrorString(err));
        }

        fft_shift_x<<<blocksPerGridShiftKernel,threadsPerBlockShiftKernel>>>(deviceKernelMatrix,ints[V_DETECTORWIDTH]*FFT_PADDINGX,ints[V_DETECTORHEIGHT]*FFT_PADDINGY,1);
        cudaDeviceSynchronize();
        err = cudaPeekAtLastError();
        if( err != cudaSuccess)
        {
            printf("Compose Kernel CUDA error: %s\n", cudaGetErrorString(err));
        }  
        fft_shift_y<<<blocksPerGridShiftKernel,threadsPerBlockShiftKernel>>>(deviceKernelMatrix,ints[V_DETECTORWIDTH]*FFT_PADDINGX,ints[V_DETECTORHEIGHT]*FFT_PADDINGY,1);
        cudaDeviceSynchronize();
        err = cudaPeekAtLastError();
        if( err != cudaSuccess)
        {
            printf("Compose Kernel CUDA error: %s\n", cudaGetErrorString(err));
        }
        
        HANDLE_ERROR(cudaMemcpy(&host_hsum,device_hsum,sizeof(cufftComplex),cudaMemcpyDeviceToHost);)
        cudaFree(device_hsum);
        //printf("hsum x %f hsum y %f \n",host_hsum.x,host_hsum.y);
        (*hsumx) = host_hsum.x;
        (*hsumy) = host_hsum.y;

        //printCufftComplexArctanFromDevice(deviceKernelMatrix,ints[V_DETECTORWIDTH]*FFT_PADDINGX,ints[V_DETECTORHEIGHT]*FFT_PADDINGY);
        //printCufftComplexFromDevice(deviceKernelMatrix,ints[V_DETECTORWIDTH]*FFT_PADDINGX,ints[V_DETECTORHEIGHT]*FFT_PADDINGY);
    }


    

    //for testing purposes only
    __global__
    void convolveSameSize(cufftComplex *deviceDetectorCufftOut,cufftComplex *deviceDetector,cufftComplex *cufftKernel,size_t startopX)
    {
        size_t opX = (blockIdx.x*blockDim.x)+threadIdx.x;
        size_t tY = (blockIdx.y*blockDim.y)+threadIdx.y;
        size_t tX = (blockIdx.z*blockDim.z)+threadIdx.z;
        opX+=startopX;

        long int kX, kY, pX,pY;

        pY = opX/detectorWidth;
        pX = opX%detectorWidth;
        
        kX = pX - (detectorWidth/2) + tX;
        kY = pY - (detectorHeightC/2) + tY;
        if (kX>=0 && kY>=0 && kX < detectorWidth && kY<detectorHeightC && opX<(detectorWidth*detectorHeightC))
        {
            float R1,I1,R2,I2,outR,outI;
            
            size_t detectorPoint = cup2d(kX,kY,detectorWidth);                      
            size_t kernelPoint = cup2d(tX,tY,detectorWidth);
            size_t outDetectorPoint = cup2d(pX,pY,detectorWidth);

            R1 = deviceDetector[detectorPoint].x;
            I1 = deviceDetector[detectorPoint].y;

            R2 = cufftKernel[kernelPoint].x;
            I2 = cufftKernel[kernelPoint].y;

            outR = R1*R2 - I1*I2;
            outI = R1*I2 + R2*I1;

            atomicAdd ( &deviceDetectorCufftOut[outDetectorPoint].x, outR );
            atomicAdd ( &deviceDetectorCufftOut[outDetectorPoint].y, outI );        
        }
    }


    __global__
    void pointToPointComplexMatrixMultiplication(cufftComplex *deviceDetector,cufftComplex *cufftKernel)
    {
        size_t myPosX = (blockIdx.x*blockDim.x)+threadIdx.x;
        if (myPosX<detectorSize)
        {
            float R1,I1,R2,I2;

            R1 = deviceDetector[myPosX].x;
            I1 = deviceDetector[myPosX].y;

            R2 = cufftKernel[myPosX].x;
            I2 = cufftKernel[myPosX].y;

            deviceDetector[myPosX].x = R1*R2 - I1*I2;
            deviceDetector[myPosX].y = R1*I2 + R2*I1;
        }
    }

    /*
    __global__
    void dividePointsByN(cufftComplex *deviceDetector)
    {
        size_t myPosX = (blockIdx.x*blockDim.x)+threadIdx.x;
        if (myPosX<detectorSize)
        {
            deviceDetector[myPosX].x = deviceDetector[myPosX].x/(float)detectorSize;
            deviceDetector[myPosX].y = deviceDetector[myPosX].y/(float)detectorSize;
        }
    }
    */

    __global__
    void composeRadiation(float *deviceDetector, float *deviceDetectorImaginary, cufftComplex *deviceDetectorOut,float *validPoints,float *detectorPixelData)
    {
        size_t myPosX = (blockIdx.x*blockDim.x)+threadIdx.x;
        size_t myPosY = (blockIdx.y*blockDim.y)+threadIdx.y;
        if (myPosY<detectorHeightC && myPosX<detectorWidth)
        {
            size_t pointPosition,cufftPointPosition;
            double real, imaginary,absorptionMultiE0,deviceDetectorRealPart;
            float stepSize = detectorPixelData[ cudp3d(myPosX,myPosY,PV_STEPSIZE,detectorWidth,detectorHeightC) ];
            pointPosition = cup2d(myPosX,myPosY,detectorWidth);  
            cufftPointPosition = cup2d(myPosX,myPosY,detectorWidth*FFT_PADDINGX);

            absorptionMultiE0 = propagationE0*exp(-( ( ( propagations2Pi_div_lambda ) * deviceDetectorImaginary[pointPosition])*stepSize*timesPixelSizeDivByGridPoints) );
            deviceDetectorRealPart = ( (propagations2Pi_div_lambda)*deviceDetector[pointPosition]*stepSize*timesPixelSizeDivByGridPoints );

            real = absorptionMultiE0 * cos(deviceDetectorRealPart);
            imaginary = (absorptionMultiE0 * ( (-1)*sin(deviceDetectorRealPart)));
            
            deviceDetectorOut[cufftPointPosition].x = real;
            deviceDetectorOut[cufftPointPosition].y = imaginary;
        
            #pragma unroll
            for(int i=0; i<FFT_PADDINGX;i++)
            {
                #pragma unroll
                for(int k=0; k<FFT_PADDINGY;k++)
                {
                    if (!(i==0 && k ==0) )
                    {
                        cufftPointPosition = cup2d(myPosX+(detectorWidth*i),myPosY+(detectorHeightC*k),detectorWidth*FFT_PADDINGX);
                        deviceDetectorOut[cufftPointPosition].x = 1;
                    }
                }
            }  
        }
    }

    __constant__ double fp_kSumReal;
    __constant__ double fp_minusKSumImag;
    __constant__ double fp_kSumRealImagSquared;
    
    __global__
    void finishPropagation(float *deviceDetector, cufftComplex *deviceDetectorCufft,float *validPoints)
    {
        size_t myPosX = (blockIdx.x*blockDim.x)+threadIdx.x;
        size_t myPosY = (blockIdx.y*blockDim.y)+threadIdx.y;
        if (myPosY<detectorHeightC && myPosX<detectorWidth)
        {
            size_t pointPosition,cufftPointPosition;
            float real, imaginary, R1,I1;
            pointPosition = cup2d(myPosX,myPosY,detectorWidth);  
            cufftPointPosition = cup2d(myPosX,myPosY,detectorWidth*FFT_PADDINGX);

            //real = (-deviceDetectorCufft[cufftPointPosition].y/ (detectorSize*lambda));
            //imaginary = ((deviceDetectorCufft[cufftPointPosition].x/ (detectorSize*lambda) ));
         

            real = deviceDetectorCufft[cufftPointPosition].x / (detectorWidth*detectorWidth*FFT_PADDINGX*FFT_PADDINGY);
            imaginary= deviceDetectorCufft[cufftPointPosition].y/ (detectorWidth*detectorWidth*FFT_PADDINGX*FFT_PADDINGY);

            R1 = real;
            I1 = imaginary;

            real = R1*fp_kSumReal - I1*fp_minusKSumImag;
            imaginary = R1*fp_minusKSumImag + fp_kSumReal*I1;

            real = real/fp_kSumRealImagSquared;
            imaginary = imaginary/fp_kSumRealImagSquared;
   
            deviceDetector[pointPosition] = ( (real*real) + (imaginary*imaginary) );
        }
    }

    __global__
    void composeRadiationNearImage(float *deviceDetector, float *deviceDetectorImaginary, cufftComplex *deviceDetectorOut,float *validPoints, float *detectorPixelData)
    {
        size_t myPosX = (blockIdx.x*blockDim.x)+threadIdx.x;
        size_t myPosY = (blockIdx.y*blockDim.y)+threadIdx.y;
        if (myPosY<detectorHeightC && myPosX<detectorWidth)
        {
            size_t pointPosition;
            float real, imaginary,absorptionMultiE0,deviceDetectorRealPart;
            float stepSize = detectorPixelData[ cudp3d(myPosX,myPosY,PV_STEPSIZE,detectorWidth,detectorHeightC) ];
            pointPosition = cup2d(myPosX,myPosY,detectorWidth);  
            absorptionMultiE0 = propagationE0*exp(-( ( ( propagations2Pi_div_lambda ) * deviceDetectorImaginary[pointPosition])*stepSize*timesPixelSizeDivByGridPoints) );
            deviceDetectorRealPart = ( (propagations2Pi_div_lambda)*deviceDetector[pointPosition]*stepSize*timesPixelSizeDivByGridPoints );

            real = absorptionMultiE0 * cos(deviceDetectorRealPart);
            imaginary = (absorptionMultiE0 * ( (-1)*sin(deviceDetectorRealPart)));
            deviceDetector[pointPosition] = pow ( (real*real) + (imaginary*imaginary), 2);
        }
    }

__global__
void calculateLineSumCuComplex(long startPos, float *deviceDetector,float * deviceDetectorImaginary,float *deviceValidPoints)
{
    int myPosX;
    int myPosY;
    //int tid = threadIdx.y;
    int tidx = threadIdx.x;
    myPosX = (blockIdx.x*blockDim.x)+tidx+startPos;
    myPosY = (blockIdx.y*blockDim.y) + threadIdx.y;
    if (myPosY<raySamplingInt && myPosX<validPointAmount)
    {
        float t=0, tempPoint[3],tempPoint2[3];
        float tVec[3];
        float rayStartX,rayStartY,rayEndX,rayEndY;
        
        tVec[0] = deviceValidPoints[cup2d(DETREALX,myPosX,POINTS_VARS_AMOUNT)] - conePosX;
        tVec[1] = deviceValidPoints[cup2d(DETREALY,myPosX,POINTS_VARS_AMOUNT)] - conePosY;
        tVec[2] = deviceValidPoints[cup2d(DETREALZ,myPosX,POINTS_VARS_AMOUNT)] - conePosZ;

        t = (volumeStartZ-conePosZ) / tVec[2];
        rayStartY = (t*tVec[1]) + conePosY;
        rayStartX = (t*tVec[0]) + conePosX;
        t = (volumeEndZ-conePosZ) / tVec[2];
        rayEndY = (t*tVec[1]) + conePosY;
        rayEndX = (t*tVec[0]) + conePosX;

        tVec[0] = rayStartX - rayEndX;
        tVec[1] = rayStartY - rayEndY;
        tVec[2] = volumeStartZ - volumeEndZ;

        t = myPosY / raySamplingDouble;
        tempPoint2[1] = (((t*tVec[1]) + rayEndY - volumey)*one_Div_voxelSize)+ volumeMiddle;//y
        tempPoint[0] = (t*tVec[0]) + rayEndX - volumex;//x
        tempPoint[2] = t*tVec[2]+ volumeEndZ - volumez;//z
        
        tempPoint2[0] = ( ( ( tempPoint[0] * objThetaCos ) - ( tempPoint[2] * objThetaSin ) )*one_Div_voxelSize) + volumeMiddle;
        tempPoint2[2] = ( ( ( tempPoint[0] * objThetaSin ) + ( tempPoint[2] * objThetaCos ) )*one_Div_voxelSize) + volumeMiddle;
        
        if (tempPoint2[1]>=startY && tempPoint2[1]<endY)
        {
            float retVal[2];
            size_t pointPosition;
            tempPoint2[1] = 1+tempPoint2[1]-startY;
            
            retVal[0] = tex3D(tex,tempPoint2[0],tempPoint2[1],tempPoint2[2]);
            retVal[1] = tex3D(texImaginary,tempPoint2[0],tempPoint2[1],tempPoint2[2]);

            pointPosition = cup2d(deviceValidPoints[cup2d(DETBASEX,myPosX,POINTS_VARS_AMOUNT)],deviceValidPoints[cup2d(DETBASEY,myPosX,POINTS_VARS_AMOUNT)],detectorWidth );

            atomicAdd ( &deviceDetector[pointPosition], retVal[0] );
            atomicAdd ( &deviceDetectorImaginary[pointPosition], retVal[1] ); 
        }
    }
}

__global__
void calculateLineSumCuParallelComplex(long startPos, float *deviceDetector, float * deviceDetectorImaginary,float *deviceValidPoints)
{
    int myPosX;
    int myPosY;
    //int tid = threadIdx.y;
    int tidx = threadIdx.x;
    myPosX = (blockIdx.x*blockDim.x)+tidx+startPos;
    myPosY = (blockIdx.y*blockDim.y) + threadIdx.y;
    if (myPosY<raySamplingInt && myPosX<validPointAmount)
    {
        float t=0, tempPoint[3],tempPoint2[3];
        float tVec;
        

        tVec = volumeStartZ - volumeEndZ;

        t = myPosY / raySamplingDouble;

        tempPoint2[1] = ((deviceValidPoints[cup2d(DETREALY,myPosX,POINTS_VARS_AMOUNT)] - volumey)*one_Div_voxelSize) + volumeMiddle;//y
        tempPoint[0] = deviceValidPoints[cup2d(DETREALX,myPosX,POINTS_VARS_AMOUNT)] - volumex;//x
        tempPoint[2] = (t*tVec) + volumeEndZ - volumez;//z
        
        tempPoint2[0] = ((( tempPoint[0] * objThetaCos ) - ( tempPoint[2] * objThetaSin ) )*one_Div_voxelSize) + volumeMiddle;
        tempPoint2[2] = (( ( (tempPoint[0] * objThetaSin) + (tempPoint[2] * objThetaCos) ) )*one_Div_voxelSize) + volumeMiddle;
        
        if (tempPoint2[1]>=startY && tempPoint2[1]<endY)
        {
            float retVal[2];
            size_t pointPosition;

            tempPoint2[1] = 1+tempPoint2[1]-startY;
            
            retVal[0] = tex3D(tex,tempPoint2[0],tempPoint2[1],tempPoint2[2]);
            retVal[1] = tex3D(texImaginary,tempPoint2[0],tempPoint2[1],tempPoint2[2]);

            pointPosition = cup2d(deviceValidPoints[cup2d(DETBASEX,myPosX,POINTS_VARS_AMOUNT)],deviceValidPoints[cup2d(DETBASEY,myPosX,POINTS_VARS_AMOUNT)],detectorWidth );

            atomicAdd ( &deviceDetector[pointPosition], retVal[0] );
            atomicAdd ( &deviceDetectorImaginary[pointPosition], retVal[1] ); 
        }  
    }
}

    __constant__ int detectorPlaneSize;
    __constant__ int detectorHeight;
    __constant__ int detectorMiddleWidth;
    __constant__ int detectorMiddleHeight;
    __constant__ float detectorRollSin;
    __constant__ float detectorRollCos;
    __constant__ float detectorPitchSin;
    __constant__ float detectorPitchCos;
    __constant__ float detectorYawSin;
    __constant__ float detectorYawCos;
    __constant__ float powConeA;
    __constant__ float powConeB;
    __constant__ float coneC;
    __constant__ float coneDistance;
    __constant__ float detectorPixelWidth;
    __constant__ float detectorPixelHeight;
    __constant__ float detectorPixelWidthCenter;
    __constant__ float detectorPixelHeightCenter;
    

__global__
void detectorProjectConeRollFirst(int *retPointAmount,float *detector, float raySampling) 
{
    int myPos = blockIdx.x*blockDim.x + threadIdx.x;
    
    if (myPos > detectorPlaneSize)
        return;

    float conePosZ = (-coneDistance);
    int y = myPos/detectorWidth;
    int x = myPos%detectorWidth;
    float ox,oy,wx,wy,wz,detZ;
    
    ox = detectorPixelWidthCenter + ( (x-detectorMiddleWidth)*detectorPixelWidth ); 
    oy = detectorPixelHeightCenter + ( (y-detectorMiddleHeight)*detectorPixelWidth ); 
    
    wx = ox*detectorRollCos - oy*detectorRollSin;
    wy = ox*detectorRollSin + oy*detectorRollCos;
    
    wx = wx*detectorPitchCos;
    wy = wy*detectorYawCos;
    
    detZ = wy*detectorYawSin + wx*detectorPitchSin;
    wz = coneC*(sqrt( pow(wx,2)/powConeA + pow(wy,2)/powConeB )) - coneDistance;
    
    if (wz <= detZ)
    {
        detector[cudp3d(x,y,PV_PRESENCE,detectorWidth,detectorHeight)] = 1;
        detector[cudp3d(x,y,PV_X,detectorWidth,detectorHeight)] = wx;
        detector[cudp3d(x,y,PV_Y,detectorWidth,detectorHeight)] = wy;
        detector[cudp3d(x,y,PV_Z,detectorWidth,detectorHeight)] = detZ;

        float tVec[3],t;
        float rayStartX,rayStartY,rayEndX,rayEndY;
        tVec[0] = wx;
        tVec[1] = wy;
        tVec[2] = wz - conePosZ;
    
        t = (volumeStartZ-conePosZ) / tVec[2];
        rayStartY = (t*tVec[1]) ;
        rayStartX = (t*tVec[0]) ;
        t = (volumeEndZ-conePosZ) / tVec[2];
        rayEndY = (t*tVec[1]) ;
        rayEndX = (t*tVec[0]) ;
        detector[cudp3d(x,y,PV_STEPSIZE,detectorWidth,detectorHeight)] = 
        (  sqrt(pow(rayStartX-rayEndX,2) + pow(rayStartY-rayEndY,2) + pow(volumeStartZ - volumeEndZ,2) )  ) / raySampling;

        atomicAdd(retPointAmount,1);
    }
}

__global__
void detectorProjectConeRollLater(int *retPointAmount,float *detector, float raySampling) 
{
    int myPos = blockIdx.x*blockDim.x + threadIdx.x;
    
    if (myPos > detectorPlaneSize)
        return;      
    int y = myPos/detectorMiddleWidth;
    int x = myPos%detectorMiddleHeight;
    float ox,oy,wx,wy,wz,detZ;
    
    ox = detectorPixelWidthCenter + ( (x-detectorMiddleWidth)*detectorPixelWidth ); 
    oy = detectorPixelHeightCenter + ( (y-detectorMiddleHeight)*detectorPixelWidth ); 
    
    ox = ox*detectorPitchCos;
    oy = oy*detectorYawCos;
    
    wx = ox*detectorRollCos - oy*detectorRollSin;
    wy = ox*detectorRollSin + oy*detectorRollCos;
    
    detZ = wy*detectorYawSin + wx*detectorPitchSin;
    wz = coneC*(sqrt( pow(wx,2)/powConeA + pow(wy,2)/powConeB )) - coneDistance;
    
    if (wz <= detZ)
    {
       detector[cudp3d(x,y,PV_PRESENCE,detectorWidth,detectorHeight)] = 1;
       detector[cudp3d(x,y,PV_X,detectorWidth,detectorHeight)] = wx;
       detector[cudp3d(x,y,PV_Y,detectorWidth,detectorHeight)] = wy;
       detector[cudp3d(x,y,PV_Z,detectorWidth,detectorHeight)] = detZ;

       float tVec[3],t;
       float rayStartX,rayStartY,rayEndX,rayEndY;
       tVec[0] = wx;
       tVec[1] = wy;
       tVec[2] = wz - (-coneDistance);
   
       t = (volumeStartZ-conePosZ) / tVec[2];
       rayStartY = (t*tVec[1]) ;
       rayStartX = (t*tVec[0]) ;
       t = (volumeEndZ-conePosZ) / tVec[2];
       rayEndY = (t*tVec[1]) ;
       rayEndX = (t*tVec[0]) ;
       detector[cudp3d(x,y,PV_STEPSIZE,detectorWidth,detectorHeight)] = (  sqrt(pow(rayStartX-rayEndX,2) + pow(rayStartY-rayEndY,2) + pow(volumeStartZ - volumeEndZ,2) )  ) / raySampling;


       atomicAdd(retPointAmount,1);          
    }
}

extern "C"
void deleteCufftComplex(void *cudaComplex)
{
    cufftComplex *complex = (cufftComplex*)cudaComplex;
    free(complex);
}

extern "C"
void *mallocCufftComplex(size_t elements)
{
    cufftComplex *complex = (cufftComplex*)malloc(elements*sizeof(cufftComplex));
    return complex;
}

extern "C"
size_t getSizeOfCufftComplex()
{
    return sizeof(cufftComplex);
};

extern "C"
void copyToDeviceMemory(void *sourceElement,void **destinationPointer,size_t size)
{
    HANDLE_ERROR(cudaMalloc(destinationPointer,size);)
    HANDLE_ERROR(cudaMemcpy((*destinationPointer),sourceElement,size,cudaMemcpyHostToDevice);)
}

extern "C"
void freeOnDevice(void *deviceMemoryPointer)
{
    HANDLE_ERROR(  cudaFree(deviceMemoryPointer); )
}

extern "C"
void memsetInDevice(void *deviceMemoryPointer, int value, size_t count)
{
    HANDLE_ERROR(  cudaMemset(deviceMemoryPointer,value,count); )
}

extern "C"
void mallocOnDevice(void ** devPtr,size_t size)
{
    HANDLE_ERROR(  cudaMalloc(devPtr,size); )
}

extern "C"
void memcpyToDevice(void * devPtr,void * srcPtr,size_t size)
{
    HANDLE_ERROR(  cudaMemcpy(devPtr,srcPtr,size,cudaMemcpyHostToDevice); )
}

extern "C"
void memcpyToDeviceCufftComplexPos(void * devPtr,void * srcPtr,size_t size,size_t pos)
{
    printf("Pos %lu, Pointer %p\n",pos,srcPtr);
    cufftComplex * data = (cufftComplex *)srcPtr;
    HANDLE_ERROR(  cudaMemcpy(devPtr,&data[pos],size,cudaMemcpyHostToDevice); )
}

extern "C"
void mcudaSetDevice(int dev)
{
    cudaSetDevice(dev);
}

extern "C"
void freeArrayOnDevice(void *deviceMemoryPointer)
{
    cudaArray *arr = (cudaArray*)deviceMemoryPointer;
    HANDLE_ERROR(  cudaFreeArray(arr); )
}

bool is_infinity_is_denormal(float f) {
    if (isinf(f)) return true;
    if (isnan(f)) return true;
    return false;
}

extern "C"
void cudaCalculateProjection(float conePosXarg, float conePosYarg, size_t pointsStart,size_t pointsAmount,int planeIndex,
float theta,float zNow, int startYa,int endYa,int sliceHeight,int * intsa,int * deviceIntsa,float *floatsa,
float *devicefloatsa,float * detector,float * detectorImaginary,float *deviceDetector,float *deviceDetectorImaginary,
void *d_volumeArrayar,void *d_volumeArrayarImaginary,float *deviceValidPoints, float *detectorPixelData)
{
    cudaEvent_t start, stop;
    cudaEventCreate(&start);
    cudaEventCreate(&stop);
    cudaArray *d_volumeArray = (cudaArray*)d_volumeArrayar;

    cudaArray *d_volumeArrayImaginary;
    d_volumeArrayImaginary = (cudaArray*)d_volumeArrayarImaginary;
    
    float milliseconds = 0;
    dim3 blocksPerGrid(1,1,1);
    dim3 threadsPerBlock(1,1,1);

    float projectionfloats[VP_DOUBLEVECTOR_SIZE];
    int projectionInts[VP_INTVECTOR_SIZE];
    //cudaError_t err;
    long startPos=0;
    int i =0;
    long neededBlocks;
    int kernelsPerJob;
    
    if (pointsAmount<=0)
        return;
    
    threadsPerBlock.x = BLOCKXSIZE;
    threadsPerBlock.y = BLOCKYSIZE;
    neededBlocks = ceil((float)pointsAmount/(float)threadsPerBlock.x);
    blocksPerGrid.x = 4096;
    blocksPerGrid.y = ceil((float)intsa[V_RAYSAMPLING]/(float)threadsPerBlock.y);
    
    sincos(theta,&projectionfloats[VP_OBJECTTHETASIN],&projectionfloats[VP_OBJECTTHETACOS]);
    projectionInts[VP_STARTY] = startYa;
    projectionInts[VP_ENDY] = endYa;

    float conePosYMinusvolumeYtemp= conePosYarg - floatsa[V_VOLUMEY];
    float conePosXMinusvolumeXtemp = conePosXarg - floatsa[V_VOLUMEX];
    float tempone_Div_voxelSize = 1.0 / floatsa[V_VOXEL_SIZE];
    float tempvolumeStartZ = (zNow + ( (float)(intsa[V_VOLUMEMIDDLE]-1) * floatsa[V_VOXEL_SIZE]) );
    float tempvolumeEndZ = (zNow - ( (float)(intsa[V_VOLUMEMIDDLE]) * floatsa[V_VOXEL_SIZE]) );

    HANDLE_ERROR(cudaMemcpyToSymbol(volumeMiddle,&intsa[V_VOLUMEMIDDLE],sizeof(int)); )
    HANDLE_ERROR(cudaMemcpyToSymbol(startY,&projectionInts[VP_STARTY],sizeof(int)); )    
    HANDLE_ERROR(cudaMemcpyToSymbol(endY,&projectionInts[VP_ENDY],sizeof(int)); )    
 
    HANDLE_ERROR(cudaMemcpyToSymbol(volumeSize,&intsa[V_VOLUMESIZE],sizeof(int)); )    
    HANDLE_ERROR(cudaMemcpyToSymbol(detectorWidth,&intsa[V_DETECTORWIDTH],sizeof(int)); )   

    
    HANDLE_ERROR(cudaMemcpyToSymbol(validPointAmount,&intsa[V_VALIDDETECTORPOINTS],sizeof(int)); )    
    HANDLE_ERROR(cudaMemcpyToSymbol(raySamplingDouble,&floatsa[V_RAYSAMPLINGDOUBLE],sizeof(float)); )    
    HANDLE_ERROR(cudaMemcpyToSymbol(raySamplingInt,&intsa[V_RAYSAMPLING],sizeof(int)); )    
    HANDLE_ERROR(cudaMemcpyToSymbol(objThetaSin,&projectionfloats[VP_OBJECTTHETASIN],sizeof(float)); )    
    HANDLE_ERROR(cudaMemcpyToSymbol(objThetaCos,&projectionfloats[VP_OBJECTTHETACOS],sizeof(float)); )    
    HANDLE_ERROR(cudaMemcpyToSymbol(volumex,&floatsa[V_VOLUMEX],sizeof(float)); )    
    HANDLE_ERROR(cudaMemcpyToSymbol(volumey,&floatsa[V_VOLUMEY],sizeof(float)); )    
    HANDLE_ERROR(cudaMemcpyToSymbol(volumez,&zNow,sizeof(float)); )  //old  floatsa[V_VOLUMEZ] 

    HANDLE_ERROR(cudaMemcpyToSymbol(volumeStartZ,&tempvolumeStartZ,sizeof(float)); )    
    HANDLE_ERROR(cudaMemcpyToSymbol(volumeEndZ,&tempvolumeEndZ,sizeof(float)); ) 
    
    HANDLE_ERROR(cudaMemcpyToSymbol(conePosZMinusVolumeZ,&floatsa[V_CONEPOSZ_MINUS_VOLUMEZ],sizeof(float)); )    
    HANDLE_ERROR(cudaMemcpyToSymbol(volumeYMinusVolumeMiddle,&floatsa[V_VOLUMEY_MINUS_VOLUMEMIDDLE],sizeof(float)); )
    HANDLE_ERROR(cudaMemcpyToSymbol(conePosZ,&floatsa[V_CONEPOSZ],sizeof(float)); ) 
    HANDLE_ERROR(cudaMemcpyToSymbol(one_Div_voxelSize,&tempone_Div_voxelSize,sizeof(float)); ) 

    HANDLE_ERROR(cudaMemcpyToSymbol(conePosX,&conePosXarg,sizeof(float)); )
    HANDLE_ERROR(cudaMemcpyToSymbol(conePosY,&conePosYarg,sizeof(float)); )

    HANDLE_ERROR(cudaMemcpyToSymbol(conePosYMinusvolumeY,&conePosYMinusvolumeYtemp,sizeof(float)); )
    HANDLE_ERROR(cudaMemcpyToSymbol(conePosXMinusvolumeX,&conePosXMinusvolumeXtemp,sizeof(float)); )

    kernelsPerJob = ceil(neededBlocks/(float)blocksPerGrid.x);
    //printf("BlocksPerGrid: %d Kernels %d \n",blocksPerGrid.x,kernelsPerJob);
    if (blocksPerGrid.x>neededBlocks)
        kernelsPerJob=1; //we may need to launch several kernels in huge problems
    //printf("Kernels %d \n",kernelsPerJob);
    cudaEventRecord(start);
    if (intsa[V_POINTMODE]==PT_NEAREST_NEIGHBOR)
    {
        tex.filterMode = cudaFilterModePoint;      // nearest neighbor
        texImaginary.filterMode = cudaFilterModePoint; 
    }else
    {
        tex.filterMode = cudaFilterModeLinear; // linear interpolation
        texImaginary.filterMode = cudaFilterModeLinear;
    }
    tex.addressMode[0] = cudaAddressModeBorder;  
    tex.addressMode[1] = cudaAddressModeBorder;
    tex.addressMode[2] = cudaAddressModeBorder;
    cudaChannelFormatDesc channelDesc = cudaCreateChannelDesc<VolumeType>();
    cudaBindTextureToArray(tex, d_volumeArray, channelDesc);

    texImaginary.addressMode[0] = cudaAddressModeBorder; 
    texImaginary.addressMode[1] = cudaAddressModeBorder;
    texImaginary.addressMode[2] = cudaAddressModeBorder;
    cudaBindTextureToArray(texImaginary, d_volumeArrayImaginary, channelDesc);

    for(i=0;i<kernelsPerJob;i++)
    {
        startPos = i*blocksPerGrid.x*threadsPerBlock.x;
        if (i==kernelsPerJob-1)
            blocksPerGrid.x = neededBlocks-(startPos/threadsPerBlock.x);
        if (intsa[V_BEAMTYPE]==B_CONE)
            calculateLineSumCuComplex<<<blocksPerGrid,threadsPerBlock>>>(startPos+pointsStart,deviceDetector,deviceDetectorImaginary,deviceValidPoints);
        if (intsa[V_BEAMTYPE]==B_PARALLEL)
            calculateLineSumCuParallelComplex<<<blocksPerGrid,threadsPerBlock>>>(startPos+pointsStart,deviceDetector,deviceDetectorImaginary,deviceValidPoints);
    }


     //cudaDeviceSynchronize();
     cudaEventRecord(stop);
     cudaEventSynchronize(stop);
     cudaEventElapsedTime(&milliseconds, start, stop);
     printf("Projection #%d - Angle %f -Ray Casting kernel Time: %f ms\n",planeIndex,theta, milliseconds);
     //err = cudaPeekAtLastError();
    // if( err != cudaSuccess)
    //// {
     //    printf("Kernel2 CUDA error: %s\n", cudaGetErrorString(err));
    // }
     
     cudaEventDestroy(start);
     cudaEventDestroy(stop);
     HANDLE_ERROR(  cudaMemcpy(&detector[dp3d(0,0,planeIndex,intsa[V_DETECTORWIDTH],intsa[V_DETECTORHEIGHT])],deviceDetector,(size_t)intsa[V_DETECTORWIDTH]*(size_t)intsa[V_DETECTORHEIGHT]*sizeof(float),cudaMemcpyDeviceToHost); )
     cudaDeviceSynchronize();
     HANDLE_ERROR(  cudaMemcpy(&detectorImaginary[dp3d(0,0,planeIndex,intsa[V_DETECTORWIDTH],intsa[V_DETECTORHEIGHT])],deviceDetectorImaginary,(size_t)intsa[V_DETECTORWIDTH]*(size_t)intsa[V_DETECTORHEIGHT]*sizeof(float),cudaMemcpyDeviceToHost); )
     cudaDeviceSynchronize(); 
}


extern "C"
void validateProjection(float conePosXarg, float conePosYarg, size_t pointsStart,size_t pointsAmount,int planeIndex,
float theta,float zNow, int startYa,int endYa,int sliceHeight,int * intsa,int * deviceIntsa,float *floatsa,
float *devicefloatsa,float * detector,float * detectorImaginary,float *deviceDetector,float *deviceDetectorImaginary,
void *d_volumeArrayar,void *d_volumeArrayarImaginary,float *deviceValidPoints, float *detectorPixelData)
{

    float *validateDetector, *validateDetectorImaginary;
    validateDetector = (float*)malloc(sizeof(float)*(size_t)intsa[V_DETECTORPLANESIZE]);
    validateDetectorImaginary = (float*)malloc(sizeof(float)*(size_t)intsa[V_DETECTORPLANESIZE]);
    float vx,vy;
    float conePosZ;
    float rBase = 0.9*0.9;
    float sphereR = (rBase*floatsa[V_VOXEL_SIZE]*(float)intsa[V_VOLUMESIZE])/2;
    float obZ = floatsa[V_VOLUMEZ];
    float a,b,c;
    float t1,t2;
    float mag;
    conePosZ = floatsa[V_CONEPOSZ];
    float ro,beta;
    ro = 5.77045e-06;
    beta = 6.2021e-09;
    float k,ix,iy,iz,fx,fy,fz;

    int halfwidth = intsa[V_DETECTOR_MIDDLEWIDTH];
    int halfheight = intsa[V_DETECTOR_MIDDLEHEIGHT];
    float detectorPixelWidthCenter = floatsa[V_DETECTOR_PIXEL_WIDTH]/2.0;
    float detectorPixelHeightCenter = floatsa[V_DETECTOR_PIXEL_HEIGHT]/2.0;
    float wz = conePosZ-obZ;
    float res1,res2;
    printf("RBase Analytic: %f\n",rBase);
    int i=0;
    for(i=0;i<intsa[V_DETECTORWIDTH];i++)
    {
        vx = detectorPixelWidthCenter+ (((i - halfwidth )*(-1))*floatsa[V_DETECTOR_PIXEL_WIDTH]);
        for(k=0;k<intsa[V_DETECTORHEIGHT];k++)
        {
            vy = detectorPixelHeightCenter + ((k-halfheight)*floatsa[V_DETECTOR_PIXEL_HEIGHT]);

            a = pow(vx,2) + pow(vy,2) + pow(-conePosZ,2);
            b = (-2) * ( wz * conePosZ);
            c = pow(wz,2) - pow(sphereR,2);

            t1 = (-b + sqrt( pow(b,2) - (4*a*c) ) )/( 2* a );
            t2 = (-b - sqrt( pow(b,2) - (4*a*c) ) )/( 2* a );

            fx = t1*(vx);
            fy = t1*(vy);   
            fz = t1*(-conePosZ) + conePosZ;
            ix = t2*(vx);
            iy = t2*(vy);
            iz = t2*(-conePosZ) + conePosZ;
            mag = sqrt( pow( ix - fx,2) + pow(iy-fy,2) + pow(iz-fz,2) );
            res1=mag*ro*floatsa[V_DETECTOR_PIXEL_HEIGHT]*floatsa[V_DETECTOR_PIXEL_WIDTH];
            res2=mag*beta*floatsa[V_DETECTOR_PIXEL_HEIGHT]*floatsa[V_DETECTOR_PIXEL_WIDTH];
            if (!(is_infinity_is_denormal(res1) && is_infinity_is_denormal(res2) ) )
            {
                validateDetector[p2d(i,k,intsa[V_DETECTORWIDTH])] = res1;
                validateDetectorImaginary[p2d(i,k,intsa[V_DETECTORWIDTH])] = res2;
            }
            else
            {
                validateDetector[p2d(i,k,intsa[V_DETECTORWIDTH])] =0;
                validateDetectorImaginary[p2d(i,k,intsa[V_DETECTORWIDTH])] =0;
            }
            detector[dp3d(i,k,0,intsa[V_DETECTORWIDTH],intsa[V_DETECTORHEIGHT])] = floatsa[V_DETECTOR_PIXEL_HEIGHT]*floatsa[V_DETECTOR_PIXEL_WIDTH]*detectorPixelData[ dp3d(i,k,PV_STEPSIZE,intsa[V_DETECTORWIDTH],intsa[V_DETECTORHEIGHT]) ]*detector[dp3d(i,k,planeIndex,intsa[V_DETECTORWIDTH],intsa[V_DETECTORHEIGHT])]; 
        }
    }

    printFloat2RadonFromHost(validateDetector,&detector[dp3d(0,0,0,intsa[V_DETECTORWIDTH],intsa[V_DETECTORHEIGHT])],intsa[V_DETECTORWIDTH],intsa[V_DETECTORHEIGHT]);
    sleep(4);
    printf("VALIDATE: \n");
    printFloatImageFromHost(validateDetector,intsa[V_DETECTORWIDTH],intsa[V_DETECTORHEIGHT]);
    sleep(4);
    printf("DISCRETE: \n");
    printFloatImageFromHost(&detector[dp3d(0,0,0,intsa[V_DETECTORWIDTH],intsa[V_DETECTORHEIGHT])],intsa[V_DETECTORWIDTH],intsa[V_DETECTORHEIGHT]);
    exit(0);
}

void applyForwardFFTtoCufftComplex(void *complex,size_t width,size_t height)
{
    cudaError_t err;
    cufftHandle plan;
    cufftResult_t cufftResult;
    cufftComplex * complexMatrix = (cufftComplex *)complex;
    cufftPlan2d(&plan,width,height,CUFFT_C2C);

    cufftResult = cufftExecC2C(plan,complexMatrix,complexMatrix,CUFFT_FORWARD);

    if (cufftResult!=CUFFT_SUCCESS)
    {
        printf("Cufft Kernel fft Err: %d",cufftResult);
        exit(EXIT_FAILURE);
    }
    cudaDeviceSynchronize();
    err = cudaPeekAtLastError();
    if( err != cudaSuccess)
    {
        printf("Compose Kernel CUDA error: %s\n", cudaGetErrorString(err));
    }
    cufftDestroy(plan);

}




extern "C"
void cudaComposeRadiation(int planeIndex,int * intsa,float *floatsa,
    float * detector,float * detectorImaginary,float *deviceDetector,float *deviceDetectorImaginary,void*deviceCufftDetectorarg,
    float *deviceValidPoints, void *deviceKernelMatrixarg, float *f10Vec, float *detectorPixelData,double hsumx,double hsumy)
{
    cudaError_t err;
    cudaEvent_t start, stop;
    cudaEventCreate(&start);
    cudaEventCreate(&stop);
    cufftHandle plan;
    cufftResult_t cufftResult;

    cufftComplex * deviceCufftDetector = (cufftComplex *)deviceCufftDetectorarg;
    cufftComplex * deviceKernelMatrix = (cufftComplex *)deviceKernelMatrixarg;
    

    /*For manual convolution tests only
    cufftComplex * deviceDetectorCufftOut;
    HANDLE_ERROR( cudaMalloc((void **)&deviceDetectorCufftOut,(size_t)intsa[V_DETECTORWIDTH]*(size_t)intsa[V_DETECTORHEIGHT]*sizeof(cufftComplex)); )
    cudaMemset(deviceDetectorCufftOut,0,(size_t)intsa[V_DETECTORWIDTH]*(size_t)intsa[V_DETECTORHEIGHT]*sizeof(cufftComplex));
    */

    double kSumReal,minusKSumImag,kSumRealImagSquared;

    kSumReal = hsumx;
    minusKSumImag = -hsumy;
    kSumRealImagSquared = pow(hsumx,2) + pow(hsumy,2);

    //printf("ksum.x %f ksum.y %f \n",hsumx,hsumy);
    printf("Propagating Projection #%d \n",planeIndex);
    
    dim3 blocksPerGridComposeKernel(1,1,1);
    dim3 threadsPerBlockComposeKernel(1,1,1);

    dim3 blocksPerGridMultiplyKernel(1,1,1);
    dim3 threadsPerBlockMultiplyKernel(1,1,1);

    dim3 blocksPerGridFinishKernel(1,1,1);
    dim3 threadsPerBlockFinishKernel(1,1,1);



    threadsPerBlockMultiplyKernel.x = 256;
    blocksPerGridMultiplyKernel.x = ceil(((double)intsa[V_DETECTORPLANESIZE]*FFT_PADDINGX*FFT_PADDINGY)/(double)threadsPerBlockMultiplyKernel.x);
    
    threadsPerBlockFinishKernel.x = 32;
    threadsPerBlockFinishKernel.y = 32;
    blocksPerGridFinishKernel.x = ceil((double)intsa[V_DETECTORWIDTH]/(double)threadsPerBlockFinishKernel.x);
    blocksPerGridFinishKernel.y = ceil((double)intsa[V_DETECTORHEIGHT]/(double)threadsPerBlockFinishKernel.y);

    threadsPerBlockComposeKernel.x = 32;
    threadsPerBlockComposeKernel.y = 32;
    blocksPerGridComposeKernel.x = ceil((double)intsa[V_DETECTORWIDTH]/(double)threadsPerBlockComposeKernel.x);
    blocksPerGridComposeKernel.y = ceil((double)intsa[V_DETECTORHEIGHT]/(double)threadsPerBlockComposeKernel.y);

    float temppropagations2Pi_div_lambda = ( (2*M_PI) / floatsa[V_LAMBDA] ) * 0.000001;//multply by 10⁶ to go to lambda² in microns
    float tempPropagationE0 = 1;
    size_t tempDetectorSize = ((size_t)intsa[V_DETECTORPLANESIZE]) * FFT_PADDINGX * FFT_PADDINGY;
    float detectorPixelArea = floatsa[V_DETECTOR_PIXEL_WIDTH] * floatsa[V_DETECTOR_PIXEL_HEIGHT];
    
    float temptimesPixelSizeDivByGridPoints = (detectorPixelArea)/floatsa[V_GRIDPOINTS];

    //printf("Lambda %f -2Pi/Lambda %f\n", floatsa[V_LAMBDA],temppropagations2Pi_div_lambda);
    
    HANDLE_ERROR(cudaMemcpyToSymbol(fp_kSumReal ,&kSumReal,sizeof(double)); )    
    HANDLE_ERROR(cudaMemcpyToSymbol(fp_minusKSumImag,&minusKSumImag,sizeof(double)); )
    HANDLE_ERROR(cudaMemcpyToSymbol(fp_kSumRealImagSquared ,&kSumRealImagSquared,sizeof(double)); )

    HANDLE_ERROR(cudaMemcpyToSymbol(propagationE0,&tempPropagationE0,sizeof(float)); )    
    HANDLE_ERROR(cudaMemcpyToSymbol(propagations2Pi_div_lambda,&temppropagations2Pi_div_lambda,sizeof(float)); )
    HANDLE_ERROR(cudaMemcpyToSymbol(lambda,&floatsa[V_LAMBDA],sizeof(float)); )
    
    HANDLE_ERROR(cudaMemcpyToSymbol(timesPixelSizeDivByGridPoints,&temptimesPixelSizeDivByGridPoints,sizeof(float)); )

    HANDLE_ERROR(cudaMemcpyToSymbol(detectorWidth,&intsa[V_DETECTORWIDTH],sizeof(int)); )   

    HANDLE_ERROR(cudaMemcpyToSymbol(detectorHeightC,&intsa[V_DETECTORHEIGHT],sizeof(int)); )   
    
    HANDLE_ERROR(cudaMemcpyToSymbol(validPointAmount,&intsa[V_VALIDDETECTORPOINTS],sizeof(int)); )
    HANDLE_ERROR(cudaMemcpyToSymbol(detectorSize,&tempDetectorSize,sizeof(size_t)); )

    HANDLE_ERROR( cudaMemcpy(deviceDetector, &detector[dp3d(0,0,planeIndex,intsa[V_DETECTORWIDTH],intsa[V_DETECTORHEIGHT])],(size_t)intsa[V_DETECTORWIDTH]*(size_t)intsa[V_DETECTORHEIGHT]*sizeof(float),cudaMemcpyHostToDevice); )
    
    //sleep(2);
    //printFloatImageFromHost(&detector[dp3d(0,0,planeIndex,intsa[V_DETECTORWIDTH],intsa[V_DETECTORHEIGHT])],intsa[V_DETECTORWIDTH],intsa[V_DETECTORHEIGHT]);
    //sleep(2);
    //printFloatImageFromHost(&detectorImaginary[dp3d(0,0,planeIndex,intsa[V_DETECTORWIDTH],intsa[V_DETECTORHEIGHT])],intsa[V_DETECTORWIDTH],intsa[V_DETECTORHEIGHT]);

    HANDLE_ERROR( cudaMemcpy(deviceDetectorImaginary, &detectorImaginary[dp3d(0,0,planeIndex,intsa[V_DETECTORWIDTH],intsa[V_DETECTORHEIGHT])],(size_t)intsa[V_DETECTORWIDTH]*(size_t)intsa[V_DETECTORHEIGHT]*sizeof(float),cudaMemcpyHostToDevice); )
    
    
    //sleep(2);
    //printCufftComplexFromDevice(deviceKernelMatrix,intsa[V_DETECTORWIDTH]*FFT_PADDINGX,intsa[V_DETECTORHEIGHT]*FFT_PADDINGY);
    //if (f10Vec[planeIndex]<floatsa[V_FRESNEL_NEARCUT])// && f10Vec[planeIndex]>FRESNEL_START_F10REGION) //uncomment when making fraunhoffer
    //{

        composeRadiation<<<blocksPerGridComposeKernel,threadsPerBlockComposeKernel>>>(deviceDetector,deviceDetectorImaginary,deviceCufftDetector,deviceValidPoints,detectorPixelData);
        cudaDeviceSynchronize();
        //sleep(2);
        //printCufftComplexArctanFromDevice(deviceCufftDetector,intsa[V_DETECTORWIDTH]*FFT_PADDINGX,intsa[V_DETECTORHEIGHT]*FFT_PADDINGY);

        

        //printf("p1: %lu, p2: %lu \n",(size_t)(((size_t)intsa[V_DETECTORWIDTH])*FFT_PADDINGX),(size_t)(((size_t)intsa[V_DETECTORHEIGHT])*FFT_PADDINGY));
        cufftResult = cufftPlan2d(&plan,((size_t)intsa[V_DETECTORWIDTH])*FFT_PADDINGX,((size_t)intsa[V_DETECTORHEIGHT])*FFT_PADDINGY,CUFFT_C2C);
        if (cufftResult!=CUFFT_SUCCESS)
        {
            printf("Cufft Plan1 Err: %d",cufftResult);
            exit(EXIT_FAILURE);
        }
        cufftResult = cufftExecC2C(plan,deviceCufftDetector,deviceCufftDetector,CUFFT_FORWARD);
        if (cufftResult!=CUFFT_SUCCESS)
        {
            printf("Cufft Exec1 Err: %d",cufftResult);
            exit(EXIT_FAILURE);
        }

        pointToPointComplexMatrixMultiplication<<<blocksPerGridMultiplyKernel,threadsPerBlockMultiplyKernel>>>(deviceCufftDetector,deviceKernelMatrix);
        cudaDeviceSynchronize();
        
        err = cudaPeekAtLastError();
        if( err != cudaSuccess)
        {
            printf("Compose Kernel CUDA error: %s\n", cudaGetErrorString(err));
        }
        cufftResult = cufftExecC2C(plan,deviceCufftDetector,deviceCufftDetector,CUFFT_INVERSE);
        if (cufftResult!=CUFFT_SUCCESS)
        {
            printf("Cufft Exec2 Err: %d",cufftResult);
            exit(EXIT_FAILURE);
        }
        cufftDestroy(plan);

        
        
        //sleep(2);
        //printCufftComplexArctanFromDevice(deviceCufftDetector,intsa[V_DETECTORWIDTH]*FFT_PADDINGX,intsa[V_DETECTORHEIGHT]*FFT_PADDINGY);

        finishPropagation<<<blocksPerGridFinishKernel,threadsPerBlockFinishKernel>>>(deviceDetector,deviceCufftDetector,deviceValidPoints);
        cudaDeviceSynchronize();
        



   // }
   // if (f10Vec[planeIndex]>=floatsa[V_FRESNEL_NEARCUT])
   // {
       // sleep(2);
       // printCufftComplexFromDevice(deviceCufftDetector,intsa[V_DETECTORWIDTH]*FFT_PADDINGX,intsa[V_DETECTORHEIGHT]*FFT_PADDINGY);
  
 //       composeRadiationNearImage<<<blocksPerGridComposeKernel,threadsPerBlockComposeKernel>>>(deviceDetector,deviceDetectorImaginary,deviceCufftDetector,deviceValidPoints, detectorPixelData);
  //      cudaDeviceSynchronize();
        //sleep(2);
        //printFloatImageFromHost(&detector[dp3d(0,0,planeIndex,intsa[V_DETECTORWIDTH],intsa[V_DETECTORHEIGHT])],intsa[V_DETECTORWIDTH],intsa[V_DETECTORHEIGHT]);
        //sleep(2);
        //printFloatImageFromHost(&detectorImaginary[dp3d(0,0,planeIndex,intsa[V_DETECTORWIDTH],intsa[V_DETECTORHEIGHT])],intsa[V_DETECTORWIDTH],intsa[V_DETECTORHEIGHT]);

   // }
    //if (f10Vec[planeIndex]<=floatsa[V_FRESNEL_NEARCUT])
    //{
        //fraunhofer
    //}

    err = cudaPeekAtLastError();
    if( err != cudaSuccess)
    {
        printf("Compose Kernel CUDA error: %s\n", cudaGetErrorString(err));
    }
    
    cudaEventDestroy(start);
    cudaEventDestroy(stop);
    HANDLE_ERROR(  cudaMemcpy(&detector[dp3d(0,0,planeIndex,intsa[V_DETECTORWIDTH],intsa[V_DETECTORHEIGHT])],deviceDetector,(size_t)intsa[V_DETECTORWIDTH]*(size_t)intsa[V_DETECTORHEIGHT]*sizeof(float),cudaMemcpyDeviceToHost); )
    
}

extern "C"
void cudaPrepareCone(int * intsa,float *floatsa,float **coneProjection,float **detectorPixelValues,float **validPoints)
{
    dim3 blocksPerGrid(1,1,1);
    dim3 threadsPerBlock(1,1,1);
    int x,y;
    cudaEvent_t start, stop;
    cudaEventCreate(&start);
    cudaEventCreate(&stop);
    float milliseconds = 0;
    cudaError_t err;
    threadsPerBlock.x = 128;
    blocksPerGrid.x = ceil((float)intsa[V_DETECTORPLANESIZE]/(float)threadsPerBlock.x);
    int validPointAmount = 0;
    int *deviceValidPointAmount;
    float *hostDetector;
    float *deviceDetector;
    float projectionfloats[VCP_DOUBLEVECTOR_SIZE];

    float tempDetectorPixelWidthCenter = floatsa[V_DETECTOR_PIXEL_WIDTH]/2.0;
    float tempDetectorPixelHeightCenter = floatsa[V_DETECTOR_PIXEL_HEIGHT]/2.0;

    float tempvolumeStartZ = (floatsa[V_VOLUMEZ] + ( (float)(intsa[V_VOLUMEMIDDLE]-1) * floatsa[V_VOXEL_SIZE]) );
    float tempvolumeEndZ = (floatsa[V_VOLUMEZ] - ( (float)(intsa[V_VOLUMEMIDDLE]) * floatsa[V_VOXEL_SIZE]) );


    HANDLE_ERROR( cudaMalloc((void **)&deviceValidPointAmount,sizeof(int)); )
    HANDLE_ERROR( cudaMalloc((void**)&deviceDetector,intsa[V_DETECTORPLANESIZE]*PV_VECTORSIZE*sizeof(float)); )

    sincos(floatsa[V_DETECTORROLL],&projectionfloats[VCP_DETECTOR_ROLL_SIN],&projectionfloats[VCP_DETECTOR_ROLL_COS]);
    sincos(floatsa[V_DETECTORPITCH],&projectionfloats[VCP_DETECTOR_PITCH_SIN],&projectionfloats[VCP_DETECTOR_PITCH_COS]);
    sincos(floatsa[V_DETECTORYAW],&projectionfloats[VCP_DETECTOR_YAW_SIN],&projectionfloats[VCP_DETECTOR_YAW_COS]);
    projectionfloats[VCP_DETECTOR_POWCONEA] = pow(floatsa[V_CONEA],2);
    projectionfloats[VCP_DETECTOR_POWCONEB] = pow(floatsa[V_CONEB],2);

    cudaMemset(deviceDetector,0,(size_t)intsa[V_DETECTORPLANESIZE]*PV_VECTORSIZE*sizeof(float));
    cudaMemcpy(deviceValidPointAmount,&validPointAmount,sizeof(int),cudaMemcpyHostToDevice);
    
    HANDLE_ERROR(cudaMemcpyToSymbol(detectorPlaneSize,&intsa[V_DETECTORPLANESIZE],sizeof(int)); )   
    HANDLE_ERROR(cudaMemcpyToSymbol(detectorWidth,&intsa[V_DETECTORWIDTH],sizeof(int)); )   
    HANDLE_ERROR(cudaMemcpyToSymbol(detectorHeight,&intsa[V_DETECTORHEIGHT],sizeof(int)); ) 
    HANDLE_ERROR(cudaMemcpyToSymbol(detectorMiddleWidth,&intsa[V_DETECTOR_MIDDLEWIDTH],sizeof(int)); )   
    HANDLE_ERROR(cudaMemcpyToSymbol(detectorMiddleHeight,&intsa[V_DETECTOR_MIDDLEHEIGHT],sizeof(int)); ) 

    HANDLE_ERROR(cudaMemcpyToSymbol(volumeStartZ,&tempvolumeStartZ,sizeof(float)); )    
    HANDLE_ERROR(cudaMemcpyToSymbol(volumeEndZ,&tempvolumeEndZ,sizeof(float)); ) 
    

    HANDLE_ERROR(cudaMemcpyToSymbol(coneC,&floatsa[V_CONEC],sizeof(float)); )
    HANDLE_ERROR(cudaMemcpyToSymbol(detectorPixelWidth,&floatsa[V_DETECTOR_PIXEL_WIDTH],sizeof(float)); )
    HANDLE_ERROR(cudaMemcpyToSymbol(detectorPixelHeight,&floatsa[V_DETECTOR_PIXEL_HEIGHT],sizeof(float)); )
    HANDLE_ERROR(cudaMemcpyToSymbol(powConeA,&projectionfloats[VCP_DETECTOR_POWCONEA],sizeof(float)); )
    HANDLE_ERROR(cudaMemcpyToSymbol(powConeB,&projectionfloats[VCP_DETECTOR_POWCONEB],sizeof(float)); )
    HANDLE_ERROR(cudaMemcpyToSymbol(detectorRollCos,&projectionfloats[VCP_DETECTOR_ROLL_COS],sizeof(float)); )
    HANDLE_ERROR(cudaMemcpyToSymbol(detectorRollSin,&projectionfloats[VCP_DETECTOR_ROLL_SIN],sizeof(float)); )
    HANDLE_ERROR(cudaMemcpyToSymbol(detectorPitchCos,&projectionfloats[VCP_DETECTOR_PITCH_COS],sizeof(float)); )
    HANDLE_ERROR(cudaMemcpyToSymbol(detectorPitchSin,&projectionfloats[VCP_DETECTOR_PITCH_SIN],sizeof(float)); )
    HANDLE_ERROR(cudaMemcpyToSymbol(detectorYawCos,&projectionfloats[VCP_DETECTOR_YAW_COS],sizeof(float)); )
    HANDLE_ERROR(cudaMemcpyToSymbol(detectorYawSin,&projectionfloats[VCP_DETECTOR_YAW_SIN],sizeof(float)); )
    HANDLE_ERROR(cudaMemcpyToSymbol(coneDistance,&floatsa[V_CONEC],sizeof(float)); )

    HANDLE_ERROR(cudaMemcpyToSymbol(detectorPixelWidthCenter,&tempDetectorPixelWidthCenter,sizeof(float)); )
    HANDLE_ERROR(cudaMemcpyToSymbol(detectorPixelHeightCenter,&tempDetectorPixelHeightCenter,sizeof(float)); )

    
    cudaEventRecord(start);
    if (intsa[V_DETECTOR_ROLLMODE]==ROLL_FIRST)
        detectorProjectConeRollFirst<<<blocksPerGrid,threadsPerBlock>>>(deviceValidPointAmount,deviceDetector,intsa[V_RAYSAMPLING]);
    else
        detectorProjectConeRollLater<<<blocksPerGrid,threadsPerBlock>>>(deviceValidPointAmount,deviceDetector,intsa[V_RAYSAMPLING]);

    cudaEventRecord(stop);
    cudaEventElapsedTime(&milliseconds, start, stop);
    printf("Beam Surface Definition Time: %f ms\n", milliseconds);
    err = cudaPeekAtLastError();
    if( err != cudaSuccess)
    {
        printf("Kernel CUDA error: %s\n", cudaGetErrorString(err));
    }
    
    hostDetector =(float*)malloc(intsa[V_DETECTORPLANESIZE]*PV_VECTORSIZE*sizeof(float));
    if (hostDetector==NULL){printf("HD Malloc Failed to Allocate Memory \n");exit(EXIT_FAILURE);}

    cudaMemcpy(hostDetector,deviceDetector,intsa[V_DETECTORPLANESIZE]*PV_VECTORSIZE*sizeof(float),cudaMemcpyDeviceToHost);
    cudaMemcpy(&validPointAmount,deviceValidPointAmount,sizeof(int),cudaMemcpyDeviceToHost);

    intsa[V_VALIDDETECTORPOINTS] = validPointAmount;
    
    if (intsa[V_SHOW_GEOMETRY])
    {
        printFloatImageFromHost(hostDetector,intsa[V_DETECTORWIDTH],intsa[V_DETECTORHEIGHT]);
        printProblemGeometry(floatsa[V_CONE_LATERAL_OPENING],floatsa[V_CONEPOSZ]*(-1),intsa[V_VOLUMESIZE]*floatsa[V_VOXEL_SIZE],(floatsa[V_CONEPOSZ]-floatsa[V_VOLUMEZ])*(-1),intsa[V_DETECTORWIDTH]*floatsa[V_DETECTOR_PIXEL_WIDTH],intsa[V_DETECTORHEIGHT]*floatsa[V_DETECTOR_PIXEL_HEIGHT]);
    }
    //printf("Amount of Valid Points %d\n",intsa[V_VALIDDETECTORPOINTS]);
    
    (*validPoints) = (float*)malloc(validPointAmount*POINTS_VARS_AMOUNT*sizeof(float));
    if ((*validPoints)==NULL){printf("VP Malloc Failed to Allocate Memory \n");exit(EXIT_FAILURE);}
    float *vpoints = (*validPoints);
    int maxX = intsa[V_DETECTORWIDTH];
    int maxY = intsa[V_DETECTORHEIGHT];
    int pos=0;
    int posTemp=0;
    for(y=0;y<maxY;y++)
    {
        for(x=0;x<maxX;x++)
        {
            if(hostDetector[dp3d(x,y,0,maxX,maxY)]==0)
                continue;
            vpoints[p2d(DETBASEX,pos,POINTS_VARS_AMOUNT)] = x;
            vpoints[p2d(DETBASEY,pos,POINTS_VARS_AMOUNT)] = y;
            vpoints[p2d(DETREALX,pos,POINTS_VARS_AMOUNT)] = hostDetector[dp3d(x,y,1,intsa[V_DETECTORWIDTH],intsa[V_DETECTORHEIGHT])];
            vpoints[p2d(DETREALY,pos,POINTS_VARS_AMOUNT)] = hostDetector[dp3d(x,y,2,intsa[V_DETECTORWIDTH],intsa[V_DETECTORHEIGHT])];
            vpoints[p2d(DETREALZ,pos,POINTS_VARS_AMOUNT)] = hostDetector[dp3d(x,y,3,intsa[V_DETECTORWIDTH],intsa[V_DETECTORHEIGHT])];
            pos++;
            posTemp++;
            //printf("BaseX %d BaseY %d RealX %d RealY %d \n",vpoints[p2d(DETBASEX,pos,POINTS_VARS_AMOUNT)]);
        }
        
        posTemp=0;
        if (pos>=validPointAmount)
            break;
    }
    //free(hostDetector);
    (*detectorPixelValues) = hostDetector;
    (*coneProjection) = (float*)malloc(intsa[V_DETECTORPLANESIZE]*sizeof(float));
    if ((*coneProjection)==NULL){printf("CP Malloc Failed to Allocate Memory \n");exit(EXIT_FAILURE);}

    cudaMemcpy((*coneProjection),deviceDetector,(size_t)intsa[V_DETECTORPLANESIZE]*sizeof(float),cudaMemcpyDeviceToHost);
    cudaFree(deviceDetector);
    cudaFree(deviceValidPointAmount);
    //exit(0);
}

extern "C"
int getCudaDevicesAndMemory(size_t **devicesMemory)
{
    int deviceCount = 0;
    cudaError_t error_id = cudaGetDeviceCount(&deviceCount);
    size_t dummy = 0;
    size_t freeMemory = 0;
    size_t *memoryVals;

    if (error_id != cudaSuccess)
    {
        printf("cudaGetDeviceCount returned %d\n-> %s\n", (int)error_id, cudaGetErrorString(error_id));
        printf("Result = FAIL\n");
        exit(EXIT_FAILURE);
    }
    if (deviceCount == 0)
    {
        printf("There are no available device(s) that support CUDA\n");
        exit(EXIT_FAILURE);
    }
    else
        printf("Detected %d CUDA Capable device(s)\n", deviceCount);

    int dev;
    (*devicesMemory) = (size_t*)malloc(sizeof(size_t)*deviceCount);
    if ((*devicesMemory)==NULL){printf("DM Malloc Failed to Allocate Memory \n");exit(EXIT_FAILURE);}

    memoryVals = (*devicesMemory);
    for (dev = 0; dev < deviceCount; ++dev)
    {
        cudaSetDevice(dev);
        //cudaDeviceProp deviceProp;
        cudaMemGetInfo(&freeMemory,&dummy);
       // cudaGetDeviceProperties(&deviceProp, dev);

        //printf("  Total amount of global memory:                 %.0f MBytes (%llu bytes)\n",
        //       (float)deviceProp.totalGlobalMem/1048576.0f, (unsigned long long) deviceProp.totalGlobalMem);
        memoryVals[dev] = freeMemory;
        //printf("mem: %lu\n",memoryVals[dev]);
    }
    return deviceCount;
}

extern "C"
void cpyVolumeSliceToDevice(int startY,int endY,int sliceHeight,float *deviceVolume,float *volume,int volumeSize)
{
    int i,deltaY,planeSize;
    planeSize = (volumeSize)*(volumeSize);
    deltaY = endY - startY;
    printf("Slice Copying\n");
    
    if (deltaY != sliceHeight)
        cudaMemset(deviceVolume,0,planeSize*(sliceHeight+2)*sizeof(float));
    if ((deltaY+2)>volumeSize)
        deltaY-=2;
    if (startY!=0)
    {
        for(i=0;i<volumeSize;i++)
        {
            HANDLE_ERROR( cudaMemcpy(&deviceVolume[dp3d(0,0,i,volumeSize,sliceHeight+2)],&volume[p3dh(0,startY-1,i,volumeSize)],(size_t)volumeSize*(size_t)(deltaY+2)*sizeof(float),cudaMemcpyHostToDevice); )
        }
    }else
    {
        cudaMemset(deviceVolume,0,planeSize*(sliceHeight+2)*sizeof(float));
        for(i=0;i<volumeSize;i++)
        {
            HANDLE_ERROR( cudaMemcpy(&deviceVolume[dp3d(0,1,i,volumeSize,sliceHeight+2)],&volume[p3dh(0,startY,i,volumeSize)],(size_t)volumeSize*(size_t)(deltaY+1)*sizeof(float),cudaMemcpyHostToDevice); )
        }
    } 
}



extern "C"
void *malloc3DArrayOnDevice(size_t width,size_t height,size_t depth)
{
    cudaArray *d_volumeArray = 0;
    cudaChannelFormatDesc channelDesc = cudaCreateChannelDesc<VolumeType>();
    cudaExtent volumeSize = make_cudaExtent(width, height, depth);

    HANDLE_ERROR( cudaMalloc3DArray(&d_volumeArray, &channelDesc, volumeSize); )
    
    return (void*) d_volumeArray;
}

extern "C"
void cpyVolumeSliceToDeviceTexture(void * cudaArrayarg,int startY,int endY,int sliceHeight,float *volume,int volumeSizearg)
{
    cudaArray *d_volumeArray = (cudaArray*)cudaArrayarg;
    cudaExtent volumeSize = make_cudaExtent(volumeSizearg, sliceHeight+2, volumeSizearg);
    
    float *volumeSlice = (float*)calloc(volumeSize.width*volumeSize.depth*(size_t)(sliceHeight+2),sizeof(float));
    if (volumeSlice==NULL){printf("Error Allocating Memory, not enough, program will exit\n");exit(EXIT_FAILURE);}
    if (endY>=(volumeSizearg-1) )
        endY = volumeSizearg-2;
    size_t deltaY = endY - startY;
        
    size_t pageSize0 = volumeSize.width*(deltaY+2)*sizeof(float);
    size_t pageSize1 = volumeSize.width*(deltaY+1)*sizeof(float);
    
    int i;  
    if (startY!=0)
    {
        for(i=0;i<volumeSize.depth;i++)
        {
            memcpy(&volumeSlice[dp3d(0,0,i,volumeSize.width,sliceHeight+2)],&volume[p3dh(0,startY-1,i,volumeSize.width)],pageSize0);
        }
    }else
    {
        for(i=0;i<volumeSize.depth;i++)
        {
            memcpy(&volumeSlice[dp3d(0,1,i,volumeSize.width,sliceHeight+2)],&volume[p3dh(0,startY,i,volumeSize.width)],pageSize1);
        }
    } 

    cudaMemcpy3DParms copyParams = {0};
    copyParams.srcPtr   = make_cudaPitchedPtr(volumeSlice, volumeSize.width*sizeof(VolumeType), volumeSize.width, volumeSize.height);
    copyParams.dstArray = d_volumeArray;
    copyParams.extent   = volumeSize;
    copyParams.kind     = cudaMemcpyHostToDevice;
    HANDLE_ERROR( cudaMemcpy3D(&copyParams); )
    free(volumeSlice);
}