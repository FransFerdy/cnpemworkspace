#ifndef __SIS_UTIL__
#define __SIS_UTIL__
#include "sis_datatypes.h"

#ifdef __cplusplus
extern "C" {
#endif

float *** util_createDinamicVolume(int dim1,int dim2,int dim3);
void util_freeDinamicVolume(float *** array,int dim1,int dim2);
float ** util_createDinamicPlane(int dim1,int dim2);
void util_freeDinamicPlane(float ** array,int dim1);
void startTimer(sis_TimeMeasure* timer);
void finishTimer(sis_TimeMeasure* timer);
void* sis_stackPop(sis_StackHead* head);
void sis_stackPut(sis_StackHead* head,void *element);
int util_getCores();
void callPython(char ** params);
float timeDiff(sis_TimeMeasure* timer);
void secondsPrettyPrint(float seconds);

#ifdef __cplusplus
}
#endif

#endif 
