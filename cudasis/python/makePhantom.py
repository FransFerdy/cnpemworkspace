import h5py
import numpy
import sys
import matplotlib.pyplot as plt

N = int(sys.argv[1])

radius = 0.3

x = numpy.linspace(-1,1,N)

xx,yy,zz = numpy.meshgrid(x,x,x)

z_real = 4 * (xx**2 + yy**2 + zz**2 < radius**2).astype(numpy.double)

z_imag =  2 * (xx**2 + yy**2 + zz**2 < radius**2).astype(numpy.double)


dtype = numpy.float32
rec   = h5py.File("data.h5", "w")
dset  = rec.create_dataset('images', [N,N,N], dtype=dtype)
dset[:,:,:] = z_real[:,:,:]

dset2  = rec.create_dataset('images2', [N,N,N], dtype=dtype)
dset2[:,:,:] = z_imag[:,:,:]

rec.flush()
rec.close()





