import numpy as np
import matplotlib.pyplot as plt
import sys
import signal
import h5py
import matplotlib.animation as animation

fig = plt.figure()
print 'Loading File:', str(sys.argv[1])

f = h5py.File(str(sys.argv[1]), 'r')
dset = f['images']
a = dset[:,:,:]
f.close()
count = 0
print a.shape
'''
for plane in a:
    plane.shape
    mn = plane.min()
    mx = plane.max()
    
    if mn==mx:    
        mn = 0
        mx = a[count,0,0]
        a[count,0,0] = 0
    print mn,mx
    plane = plane - mn
    plane = plane/mx
    a[count,:,:] = plane
    count+=1
'''
halfY = a.shape[1]/2
halfX = np.shape(a)[2]/2

z = 0
maxZ = a.shape[0]
inc = 1

im = plt.imshow(a[z,:,:], extent=[-halfX, halfX, -halfY, halfY], animated=True)

def updatefig(*args):
    global z, maxZ, inc
    z += inc
    if z == maxZ-1 or z == 0:
        inc *= -1
    im.set_array(a[z,:,:])
    return im,

ani = animation.FuncAnimation(fig, updatefig, interval=500, blit=True)
plt.colorbar()
plt.show()

sys.exit(0)