import numpy as np
from mpl_toolkits.mplot3d import Axes3D
import matplotlib.pyplot as plt
from matplotlib import cm
from matplotlib.ticker import LinearLocator, FormatStrFormatter
import math
import datetime


def calculateLineSum(posCone,posDetector,volume,volPos,volSize):
    tVec = posDetector - posCone
    volPos = (volPos-volSize/2).astype(int)
    rayStep=0.01
    t=0
    lastDetectPoint = np.zeros( (3) ).astype(int)
    retVal=0
    while(t<1):
        point = np.zeros( (3) ).astype(int)
        point[0] = round(posCone[0] + t*tVec[0])
        point[1] = round(posCone[1] + t*tVec[1]) 
        point[2] = round(posCone[2] + t*tVec[2]) 
        t+=rayStep
        internalPoint = point-volPos

        

        if (internalPoint[0]<0 or internalPoint[1]<0 or internalPoint[2]<0 or internalPoint[0]>=volSize or internalPoint[1]>=volSize or internalPoint[2]>=volSize):
            continue
        if (lastDetectPoint[0]==point[0] and lastDetectPoint[1]==point[1] and lastDetectPoint[2]==point[2] ):
            continue
        lastDetectPoint = point

        retVal+= volume[internalPoint[0],internalPoint[1],internalPoint[2]]
    return retVal   
        



volSize = 32
volMiddle = volSize/2
volume = np.zeros((volSize,volSize,volSize))
sphereR = volSize*0.8/2
smallSphereR=volSize*0.2
volPos = np.zeros((3))
volPos[1] = 4
volPos[2] = -60

detectorW=256
detectorH=256
detectorMiddleW = detectorW/2
detectorMiddleH = detectorH/2
detector = np.zeros((detectorW,detectorH))
detectorZ = 0

coneBeamDistance=120
coneA=1
coneB=1
posCone = np.zeros(3)
posCone[2] = -coneBeamDistance

for x in range(0,volSize):
    for y in range(0,volSize):
        for z in range(0,volSize):
            wx = x-volMiddle
            wy = y-volMiddle
            wz = z-volMiddle
            distance = np.sqrt(wx**2+wy**2+wz**2)
            if (distance < sphereR and distance>smallSphereR and wz > -volMiddle/4  and wz< volMiddle/4):
                volume[x,y,z] = 1


start = datetime.datetime.now()

for x in range(0,detectorW):
    print x 
    for y in range (0,detectorH):
        wx = x-detectorMiddleW
        wy = y-detectorMiddleH
        wz = np.sqrt( (wx**2)/(coneA**2) + (wy**2)/(coneB**2) ) - coneBeamDistance
        
        if (wz<=detectorZ):
            detectPoint = np.zeros((3))
            detectPoint[0] = wx
            detectPoint[1] = wy
            detectPoint[2] = detectorZ
            detector[x,y] = calculateLineSum(posCone,detectPoint,volume,volPos,volSize)
stop = datetime.datetime.now()
elapsed = stop - start
print elapsed            
'''
fig = plt.figure()
ax = fig.gca(projection='3d')
ax.voxels(volume)
plt.show()
'''

plt.imshow(detector)
plt.colorbar()
plt.show()
'''
volSlice = 16
plt.imshow(volume[:,:,volSlice])
plt.colorbar()
plt.show()
plt.imshow(volume[:,volSlice,:])
plt.colorbar()
plt.show()
plt.imshow(volume[volSlice,:,:])
plt.colorbar()
plt.show()

fig = plt.figure()
ax = fig.gca(projection='3d')
ax.voxels(volume)

plt.show()
#fig = plt.figure()
'''
#ax = fig.gca(projection='3d')

#surf = ax.plot_surface(volume[:,0,0], volume[0,:,0], volume[0,0,:], cmap=cm.coolwarm,
 #                      linewidth=0, antialiased=False)

#fig.colorbar(surf, shrink=0.5, aspect=5)



'''
x = np.linspace(-1,1,512)
xx,yy = np.meshgrid(x,x)
z = np.sqrt(xx**2 + yy**2)

theta = math.pi/5
gama = math.pi/5
print np.cos(theta)
print np.sin(theta)

nxx = (xx*np.cos(theta) - yy*np.sin(theta))
nyy = (xx*np.sin(theta) +yy*np.cos(theta))*np.cos(gama) - z*np.sin(gama)
nz = np.sqrt(nxx**2 + nyy**2) - (xx*np.sin(theta)+yy*np.cos(theta))*np.sin(gama) + z*np.cos(theta)

fig = plt.figure()
ax = fig.gca(projection='3d')

surf = ax.plot_surface(nxx, nyy, nz, cmap=cm.coolwarm,
                       linewidth=0, antialiased=False)

# Customize the z axis.
#ax.set_zlim(-1.01, 1.01)
#ax.zaxis.set_major_locator(LinearLocator(10))
#ax.zaxis.set_major_formatter(FormatStrFormatter('%.02f'))

# Add a color bar which maps values to colors.
fig.colorbar(surf, shrink=0.5, aspect=5)

plt.show()
'''

