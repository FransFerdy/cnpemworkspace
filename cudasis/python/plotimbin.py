import numpy as np
import matplotlib.pyplot as plt
import sys
import signal
import h5py

if (len(sys.argv)<4):
    print "Invalid Number of Arguments, call python plotim.py filename.txt width height"
    sys.exit(1); 

print 'Loading File:', str(sys.argv[1])

a = np.fromfile(sys.argv[1], dtype=np.float32)

print a.shape
a = a.reshape(int(sys.argv[2]),int(sys.argv[3]))
print a.shape
halfY = a.shape[0]/2
halfX = a.shape[1]/2
#print halfY

plt.imshow(a, extent=[-halfX, halfX, -halfY, halfY])
plt.colorbar()
plt.show()
sys.exit(0)

