import numpy as np
from mpl_toolkits.mplot3d import Axes3D
import matplotlib.pyplot as plt
from matplotlib import cm
from matplotlib.ticker import LinearLocator, FormatStrFormatter
import math
import Queue
import threading
import thread
import time
import datetime

def calculateLineSum(posCone,posDetector,volume,volPos,volSize):
    tVec = posDetector - posCone
    volPos = (volPos-volSize/2).astype(int)
    rayStep=0.001
    t=0
    lastDetectPoint = np.zeros( (3) ).astype(int)
    retVal=0
    while(t<1):
        point = np.zeros( (3) ).astype(int)
        point[0] = round(posCone[0] + t*tVec[0] ) 
        point[1] = round(posCone[1] + t*tVec[1] )
        point[2] = round(posCone[2] + t*tVec[2] )
        t+=rayStep
        internalPoint = point-volPos
        if (internalPoint[0]<0 or internalPoint[1]<0 or internalPoint[2]<0 or internalPoint[0]>=volSize or internalPoint[1]>=volSize or internalPoint[2]>=volSize):
            continue

        if (lastDetectPoint[0]==point[0] and lastDetectPoint[1]==point[1] and lastDetectPoint[2]==point[2] ):
            continue
        lastDetectPoint = point

        retVal+= volume[internalPoint[0],internalPoint[1],internalPoint[2]]
    return retVal   
        
def makeDonutizedVolume(volSize):
    volMiddle = int(volSize/2)
    volume = np.zeros((volSize,volSize,volSize))
    sphereR = volSize*0.8/2
    smallSphereR=volSize*0.2
    for x in range(0,volSize):
        for y in range(0,volSize):
            for z in range(0,volSize):
                wx = x-volMiddle
                wy = y-volMiddle
                wz = z-volMiddle
                distance = np.sqrt(wx**2+wy**2+wz**2)
                if (distance < sphereR and distance>smallSphereR and wz > -volMiddle/4  and wz< volMiddle/4):
                    volume[x,y,z] = 1
    return volMiddle, volume

class MyVolume:
    size=0
    middle=0
    data=0
    pos=0

    def __init__(self,sizearg,posarg,volfunction):
        self.size = sizearg
        self.pos = posarg
        self.middle, self.data = volfunction(self.size)

class MyDetector:
    width=0
    height=0
    middleW = 0
    middleH = 0
    plane = 0
    pos = 0

    def __init__(self,widtharg,heightarg,posarg):
        self.width = widtharg
        self.height = heightarg
        self.middleW = int(self.width/2)
        self.middleH = int(self.height/2)
        self.plane = np.zeros((self.width,self.height))
        self.pos = posarg

class MyCone:
    distance=0
    a=0
    b=0
    pos=0
    def __init__(self,distancearg,aarg,barg,posarg):
        self.distance = distancearg
        self.a = aarg
        self.b = barg
        self.pos = posarg

class lineJobPoint:
    detectorX=0
    detectorY=0
    detector =0
    endPoint = 0
    volume =0
    cone = 0
    def __init__(self,detectorXarg,detectorYarg,detectorarg,volumearg,conearg,endPointarg):
        self.detectorX=detectorXarg
        self.detectorY=detectorYarg
        self.detector =detectorarg
        self.endPoint = endPointarg
        self.volume =volumearg
        self.cone = conearg

def lineJob(myid,lineProcessQueue,queueMutex,resultMutex,projectControl):
    print 'Thread ', myid, 'online'
    while(projectControl["work"]==False):
        time.sleep(1)
    
    while(projectControl["die"]==False):
        myJob=0
        queueMutex.acquire()
        if (not lineProcessQueue.empty()):
            myJob = lineProcessQueue.get()
        queueMutex.release()
        if (myJob!=0):
            myJob.detector.plane[myJob.detectorX,myJob.detectorY] = calculateLineSum(myJob.cone.pos,myJob.endPoint,myJob.volume.data,myJob.volume.pos,myJob.volume.size)
            resultMutex.acquire()
            projectControl["r"]+=1
            resultMutex.release()
        

#@maxthreads: #ofCores * #threadsperCore * 16
def planeProjection(maxThreads,cone,detector,volume):
    detectorPoints = detector.width*detector.height;
    lineProcessQueue = Queue.Queue(0)
    queueMutex = threading.Lock()
    resultMutex = threading.Lock()
    projectControl = {"die":False,"r":0,"work":False}
    start = datetime.datetime.now()

    for i in range(0,maxThreads):
        try:
            thread.start_new_thread( lineJob, (i,lineProcessQueue, queueMutex, resultMutex,projectControl) )
        except:
            print "Error: unable to start thread"
    print 'creating jobs'        

    for x in range(0,detector.width):
        #print 'progress:', float(projectControl["results"])/detectorPoints*100
        for y in range (0,detector.height):
            wx = x-detector.middleW
            wy = y-detector.middleH
            wz = np.sqrt( (wx**2)/(cone.a**2) + (wy**2)/(cone.b**2) ) - cone.distance
            
            if (wz<=detector.pos[2]):
                detectPoint = np.zeros((3))
                detectPoint[0] = wx
                detectPoint[1] = wy
                detectPoint[2] = detector.pos[2]
                queueMutex.acquire()
                lineProcessQueue.put(lineJobPoint(x,y,detector,volume,cone,detectPoint))
                queueMutex.release()
            else:
                resultMutex.acquire()
                projectControl["r"]+=1
                resultMutex.release()
    projectControl["work"]=True
    while(projectControl["r"]<detectorPoints):
        print 'progress:', int(float(projectControl["r"])/detectorPoints*100),'%'
        time.sleep(5)
    projectControl["die"]=True
    stop = datetime.datetime.now()
    elapsed = stop - start
    print elapsed 
    plt.imshow(detector.plane)
    plt.colorbar()
    plt.show()    


def main():
    volSize = 32
    volPos = np.zeros((3))
    volPos[1] = 0
    volPos[2] = -100
    wVol = MyVolume(volSize,volPos,makeDonutizedVolume)

    detectorW=256
    detectorH=256
    detectorPos = np.zeros((3))
    wDetector = MyDetector(detectorW,detectorH,detectorPos)
        

    coneBeamDistance=120
    coneA=1
    coneB=1
    posCone = np.zeros(3)
    posCone[2] = -coneBeamDistance
    wCone = MyCone(coneBeamDistance,coneA,coneB,posCone)

    maxThreads = 2

    planeProjection(maxThreads,wCone,wDetector,wVol)

main()



            
'''
fig = plt.figure()
ax = fig.gca(projection='3d')
ax.voxels(volume)
plt.show()


plt.imshow(detector)
plt.colorbar()
plt.show()

volSlice = 16
plt.imshow(volume[:,:,volSlice])
plt.colorbar()
plt.show()
plt.imshow(volume[:,volSlice,:])
plt.colorbar()
plt.show()
plt.imshow(volume[volSlice,:,:])
plt.colorbar()
plt.show()

fig = plt.figure()
ax = fig.gca(projection='3d')
ax.voxels(volume)

plt.show()
#fig = plt.figure()
'''
#ax = fig.gca(projection='3d')

#surf = ax.plot_surface(volume[:,0,0], volume[0,:,0], volume[0,0,:], cmap=cm.coolwarm,
 #                      linewidth=0, antialiased=False)

#fig.colorbar(surf, shrink=0.5, aspect=5)



'''
x = np.linspace(-1,1,512)
xx,yy = np.meshgrid(x,x)
z = np.sqrt(xx**2 + yy**2)

theta = math.pi/5
gama = math.pi/5
print np.cos(theta)
print np.sin(theta)

nxx = (xx*np.cos(theta) - yy*np.sin(theta))
nyy = (xx*np.sin(theta) +yy*np.cos(theta))*np.cos(gama) - z*np.sin(gama)
nz = np.sqrt(nxx**2 + nyy**2) - (xx*np.sin(theta)+yy*np.cos(theta))*np.sin(gama) + z*np.cos(theta)

fig = plt.figure()
ax = fig.gca(projection='3d')

surf = ax.plot_surface(nxx, nyy, nz, cmap=cm.coolwarm,
                       linewidth=0, antialiased=False)

# Customize the z axis.
#ax.set_zlim(-1.01, 1.01)
#ax.zaxis.set_major_locator(LinearLocator(10))
#ax.zaxis.set_major_formatter(FormatStrFormatter('%.02f'))

# Add a color bar which maps values to colors.
fig.colorbar(surf, shrink=0.5, aspect=5)

plt.show()
'''

