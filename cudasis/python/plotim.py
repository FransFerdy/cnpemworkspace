import numpy as np
import matplotlib.pyplot as plt
import sys
import signal

if (len(sys.argv)<2):
    print "Invalid Number of Arguments, call python plotim.py filename.txt"
    sys.exit(1); 

print 'Loading File:', str(sys.argv[1])


a = np.fromfile(str(sys.argv[1]),np.float)#np.loadtxt(str(sys.argv[1]))
a = a.reshape(int(sys.argv[2]),int(sys.argv[3]))
print a.shape
print "Plotting"

plt.imshow(a)
plt.colorbar()
plt.show()
sys.exit(0)