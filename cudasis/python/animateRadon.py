import numpy as np
import matplotlib.pyplot as plt
import sys
import signal
import h5py
import matplotlib.animation as animation

fig = plt.figure()
print 'Loading File:', str(sys.argv[1])

f = h5py.File(str(sys.argv[1]), 'r')
dset = f['images']
a = dset[:,:,:]
f.close()
count = 0
print a.shape

for plane in a:
    plane.shape
    plane = plane - plane.min()
    plane = plane/plane.max()
    a[count,:,:] = plane
    count+=1

halfY = a.shape[1]/2
halfX = np.shape(a)[2]/2

z = 0
maxZ = a.shape[0]
inc = 1

im, = plt.plot(a[z,halfY,:], animated=True)

def updatefig(*args):
    global z, maxZ, inc, halfY
    z += inc
    if z == maxZ-1 or z == 0:
        inc *= -1
    im.set_ydata(a[z,halfY,:])
    return im,

ani = animation.FuncAnimation(fig, updatefig, interval=50, blit=True)
plt.show()

sys.exit(0)