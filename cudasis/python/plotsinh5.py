import numpy as np
import matplotlib.pyplot as plt
import sys
import signal
import h5py

if (len(sys.argv)<2):
    print "Invalid Number of Arguments, call python plotim.py filename.txt"
    sys.exit(1); 

print 'Loading File:', str(sys.argv[1])

f = h5py.File(str(sys.argv[1]), 'r')
dset = f['images']
a = dset[:,:]
f.close()


print "Plotting"

plt.imshow(a)
plt.colorbar()
plt.show()
sys.exit(0)


