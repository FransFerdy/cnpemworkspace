import numpy as np
import matplotlib.pyplot as plt
import sys
import signal
import h5py

if (len(sys.argv)<2):
    print "Invalid Number of Arguments, call python plotim.py filename.txt"
    sys.exit(1); 

print 'Loading File:', str(sys.argv[1])

f = h5py.File(str(sys.argv[1]), 'r')
dset = f['images']
print dset.shape
middle = dset.shape[2]/2
a = dset[:,:,:]
f.close()

print "Plotting"

plt.imshow(a[:,middle,:])
plt.colorbar()
plt.show()
sys.exit(0)


'''
fig = plt.figure()
ax = fig.gca(projection='3d')
ax.voxels(a)
plt.show()
'''

'''
import numpy as np
from mpl_toolkits.mplot3d import Axes3D
import matplotlib.pyplot as plt
from matplotlib import cm
from matplotlib.ticker import LinearLocator, FormatStrFormatter


fig = plt.figure()
ax = fig.gca(projection='3d')
ax.voxels(volume)
plt.show()

'''
