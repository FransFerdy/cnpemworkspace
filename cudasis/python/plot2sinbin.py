import numpy as np
import matplotlib.pyplot as plt
import sys
import signal
import h5py

if (len(sys.argv)<5):
    print "Invalid Number of Arguments, call python plotim.py filename.txt width height"
    sys.exit(1); 

print 'Loading File:', str(sys.argv[1])
print 'Loading File:', str(sys.argv[2])

a = np.fromfile(sys.argv[1], dtype=np.float32)
b = np.fromfile(sys.argv[2], dtype=np.float32)

print a.shape
a = a.reshape(int(sys.argv[3]),int(sys.argv[4]))
b = b.reshape(int(sys.argv[3]),int(sys.argv[4]))

print a.shape
print b.shape
halfY = a.shape[0]/2
halfX = a.shape[1]/2

c = a[:,halfY,]
d = b[:,halfY]
#print halfY

plt.plot(c)
plt.plot(d)
plt.show()
sys.exit(0)

