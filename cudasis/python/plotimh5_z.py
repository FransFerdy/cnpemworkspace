import numpy as np
import matplotlib.pyplot as plt
import sys
import signal
import h5py

if (len(sys.argv)<2):
    print "Invalid Number of Arguments, call python plotim.py filename.txt"
    sys.exit(1); 

print 'Loading File:', str(sys.argv[1])

f = h5py.File(str(sys.argv[1]), 'r')
dset = f['images']
a = dset[int(sys.argv[2]),:,:]
f.close()

print "Plotting"

halfY = np.shape(a)[0]/2
halfX = np.shape(a)[1]/2
#print halfY

plt.imshow(a, extent=[-halfX, halfX, -halfY, halfY])
plt.colorbar()
plt.show()
sys.exit(0)

