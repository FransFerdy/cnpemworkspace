import math
import numpy as np
import matplotlib.pyplot as plt
import sys
import signal

if (len(sys.argv)<2):
    print "Invalid Number of Arguments, call python plotim.py filename.txt"
    sys.exit(1); 

angle = eval(sys.argv[1])
distance = eval(sys.argv[2])
largura = eval(sys.argv[3])
distanceFromCenter = largura/2
cubex = eval(sys.argv[4])
detlargura = eval(sys.argv[5])
detalture = eval(sys.argv[6])
catv = math.tan(angle/2)*distance

print math.tan(math.pi/4)

plt.plot([0,distance],[0,catv])
plt.plot([0,distance],[0,0])
plt.plot([0,distance],[0,-catv])
plt.plot([cubex-distanceFromCenter,cubex+distanceFromCenter],[0-distanceFromCenter,0-distanceFromCenter])
plt.plot([cubex-distanceFromCenter,cubex+distanceFromCenter],[0+distanceFromCenter,0+distanceFromCenter])
plt.plot([cubex-distanceFromCenter,cubex-distanceFromCenter],[0+distanceFromCenter,0-distanceFromCenter])
plt.plot([cubex+distanceFromCenter,cubex+distanceFromCenter],[0+distanceFromCenter,0-distanceFromCenter])
plt.axes().set_aspect('equal', 'datalim')
plt.plot([distance,distance],[-detlargura/2,+detlargura/2])
plt.show()
'''

a = np.fromfile(str(sys.argv[1]),np.float)#np.loadtxt(str(sys.argv[1]))
a = a.reshape(int(sys.argv[2]),int(sys.argv[3]))
print a.shape
print "Plotting"

plt.imshow(a)
plt.colorbar()
plt.show()
sys.exit(0)
'''