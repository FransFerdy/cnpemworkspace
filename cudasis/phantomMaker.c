

#include "sis_datatypes.h"
#include "sis_util.h"
#include "sis_forms.h"
#include "cJSON.h"

#include <stdlib.h>
#include <stdbool.h>
#include <stdio.h>
#include <string.h>
#include <math.h>
#include <unistd.h>
#include <errno.h>
#include <pthread.h>
#include <getopt.h>
//#include <cuda.h>
#include "cudaFun.h"
#include "material.h"

#include "sis_cpu.h"

#define Cvacuum 299792458
#define HConstant 6.626e-34
#define EV_TO_JOULE 1.602176565e-19
#ifndef M_PI
#    define M_PI 3.14159265358979323846
#endif
#define CPUGPU_RATIO 23

char outputFolder[1000]="./"; 

/*
Get 3D cube coordinate in 1D array
@x, x coordinate
@y, y coordinate
@z, z coordinate
*/
static size_t p3d(size_t x,size_t y, size_t z, size_t size)
{
    return (x+y*size+(z*size*size));// ((x*d1)+y*d2)+z;
}

/*
Get 3D coordinate in 1D array
@x, x coordinate
@y, y coordinate
@z, z coordinate
@height, dimension 1
@depth dimension 2
*/
static size_t dp3d(size_t x,size_t y, size_t z, size_t width,size_t height)
{
    return (x+y*width+(z*width*height));// ((x*d1)+y*d2)+z;
}
/*
Get 2D plane coordinate in 1D array
@x, x coordinate
@y, y coordinate
*/
static size_t p2d(size_t x,size_t y, size_t width)
{
    return (x+y*width);// ((x*d1)+y*d2)+z;
}


void print_usage() {
    printf("Usage: phantomMkaer -t Threads -o filename.h5 -E \n");
    printf("-O                      Output folder, all files will be saved here\n");
    printf("-R                      Random Seed (Default: 7)\n");
    printf("-E                      Elements in Phantom ( input sample(list): shape,scale,yawrotation,x,y,z,element1,element2,...:shape,scale...) The first element in a phantom is the main material Possible shapes: cube,torus,sphere,disc,cylinder, scale(min 0),x,y,z min =-1 max =1\n");
    printf("-e                      Energy kev (default 10 kev) \n");
    printf("-o                      output file name \n");
    printf("-t                      maximun amount of threads \n");
    printf("-D                      Phantom Volume Dimension Size \n");
    printf("-j                      Json Object with Parameters\n");
    printf("-h                      Show this help print\n");
}

cJSON * loadJsonFile(char * filename)
{
    ;FILE * pFile;
    long lSize;
    char * buffer;
    size_t result;
    cJSON * ret;
    const char * jsonErr = NULL;
    pFile = fopen ( filename , "rb" );
        if (pFile==NULL) {fputs ("Opening Json File error",stderr); exit (1);}
    fseek (pFile , 0 , SEEK_END);
    lSize = ftell (pFile);
    rewind (pFile);
    buffer = (char*) malloc (sizeof(char)*lSize);
        if (buffer == NULL) {fputs ("Memory Allocation error",stderr); exit (2);}
    result = fread (buffer,1,lSize,pFile);
    if (result != lSize) {fputs ("File Reading error",stderr); exit (3);}
    fclose (pFile);  
    ret = cJSON_Parse(buffer);
    jsonErr =cJSON_GetErrorPtr();
    if (jsonErr!=NULL)
        printf("Error Parsing JSON At: %s\n",jsonErr);  
    free (buffer);
    return ret;
}

int main(int argc,  char** argv)
{
    int ints[V_INTVECTOR_SIZE];
    float floats[V_DOUBLEVECTOR_SIZE];
    char posInput[1000]; 
    int maxThreads;
    char* filename = NULL;
    char c;
    cJSON *jsonin =NULL; 
    char *jsonPrint;
    FILE * pFile;
    float energy;
    char outFileName2[1000];

    int phantomAmount=0;
    int elemAmount[20];
    char elements[20][100][12];
    float *elementsIndexes[100];
    size_t phantomVolumeSize=128;

    long int randomSeed = 7;
    
    sis_TimeMeasure deltaT;

    char outFileName[1000] = "outPhantom.h5";
    
    maxThreads = util_getCores();
    if (maxThreads<1)
        maxThreads=1;

    pFile = fopen ( "asciiart2.txt" , "r" );
    if (pFile!=NULL)
    {
        char c='a';
        while (c != EOF)
        {
            c = fgetc (pFile);
            if (c!=EOF)
                printf("%c",c);
        };
        fclose(pFile);
        printf("\n");
    }


    srand(randomSeed);
    while (optind < argc) 
    {
        if ((c = getopt(argc, argv, "t:D:R:o:hj:E:e:O:")) != -1) 
        {
            // Option argument
            switch (c)
            {
                case 'O':
                    strcpy(outputFolder,optarg);
                    strcat(outputFolder,"/");
                break;
                case 't': maxThreads = (int)atoi(optarg);
                break;
                case 'D': phantomVolumeSize = (int)atoi(optarg);
                break;
                case 'R': randomSeed = atoi(optarg); srand(randomSeed);
                break;
                case 'o': strcpy(outFileName,optarg);
                break;
                case 'h': print_usage(); exit(0);
                break;
                case 'j': 
                    jsonin = loadJsonFile(optarg);
                break;
                case 'e': 
                    energy = (float)atof(optarg);
                break;
                case 'E' :
                {
                    ;             
                    int countv = 0;
                    int object=0;
                    int pos=0,lastPos=0;
                    char* initialPoint=optarg;
                    phantomAmount=1;
                    for(pos=0; true;pos++)
                    {
                        if (optarg[pos]==',' || optarg[pos]=='\0' ||  optarg[pos]==':' )
                        {
                            strcpy(posInput,initialPoint);
                            posInput[pos-lastPos] = '\0';
                            lastPos=pos+1;
                            strcpy(elements[object][countv],posInput);
                            countv++;
                            initialPoint = &optarg[lastPos]; 
                            if (optarg[pos]==':')
                            {
                                phantomAmount++;
                                elemAmount[object] = countv;
                                object++;
                                countv=0;
                            }
                        }
                        if ( optarg[pos]=='\0')
                            break;
                    }    
                    elemAmount[object] = countv;
                break;
                }

            }
        }
    }

    if (jsonin!=NULL)
    {
        cJSON *param;
        jsonPrint = cJSON_Print(jsonin);
        if (jsonPrint!=NULL)
            printf("Json %s\n",jsonPrint);

        param = cJSON_GetObjectItemCaseSensitive(jsonin, "threads");
        if (cJSON_IsNumber(param))
            maxThreads = param->valueint;
        param = cJSON_GetObjectItemCaseSensitive(jsonin, "energy");
        if (cJSON_IsNumber(param))
            energy = param->valuedouble;
        param = cJSON_GetObjectItemCaseSensitive(jsonin, "randomSeed");
        if (cJSON_IsNumber(param))
        {
            randomSeed = param->valueint; 
            srand(randomSeed);
        }

        param = cJSON_GetObjectItemCaseSensitive(jsonin, "output_folder");
        if (cJSON_IsString(param) && param->valuestring!=NULL)
        {
            char *originalString = param->valuestring;
            strcpy(outputFolder,originalString);
            strcat(outputFolder,"/");
        }

        param = cJSON_GetObjectItemCaseSensitive(jsonin, "phantom_volume_size");
        if (cJSON_IsNumber(param))
            phantomVolumeSize = param->valueint;

        param = cJSON_GetObjectItemCaseSensitive(jsonin, "output_filename");
        if (cJSON_IsString(param) && param->valuestring!=NULL)
            strcpy(outFileName,param->valuestring);

        param = cJSON_GetObjectItemCaseSensitive(jsonin, "phantom_objects");
        if (cJSON_IsString(param) && param->valuestring!=NULL)
        {
            {
                ;
                int countv = 0;
                int object=0;
                int pos=0,lastPos=0;
                char* initialPoint=param->valuestring;
                char *originalString = param->valuestring;
                phantomAmount=1;
                for(pos=0; true;pos++)
                {
                    if (originalString[pos]==',' || originalString[pos]=='\0' ||  originalString[pos]==':' )
                    {
                        strcpy(posInput,initialPoint);
                        posInput[pos-lastPos] = '\0';
                        lastPos=pos+1;
                        strcpy(elements[object][countv],posInput);
                        countv++;
                        initialPoint = &originalString[lastPos]; 
                        if (originalString[pos]==':')
                        {
                            phantomAmount++;
                            elemAmount[object] = countv;
                            object++;
                            countv=0;
                        }
                    }
                    if ( originalString[pos]=='\0')
                        break;
                }    
                elemAmount[object] = countv;
            }
        }  
    }

    char fe[3] = "Fe";
    elementsIndexes[0] = getMaterialRefractiveIndexAtEnergy(fe,energy,true);
    floats[V_LAMBDA] = elementsIndexes[0][2] * 0.001;//multiply to have number in micrometers
    floats[V_WAVEK] = 2*M_PI/floats[V_LAMBDA];
    free(elementsIndexes[0]);

    sis_Volume* volume;
    sis_Detector* detector;
    
    if (filename==NULL)
    { 
        if (elemAmount[0]==0)
        {
            printf("You must specify the volume elements, with -E option, read -h for details.\n");
            exit(0);
        }
        
        //printf("Building Phantom %lu^3\n",phantomVolumeSize);
        startTimer(&deltaT);
        volume = createVolumeComplex(phantomVolumeSize,0,0,0);
        printf("PhantomAmount %i\n",phantomAmount);
        int realElemAmount=0;
        for (int k=0; k<phantomAmount;k++)
        {
            realElemAmount = elemAmount[k]-6;
            for (int i=6;i<elemAmount[k];i++)
            {
                elementsIndexes[i-6] = getMaterialRefractiveIndexAtEnergy(elements[k][i],energy,true);
            }
            if (strcmp(elements[k][0],"torus")==0)
                makeDonutsVolumeComplex(volume,0.9*atof(elements[k][1]), 0.2*atof(elements[k][1]),atof(elements[k][2]),atof(elements[k][3]),atof(elements[k][4]),atof(elements[k][5]),realElemAmount,elementsIndexes,maxThreads,randomSeed);
            if (strcmp(elements[k][0],"sphere")==0)
                makeSphereVolumeComplex(volume,0.9*atof(elements[k][1]),atof(elements[k][2]),atof(elements[k][3]),atof(elements[k][4]),atof(elements[k][5]),realElemAmount,elementsIndexes,maxThreads,randomSeed);
            if (strcmp(elements[k][0],"cube")==0)
                makeCubeVolumeComplex(volume,0.9*atof(elements[k][1]),atof(elements[k][2]),atof(elements[k][3]),atof(elements[k][4]),atof(elements[k][5]),realElemAmount,elementsIndexes,maxThreads,randomSeed);
            if (strcmp(elements[k][0],"disc")==0)
                makeDiscVolumeComplex(volume,0.9*atof(elements[k][1]), 0.4*atof(elements[k][1]),atof(elements[k][2]),atof(elements[k][3]),atof(elements[k][4]),atof(elements[k][5]),realElemAmount,elementsIndexes,maxThreads,randomSeed);
            if (strcmp(elements[k][0],"cylinder")==0)
                makeDiscVolumeComplex(volume,0.1*atof(elements[k][1]), 1*atof(elements[k][1]),(M_PI/2)+atof(elements[k][2]),atof(elements[k][3]),atof(elements[k][4]),atof(elements[k][5]),realElemAmount,elementsIndexes,maxThreads,randomSeed);
                    
            for(int i=0; i<realElemAmount;i++)
                free(elementsIndexes[i]);
        }

        finishTimer(&deltaT);

        printf("Finished Building Phantom\n");
        printf("Saving Phantom.h5\n");
        strcpy(outFileName2, outputFolder);
        strcat(outFileName2, outFileName);
        saveHDF5Volume(volume, outFileName2);
    }

    cJSON_Delete(jsonin);
}
