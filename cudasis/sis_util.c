#include "sis_util.h"
#include <stdlib.h> 
#include <string.h>
#include <math.h>
#include <stdio.h>
#include <errno.h>

#ifdef _WIN32
#define WIN32_LEAN_AND_MEAN
#include <windows.h>
#else
#include <unistd.h>
#endif

int util_getCores()
{
    long nprocs = -1;
    long nprocs_max = -1;
    #ifdef _WIN32
        #ifndef _SC_NPROCESSORS_ONLN
            SYSTEM_INFO info;
            GetSystemInfo(&info);
            #define sysconf(a) info.dwNumberOfProcessors
            #define _SC_NPROCESSORS_ONLN
        #endif
    #endif
    #ifdef _SC_NPROCESSORS_ONLN
        nprocs = sysconf(_SC_NPROCESSORS_ONLN);
        if (nprocs < 1)
        {
            fprintf(stderr, "Could not determine number of CPUs online:\n%s\n", 
            strerror (errno));
            return -1;
        }
        nprocs_max = sysconf(_SC_NPROCESSORS_CONF);
        if (nprocs_max < 1)
        {
            fprintf(stderr, "Could not determine number of CPUs configured:\n%s\n", 
            strerror (errno));
            return -1;
        }
        return nprocs;
    #else
        fprintf(stderr, "Could not determine number of CPUs");
        return -1;
    #endif 
}
 
 float *** util_createDinamicVolume(int dim1,int dim2,int dim3)
 {
    float *** array = (float ***)malloc(dim1*sizeof(float**));
    int i,j;
        for (i = 0; i< dim1; i++) 
        {
            array[i] = (float **) malloc(dim2*sizeof(float *));

            for (j = 0; j < dim2; j++) 
            {
              array[i][j] = (float *)malloc(dim3*sizeof(float));
              memset(array[i][j],0,dim3*sizeof(float));
            }

        }
    return array;
}
 
 void util_freeDinamicVolume(float *** array,int dim1,int dim2)
 {
    int i,j;
    for (i = 0; i< dim1; i++) 
    {
      for (j = 0; j < dim2; j++) 
      {
          free(array[i][j]);
      }
      free(array[i]);
    }
    free(array);
 }

float ** util_createDinamicPlane(int dim1,int dim2)
 {
    float ** array = (float **)malloc(dim1*sizeof(float*));
        int i;
        for (i = 0; i< dim1; i++) 
        {
              array[i] = (float *)malloc(dim2*sizeof(float));
              memset(array[i],0,dim2*sizeof(float));
        }
    return array;
 }
 
void util_freeDinamicPlane(float ** array,int dim1)
 {
    int i;
    for (i = 0; i< dim1; i++) 
    {
      free(array[i]);
    }
    free(array);
 }

void startTimer(sis_TimeMeasure* timer)
{
    timer->start = time(NULL);
}
void finishTimer(sis_TimeMeasure* timer)
{
    float seconds;
    timer->finish = time(NULL);
    seconds = timer->finish-timer->start;
    printf("\nDone! %d:%d:%d since start\n",((int)seconds/3600),((int)seconds/60)%60,((int)seconds)%60);
}
float timeDiff(sis_TimeMeasure* timer)
{
    float seconds;
    timer->finish = time(NULL);
    seconds = timer->finish-timer->start;
    return seconds;
}
void secondsPrettyPrint(float seconds)
{
    printf(" %d:%d:%d ",((int)seconds/3600),((int)seconds/60)%60,((int)seconds)%60);
}


void sis_stackPut(sis_StackHead* head,void *element)
{
    sis_StackPoint* aux;

    aux = (sis_StackPoint*) malloc (sizeof(sis_StackPoint));
    if (aux==NULL)
    {
        printf("Error Alocating Memory");
        exit(0);
    }
    aux->data = element;
    aux->next=NULL;

    if (head->last==NULL)
    {
        head->last=aux;
        head->first=aux;
    }
    else{
        head->last->next = aux;
        head->last = aux;
    }
    
    head->size++;
}

void* sis_stackPop(sis_StackHead* head)
{
    sis_StackPoint* aux;
    if (head->first==NULL)
        return NULL;
    sis_LineJobPoint* ret = head->first->data;
    aux= head->first;
    head->first = head->first->next;
    free(aux);
    if (head->first==NULL)
        head->last=NULL;
    head->size--;
    return ret;
}


void callPython(char ** params)
{
    pid_t pid = vfork();
    
    if (pid==0)
    {
        execvp("python",params);
    }
    else
    {
        if (pid<0)
            printf("Error Creating Python Process\n");    
    }
}