
#ifndef __SIS_DATATYPES__
#define __SIS_DATATYPES__

#include <time.h>
#include <pthread.h>

typedef struct sis_Volume
{
    int size;
    int middle;
    int pos[3];
    float *data;
    float *dataImaginary;
} sis_Volume;

typedef struct sis_ConeBeam
{
    int distance;
    float a;
    float b;
    float c; 
    int pos[3];
} sis_ConeBeam;

typedef struct sis_Detector
{
    int width;
    int height;
    int depth;
    int middleW;
    int middleH;
    float pitch;//pitch
    float yaw;//yaw
    float roll;
    int rollMode;
    int pos[3];
    float *data;
    float *dataImaginary;
} sis_Detector;

typedef struct sis_LineVars
{
    int volPos[3], point[3];
    int detectorX,detectorY;
    float retVal,rayStep,rayPrecision,t,tVec[3];
} sis_LineVars;

typedef struct ParticlePoint
{
    int x, y, z;
} ParticlePoint;
     
typedef struct sis_LineJobPoint
{
    int detectorX;
    int detectorY;
    int endPoint[3];
    sis_Detector* detector;
    sis_ConeBeam* cone;
    sis_Volume* volume;
} sis_LineJobPoint; 

typedef struct sis_StackHead
{
    struct sis_StackPoint * first;
    struct sis_StackPoint * last;
    int size;
} sis_StackHead;

typedef struct sis_StackPoint
{
    struct sis_StackPoint * next;
    void * data;
} sis_StackPoint;

typedef struct sis_TimeMeasure
{
    time_t start;
    time_t finish;
} sis_TimeMeasure;

typedef struct sis_ProjectionControl
{ 
    int state;
    int results;
    float rayStep;
    float objectRotation;
    int detectorPoints;
    int planeIndex;
    int pointMode;
    sis_Detector* detector;
    sis_Volume* volume;
    sis_ConeBeam* cone;
}sis_ProjectionControl;

typedef struct sis_JobConfig
{
    pthread_mutex_t* resultMutex;
    sis_ProjectionControl* projectControl;
    int *threadCount;
    int threadIds;
    int *ints;
    float *floats;
    float *validPoints;
    float *detector;
    float *volume;
    int planeIndex;
    float thetaSin;
    float thetaCos;
} sis_JobConfig;

typedef struct sis_SharedVars
{
    void* fresnelKernel;
} sis_SharedVars;

typedef struct sis_DeviceConfig
{
    pthread_mutex_t* resultMutex;
    int rotationEnd;
    size_t myMemory;
    int myId;
    int cpuJobs;
    int gpuDevices;
    float *validPoints;
    sis_Detector *detector;
    int *threadCount;
    int *stageCounter;
    int *stage;
    sis_SharedVars * sharedVars;
    int rotations;
    float startAngle;
    int myJobId;
    sis_Volume* volume;
    float *f10Vec;
    int * ints;
    float *floats;
    float *detectorPixelValues;
    float allowedMemoryConsumption;

} sis_DeviceConfig;

typedef struct DonutsJobConfig
{
    pthread_mutex_t* resultMutex;
    int *threadCount;
    int threadIds;

    sis_Volume* volume;
    int elemAmount;
    float **elements;
    
    int padding;
    size_t linesPerThread;
    long int seed;
    float angle;
    float addx;
    float addz;
    float addy;
    int (*insideform)(float a,float b,float c,float d, float e, float f);
    float gf1,gf2,gf3;
} DonutsJobConfig;




#endif
