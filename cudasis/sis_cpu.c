

#include "sis_datatypes.h"
#include "sis_util.h"
#include "sis_forms.h"
#include "cJSON.h"

#include <stdlib.h>
#include <stdbool.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <errno.h>
#include <math.h>
#include <pthread.h>
#include "cudaFun.h"

#define PI 3.14159265359
#define PT_VARIATION 0.0000000001
#define CPUGPU_RATIO 23

#define P0X point[p2d(0,0,3)]
#define P0Y point[p2d(1,0,3)]
#define P0Z point[p2d(2,0,3)]

#define P1X point[p2d(0,1,3)]
#define P1Y point[p2d(1,1,3)]
#define P1Z point[p2d(2,1,3)]
        
#define P2X point[p2d(0,1,3)]
#define P2Y point[p2d(1,0,3)]
#define P2Z point[p2d(2,0,3)]

#define P3X point[p2d(0,0,3)]
#define P3Y point[p2d(1,1,3)]
#define P3Z point[p2d(2,0,3)]

#define P4X point[p2d(0,1,3)]
#define P4Y point[p2d(1,1,3)]
#define P4Z point[p2d(2,0,3)]

#define P5X point[p2d(0,0,3)]
#define P5Y point[p2d(1,0,3)]
#define P5Z point[p2d(2,1,3)]

#define P6X point[p2d(0,1,3)]
#define P6Y point[p2d(1,0,3)]
#define P6Z point[p2d(2,1,3)]

#define P7X point[p2d(0,0,3)]
#define P7Y point[p2d(1,1,3)]
#define P7Z point[p2d(2,1,3)]



int ints[V_INTVECTOR_SIZE];
float floats[V_DOUBLEVECTOR_SIZE];

/*
Get 3D cube coordinate in 1D array
@x, x coordinate
@y, y coordinate
@z, z coordinate
*/
static size_t p3d(size_t x,size_t y, size_t z, size_t size)
{
    return (x+y*size+(z*size*size));// ((x*d1)+y*d2)+z;
}

/*
Get 3D coordinate in 1D array
@x, x coordinate
@y, y coordinate
@z, z coordinate
@height, dimension 1
@depth dimension 2
*/
static size_t dp3d(size_t x,size_t y, size_t z, size_t width,size_t height)
{
    return (x+y*width+(z*width*height));// ((x*d1)+y*d2)+z;
}
/*
Get 2D plane coordinate in 1D array
@x, x coordinate
@y, y coordinate
*/
static size_t p2d(size_t x,size_t y, size_t width)
{
    return (x+y*width);// ((x*d1)+y*d2)+z;
}

void * calculateLineSumTriCpu(void *arg)
{
    sis_JobConfig* jobConfig = (sis_JobConfig*)arg;
    int myId;
    int myStep=0;
    int myPosX=0;
    int myPosY=0;
    int *ints = jobConfig->ints;
    float * floats = jobConfig->floats;
    int threadOffset = ints[V_MAXTHREADS];
    int maxPoints = ints[V_VALIDDETECTORPOINTS]; 
    int volumeSize = ints[V_VOLUMESIZE];
    int detectorWidth = ints[V_DETECTORWIDTH];
    int detectorHeight = ints[V_DETECTORHEIGHT];
    float raySamplingDouble = floats[V_RAYSAMPLINGDOUBLE];
    int raySamplingInt = ints[V_RAYSAMPLING];
    float volumex = floats[V_VOLUMEX];
    float tVec[3];
    float objThetaSin = jobConfig->thetaSin;
    float objThetaCos = jobConfig->thetaCos;
    float conePosZMinusVolumeZ = floats[V_CONEPOSZ_MINUS_VOLUMEZ];
    float volumeYMinusVolumeMiddle = floats[V_VOLUMEY_MINUS_VOLUMEMIDDLE];
    float t=0, tempPoint[3],tempPoint2[3];
    float m;
    size_t point[AMOUNT_POINTS_INFO];
    float *volumeData = jobConfig->volume;
    float *detector = jobConfig->detector;
    float *validPoints = jobConfig->validPoints;
    int planeIndex = jobConfig->planeIndex;
    int conePosZ = (-floats[V_CONEDISTANCE]);
    float rayStep = 1.0 / raySamplingDouble;

    pthread_mutex_lock(jobConfig->resultMutex);
    myId =jobConfig->threadIds;
    jobConfig->threadIds++;
    pthread_mutex_unlock(jobConfig->resultMutex);
    myPosX=myId;

    while(myPosX < maxPoints)
    {
        tVec[0] = validPoints[p2d(DETREALX,myPosX,POINTS_VARS_AMOUNT)];
        tVec[1] = validPoints[p2d(DETREALY,myPosX,POINTS_VARS_AMOUNT)];
        tVec[2] = validPoints[p2d(DETREALZ,myPosX,POINTS_VARS_AMOUNT)] - conePosZ;
        myPosY=0;
        t=0;
        while(myPosY<raySamplingInt)
        {
            tempPoint2[1] = (t*tVec[1]) - volumeYMinusVolumeMiddle;//y
            tempPoint[0] = (t*tVec[0])- volumex;//x
            tempPoint[2] = t*tVec[2]+ conePosZMinusVolumeZ;//z
            
            tempPoint2[0] = ( tempPoint[0] * objThetaCos ) - ( tempPoint[2] * objThetaSin ) + ints[V_VOLUMEMIDDLE];
            tempPoint2[2] = ( ( tempPoint[0] * objThetaSin + tempPoint[2] * objThetaCos ) ) + ints[V_VOLUMEMIDDLE];
            t +=rayStep;
            if ((tempPoint2[0]>=0 && tempPoint2[1]>=0 && tempPoint2[2]>=0 && tempPoint2[0]<volumeSize-1 && tempPoint2[1]<volumeSize-1 && tempPoint2[2]<volumeSize-1))
            {
                float pIntensity[CALC_POINTS_SIZE] = {0};
                
                point[p2d(0,0,3)] = (int)(tempPoint2[0]+PT_VARIATION);
                point[p2d(0,1,3)] = point[p2d(0,0,3)]+1;

                point[p2d(1,0,3)] = (int)(tempPoint2[1]+PT_VARIATION);
                point[p2d(1,1,3)] = point[p2d(1,0,3)]+1;

                point[p2d(2,0,3)] = (int)(tempPoint2[2]+PT_VARIATION);     
                point[p2d(2,1,3)] = point[p2d(2,0,3)]+1;
                if ((tempPoint2[0]>0 && tempPoint2[1]>0 && tempPoint2[2]>0 && tempPoint2[0]<volumeSize-1 && tempPoint2[1]<volumeSize-1 && tempPoint2[2]<volumeSize-1))
                {
                    pIntensity[0] = volumeData[dp3d(P0X,P0Y,P0Z,volumeSize,volumeSize)];
                    pIntensity[5] = volumeData[dp3d(P5X,P5Y,P5Z,volumeSize,volumeSize)];
                    pIntensity[3] = volumeData[dp3d(P3X,P3Y,P3Z,volumeSize,volumeSize)];
                    pIntensity[7] = volumeData[dp3d(P7X,P7Y,P7Z,volumeSize,volumeSize)];
                    pIntensity[2] = volumeData[dp3d(P2X,P2Y,P2Z,volumeSize,volumeSize)];
                    pIntensity[6] = volumeData[dp3d(P6X,P6Y,P6Z,volumeSize,volumeSize)];
                    pIntensity[1] = volumeData[dp3d(P1X,P1Y,P1Z,volumeSize,volumeSize)];
                    pIntensity[4] = volumeData[dp3d(P4X,P4Y,P4Z,volumeSize,volumeSize)];
                }
                else
                {
                    if (P0X>=0 && P0X<volumeSize)
                    {
                        if (P0Z>=0 && P0Z<volumeSize)
                            pIntensity[0] = volumeData[dp3d(P0X,P0Y,P0Z,volumeSize,volumeSize)];
                        if (P5Z>=0 && P5Z<volumeSize)
                            pIntensity[5] = volumeData[dp3d(P5X,P5Y,P5Z,volumeSize,volumeSize)];
                        if (P3Z>=0 && P3Z<volumeSize)
                            pIntensity[3] = volumeData[dp3d(P3X,P3Y,P3Z,volumeSize,volumeSize)];
                        if (P7Z>=0 && P7Z<volumeSize)
                            pIntensity[7] = volumeData[dp3d(P7X,P7Y,P7Z,volumeSize,volumeSize)];
                    }
                    if (P1X>=0 && P1X<volumeSize)
                    {
                        if (P2Z>=0 && P2Z<volumeSize)
                            pIntensity[2] = volumeData[dp3d(P2X,P2Y,P2Z,volumeSize,volumeSize)];
                        if (P1Z>=0 && P1Z<volumeSize)
                            pIntensity[6] = volumeData[dp3d(P6X,P6Y,P6Z,volumeSize,volumeSize)];
                        if (P1Z>=0 && P1Z<volumeSize)
                            pIntensity[1] = volumeData[dp3d(P1X,P1Y,P1Z,volumeSize,volumeSize)];
                        if (P7Z>=0 && P7Z<volumeSize)
                            pIntensity[4] = volumeData[dp3d(P4X,P4Y,P4Z,volumeSize,volumeSize)];
                    }
                }
                m = (pIntensity[2]-pIntensity[0])/(P2X - point[p2d(0,0,3)]);
                pIntensity[INTENSITY_OFFSET+0] = ((tempPoint2[0]-point[p2d(0,0,3)]) * m) + pIntensity[0];
                point[p2d(0,POS_OFFSET+0,3)] = tempPoint2[0];
                point[p2d(1,POS_OFFSET+0,3)] = P2Y;
                point[p2d(2,POS_OFFSET+0,3)] = P2Z;
                
                m = (pIntensity[4]-pIntensity[3])/(P4X - P3X);
                pIntensity[INTENSITY_OFFSET+1] = ((tempPoint2[0]-P3X) * m) + pIntensity[3];
                point[p2d(0,POS_OFFSET+1,3)] = tempPoint2[0];
                point[p2d(1,POS_OFFSET+1,3)] = P3Y;
                point[p2d(2,POS_OFFSET+1,3)] = P3Z;

                m = (pIntensity[6]-pIntensity[5])/(P6X - P5X);
                pIntensity[INTENSITY_OFFSET+2] = ((tempPoint2[0]-P5X) * m) + pIntensity[5];
                point[p2d(0,POS_OFFSET+2,3)] = tempPoint2[0];
                point[p2d(1,POS_OFFSET+2,3)] = P5Y;
                point[p2d(2,POS_OFFSET+2,3)] = P5Z;

                m = (pIntensity[1]-pIntensity[7])/(point[p2d(0,1,3)] - P7X);
                pIntensity[INTENSITY_OFFSET+3] = ((tempPoint2[0]-P7X) * m) + pIntensity[7];
                point[p2d(0,POS_OFFSET+3,3)] = tempPoint2[0];
                point[p2d(1,POS_OFFSET+3,3)] = P7Y;
                point[p2d(2,POS_OFFSET+3,3)] = P7Z;

                //variando em y
                m = (pIntensity[INTENSITY_OFFSET+1]-pIntensity[INTENSITY_OFFSET+0])/(point[p2d(1,POS_OFFSET+1,3)] - point[p2d(1,POS_OFFSET+0,3)]);
                pIntensity[INTENSITY_OFFSET+4] = ((tempPoint2[1]-point[p2d(1,POS_OFFSET+0,3)]) * m) + pIntensity[INTENSITY_OFFSET+0];
                point[p2d(0,POS_OFFSET+4,3)] = tempPoint2[0];
                point[p2d(1,POS_OFFSET+4,3)] = tempPoint2[1];
                point[p2d(2,POS_OFFSET+4,3)] = P4Z;

                m = (pIntensity[INTENSITY_OFFSET+3]-pIntensity[INTENSITY_OFFSET+2])/(point[p2d(1,POS_OFFSET+3,3)] - point[p2d(1,POS_OFFSET+2,3)]);
                pIntensity[INTENSITY_OFFSET+5] = ((tempPoint2[1]-point[p2d(1,POS_OFFSET+2,3)]) * m) + pIntensity[INTENSITY_OFFSET+2];
                point[p2d(0,POS_OFFSET+5,3)] = tempPoint2[0];
                point[p2d(1,POS_OFFSET+5,3)] = tempPoint2[1];
                point[p2d(2,POS_OFFSET+5,3)] = P6Z;

                //variando em z
                m = (pIntensity[INTENSITY_OFFSET+5]-pIntensity[INTENSITY_OFFSET+4])/(point[p2d(2,POS_OFFSET+5,3)] - point[p2d(2,POS_OFFSET+4,3)]);
                pIntensity[INTENSITY_OFFSET+6] = ((tempPoint2[2]-point[p2d(2,POS_OFFSET+4,3)]) * m) + pIntensity[INTENSITY_OFFSET+4];
                detector[dp3d(
                validPoints[p2d(DETBASEX,myPosX,POINTS_VARS_AMOUNT)],
                validPoints[p2d(DETBASEY,myPosX,POINTS_VARS_AMOUNT)],
                planeIndex,
                detectorWidth,detectorHeight)] += ((tempPoint2[2]-point[p2d(2,POS_OFFSET+4,3)]) * m) + pIntensity[INTENSITY_OFFSET+4];
            }

            myPosY++;
        }
        myStep++;
        myPosX=((myStep*threadOffset)+myId);
    }

    pthread_mutex_lock(jobConfig->resultMutex);
    (*(jobConfig->threadCount))--;
    pthread_mutex_unlock(jobConfig->resultMutex);
    return NULL;
}


void * calculateLineSumNBCpu(void *arg)
{
    sis_JobConfig* jobConfig = (sis_JobConfig*)arg;
    int myId;
    int myStep=0;
    int myPosX=0;
    int myPosY=0;
    int *ints = jobConfig->ints;
    float * floats = jobConfig->floats;
    int threadOffset = ints[V_MAXTHREADS];
    int maxPoints = ints[V_VALIDDETECTORPOINTS]; 
    int volumeSize = ints[V_VOLUMESIZE];
    int detectorWidth = ints[V_DETECTORWIDTH];
    int detectorHeight = ints[V_DETECTORHEIGHT];
    float raySamplingDouble = floats[V_RAYSAMPLINGDOUBLE];
    int raySamplingInt = ints[V_RAYSAMPLING];
    float volumex = floats[V_VOLUMEX];
    float tVec[3];
    float objThetaSin = jobConfig->thetaSin;
    float objThetaCos = jobConfig->thetaCos;
    float conePosZMinusVolumeZ = floats[V_CONEPOSZ_MINUS_VOLUMEZ];
    float volumeYMinusVolumeMiddle = floats[V_VOLUMEY_MINUS_VOLUMEMIDDLE];
    float t=0, tempPoint[3],tempPoint2[3];
    float m;
    size_t point[AMOUNT_POINTS_INFO];
    float *volumeData = jobConfig->volume;
    float *detector = jobConfig->detector;
    float *validPoints = jobConfig->validPoints;
    int planeIndex = jobConfig->planeIndex;
    int conePosZ = (-floats[V_CONEDISTANCE]);
    float rayStep = 1.0 / raySamplingDouble;
    float topVolumeLimit = volumeSize-0.53; 
    pthread_mutex_lock(jobConfig->resultMutex);
    myId =jobConfig->threadIds;
    jobConfig->threadIds++;
    pthread_mutex_unlock(jobConfig->resultMutex);
    myPosX=myId;

    while(myPosX < maxPoints)
    {
        tVec[0] = validPoints[p2d(DETREALX,myPosX,POINTS_VARS_AMOUNT)];
        tVec[1] = validPoints[p2d(DETREALY,myPosX,POINTS_VARS_AMOUNT)];
        tVec[2] = validPoints[p2d(DETREALZ,myPosX,POINTS_VARS_AMOUNT)] - conePosZ;
        myPosY=0;
        t=0;
        while(myPosY<raySamplingInt)
        {
            tempPoint2[1] = (t*tVec[1]) - volumeYMinusVolumeMiddle;//y
            tempPoint[0] = (t*tVec[0])- volumex;//x
            tempPoint[2] = t*tVec[2]+ conePosZMinusVolumeZ;//z
            
            tempPoint2[0] = ( tempPoint[0] * objThetaCos ) - ( tempPoint[2] * objThetaSin ) + ints[V_VOLUMEMIDDLE];
            tempPoint2[2] = ( ( tempPoint[0] * objThetaSin + tempPoint[2] * objThetaCos ) ) + ints[V_VOLUMEMIDDLE];
            t +=rayStep;
            if ((tempPoint2[0]>=0 && tempPoint2[1]>=0 && tempPoint2[2]>=0 && tempPoint2[0]<topVolumeLimit && tempPoint2[1]<topVolumeLimit && tempPoint2[2]<topVolumeLimit))
            {
                float point[3];

                point[0] = nearbyint(tempPoint2[0]);
                point[1] = nearbyint(tempPoint2[1]);
                point[2] = nearbyint(tempPoint2[2]); 

                detector[dp3d(
                validPoints[p2d(DETBASEX,myPosX,POINTS_VARS_AMOUNT)],
                validPoints[p2d(DETBASEY,myPosX,POINTS_VARS_AMOUNT)],
                planeIndex,
                detectorWidth,detectorHeight)] += volumeData[dp3d(point[0],point[1],point[2],volumeSize,volumeSize)];
            }

            myPosY++;
        }
        myStep++;
        myPosX=((myStep*threadOffset)+myId);
    }

    pthread_mutex_lock(jobConfig->resultMutex);
    (*(jobConfig->threadCount))--;
    pthread_mutex_unlock(jobConfig->resultMutex);
    return NULL;
}             

    
void planeProjection(int *intsa, float *floatsa, float *validPoints,int planeIndex,float theta, float* detectorData, float* volumeData)
{
    pthread_mutex_t resultMutex;
    sis_TimeMeasure deltaT;
    
    pthread_t threadReference;
    sis_JobConfig jobsConfig;
    int threadCount=0;
    int localThreadCount=0;
    
    pthread_mutex_init(&resultMutex, NULL);
    
    jobsConfig.planeIndex = planeIndex;
    jobsConfig.thetaSin = sin(theta);
    jobsConfig.thetaCos = cos(theta);
    jobsConfig.detector = detectorData;
    jobsConfig.volume = volumeData;
    jobsConfig.validPoints = validPoints;
    jobsConfig.resultMutex = &resultMutex;
    jobsConfig.threadCount = &threadCount;
    jobsConfig.threadIds=0;
    jobsConfig.ints = intsa;
    jobsConfig.floats = floatsa;
    
    int i;
    if (intsa[V_POINTMODE]==PT_NEAREST_NEIGHBOR)
    {
        for(i=0;i<ints[V_MAXTHREADS];i++)
        {
            if(pthread_create(&threadReference, NULL, calculateLineSumNBCpu, &jobsConfig)) 
            {
                fprintf(stderr, "Error creating thread\n");
            }else
            {
                pthread_mutex_lock(&resultMutex);
                threadCount++;
                pthread_mutex_unlock(&resultMutex); 
            }
        }
    }
    else
    {
        for(i=0;i<ints[V_MAXTHREADS];i++)
        {
            if(pthread_create(&threadReference, NULL, calculateLineSumTriCpu, &jobsConfig)) 
            {
                fprintf(stderr, "Error creating thread\n");
            }else
            {
                pthread_mutex_lock(&resultMutex);
                threadCount++;
                pthread_mutex_unlock(&resultMutex); 
            }
        }
    }
    startTimer(&deltaT);
    localThreadCount=1;
    while(localThreadCount>0)
    {
        pthread_mutex_lock(&resultMutex);
        localThreadCount = threadCount;
        pthread_mutex_unlock(&resultMutex);
        usleep(500);
    }
}
