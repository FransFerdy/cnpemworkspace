
#include <stdio.h>
#include "material.h"

int main(int argc,  char** argv)
{
    if (argc!=2)
    {
        printf("Wrong number of parameters, param is: element name (e.g Fe , Cu) \n");
        return 1;
    }
    getMaterialInfo(argv[1]);
}