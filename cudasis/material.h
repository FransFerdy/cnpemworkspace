#ifndef __SIS_MATERIAL__
#define __SIS_MATERIAL__

#define CLASSICAL_ELECTRON_RADIUS 2.8179403227E-15 //Meters
#define PI 3.14159265359

#ifdef __cplusplus
extern "C" {
#endif

typedef struct PhantomMaterialAtEnergy
{
    double A_atomicMass;
    double density;
    double Z_electronsPerAtom;
    double lambda;
    double f1,f2;
} PhantomMaterialAtEnergy;

typedef struct ComplexNumber
{
    float real;
    float imaginary;
} ComplexNumber;


PhantomMaterialAtEnergy * getMaterialAtEnergy(char * element,float energy);

void getMaterialInfo(char * element);

float * getMaterialRefractiveIndexAtEnergy(char * elementArg,float energy,int interp);

#ifdef __cplusplus
}
#endif

#endif 