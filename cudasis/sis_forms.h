#ifndef __SIS_FORMS__
#define __SIS_FORMS__
#include "sis_datatypes.h"

#define CLASSICAL_ELECTRON_RADIUS 2.8179403227E-15 //Meters

#ifdef __cplusplus
extern "C" {
#endif
void makeDonutizedVolume(sis_Volume* volume);
sis_Detector *createDetector(size_t widtharg,size_t heightarg, size_t deptharg,float xzangle,float yzangle,float xyangle,int rollMode);
sis_Detector *createDetectorComplex(size_t widtharg,size_t heightarg, size_t deptharg,float xzangle,float yzangle,float xyangle,int rollMode);
void deleteDetector(sis_Detector* toDelete);
sis_Volume *createVolume(size_t sizearg,int x,int y, int z);
void deleteVolume(sis_Volume* toDelete);
sis_Volume * loadHDF5floatsVolume(char * fileName, int complex);
sis_ConeBeam *createConeBeam(int distancearg,float aarg,float barg,int x,int y, int z);
void saveHDF5PlaneProjection(sis_Detector* detector, int planeIndex, char* filename);
void saveHDF5VolumeProjection(sis_Detector* detector, char* filename);

void makeDonutsVolume(sis_Volume* volume,float R, float r,size_t elemAmount, float ** elements, int maxThreads, long int seed);
void saveHDF5Volume(sis_Volume* volume, char* filename);
sis_Volume *createVolumeComplex(size_t sizearg,int x,int y, int z);
void makeDonutsVolumeComplex(sis_Volume* volume,float R, float r,float tiltAngle,float addx,float addy,float addz,size_t elemAmount, float ** elements, int maxThreads, long int seed);
void makeSphereVolumeComplex(sis_Volume* volume,float R,float tiltAngle,float addx,float addy,float addz,size_t elemAmount, float ** elements, int maxThreads, long int seed);
void makeCubeVolumeComplex(sis_Volume* volume,float size,float tiltAngle,float addx,float addy,float addz,size_t elemAmount, float ** elements, int maxThreads, long int seed);
void makeDiscVolumeComplex(sis_Volume* volume,float R,float size,float tiltAngle,float addx,float addy,float addz,size_t elemAmount, float ** elements, int maxThreads, long int seed);



#ifdef __cplusplus
}
#endif

#endif
