ARG BASENAME 
ARG CUDAVER 
# BASENAME is  either: nvidia/cuda for x86 or nvidia/cuda-ppc64le for powerpc 
# CUDAVER same cuda version as host machine, 9.2, 10.0
FROM ${BASENAME}:${CUDAVER}-devel

RUN apt-get clean
RUN apt-get update

ENV DEBIAN_FRONTEND=noninteractive

RUN apt-get install -y libcurl4-openssl-dev
RUN apt-get install -y libhdf5-dev
RUN apt-get install -y python-matplotlib
RUN apt-get install -y python-pip
RUN pip install --upgrade pip
RUN pip install numpy 
RUN apt-get install -y python-h5py

ADD cudasis /app

WORKDIR /app
RUN make clean
RUN make

